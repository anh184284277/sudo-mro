@extends('Default::web.layouts.app')
@section('content')
	<main class="site-main">
		<div class="container">
			<div class="site-main__banner">
				<div class="owl-carousel owl-theme">
                    <div class="item">
                		<a href="">
                			<img src="/assets/images/Bitmap.jpg" alt="">
                		</a>
                    </div>
                    <div class="item">
                		<a href="">
                			<img src="/assets/images/Bitmap.jpg" alt="">
                		</a>
                    </div>
                    <div class="item">
                		<a href="">
                			<img src="/assets/images/Bitmap.jpg" alt="">
                		</a>
                    </div>
                </div>		
			</div>
			<div class="site-main__images">
				<div class="list">
					<div class="list-item">
						<img src="/assets/images/Bitmap (1).jpg" alt="">
					</div>
					<div class="list-item">
						<img src="/assets/images/Bitmap Copy 3.jpg" alt="">
					</div>
					<div class="list-item">
						<img src="/assets/images/Bitmap (2).jpg" alt="">
					</div>
				</div>
			</div>
			<div class="site-main__slide">
                @if(isset($product_sellings) && count($product_sellings) > 0)
				    <h3>SẢN PHẨM NỔI BẬT</h3>
                    <div class="owl-carousel owl-theme">
                       @foreach ($product_sellings as $c => $product_selling)
                        <div class="item">
                        	<div class="item-images">
                        		<a href="">
                        			 <img src="{{ $product_selling->image }}" alt="">
                        		</a>
                        	</div>
                            <div class="item-info">
                            	<a href="">
                            		<h4>{{ $product_selling->name }}</h4>
                            	</a>
                            	<div class="item-info__price">
                            		<span>{{ formatPrice($product_selling->price) }}</span>
                            	</div>
                            </div>
                        </div>
                          @endforeach
                    </div>
                @endif
            </div>
            <div class="site-main__category">
            	<h3>DANH MỤC SẢN PHẨM</h3>
            	<div class="list">
            		<div class="row " style="justify-content: center;">
						@if(isset($categories) && count($categories) > 0)
						    @foreach ($categories as $c => $cate)
		            			<div class=" col-xl-2 col-lg-3 col-md-4 col-sm-6 ">
		            				<div class="list-product">
				            			<div class="list-product__image">
				            				<a href="">
				            					<img src="{{ $cate->getImage('medium') }}" >
				            				</a>
				            			</div>
				            			<div class="list-product__content">
				            				<a href=""> {!! $cate->name ?? '' !!}</a>
				            			</div>
				            		</div>
		            			</div>
		            		@endforeach
            			@endif
            		</div>           		
 	            </div>
            </div>
            <div class="site-main__producer">
            	<div class="top">
            		<div class="top-left">
	            		<h3>CÁC HÃNG</h3>
	            	</div>
	            	<div class="top-right">
	            		<span>Xem tất cả</span>
	            		<i class="fas fa-caret-right"></i>
	            	</div>
            	</div>
            	@if(isset($brands) && count($brands) > 0)
	            	<div class="table">
	            		@foreach ($brands as $brand)
						   <div class="band">
						    	<a href="#">
						    		<img class="lazy" src="{{ $brand->getImage('small') }}" alt="{{ getAlt($brand->image) }}" width="186" height="50">
						    	</a>
						    </div>
						@endforeach
	            	</div>
            	@endif
            </div>
        </div>
        <div class="site-main__center">
        	<div class="container">
                @if(isset($categories) && count($categories) > 0)
                    @foreach ($categories as $c => $cate)
                        @php
                            $childs = \Sudo\Ecommerce\Models\ProductCategory::where('parent_id', $cate->id)->limit(5)->get();
                            $product_ids = \DB::table('product_category_maps')
                                ->where('product_category_id', $cate->id)
                                ->pluck('product_id');
                            $products = \Sudo\Ecommerce\Models\Product::whereIn('id', $product_ids)->limit(6)->get();
                        @endphp
                		<div class="product">
        	            	<div class="product-title">
        	            		<div class="product-title__text">
        	            			<h3>{{ $cate->name }}</h3>
        	            		</div>
        	            		<div class="product-title__menu">
        	            			<ul>
                                        @foreach ($childs as $child)
                                            <li><a href="">{{ $child->name }}</a></li>
                                        @endforeach
                                        <li><a href="">Tất Cả Thiết Bị Dùng Pin</a></li>
                                        <li><a href=""><i class="fas fa-caret-right"></i></a></li>
        	            			</ul>
        	            		</div>
        	            	</div>
        	            	<div class="product-content">
                                @foreach ($products as $product)
            	            		<div class="product-content__item">
            	                    	<div class="images">
            	                    		<a href="">
            	                    			<img src="{{ $product->image }}" alt="">
            	                    		</a>
            	                    	</div>
            	                        <div class="info">
            	                        	<a href="">{{ $product->name }}</a>
            	        	               	<h3>{{ formatPrice($product->price) }}</h3>
            	                        </div>
            	                    </div>
                                @endforeach
        	            	</div>
        	            </div>
                    @endforeach
                @endif
        	</div>
        </div>
        <div class="container">
        	<div class="site-main__post">
            	<div class="title">
            		<div class="title-text">
            			<h3>KIẾN THỨC VÀ KĨ NĂNG</h3>
            		</div>
            		<div class="title-menu">
            			<ul>
            				<li><a href="">tư vấn sản phẩm</a></li>
            				<li class="in"><a href="">hướng dẫn sử dụng</a></li>
            				<li class="in"><a href="">chia sẻ kiến thức</a></li>
            				<li class="in"><a href=""><p>xem tất cả</p><i class="fas fa-caret-right"></i></a></li>
            			</ul>
            		</div>
            	</div>
            	<div class="content">
                    @foreach ($posts as $item)
                		<div class="content-item">
                        	<div class="content-item__images">
                        		<a href="">
                        			<img src="{{ $item->image}}" alt="">
                        		</a>
                        	</div>
                            <div class="content-item__info">
                            	<div class="top">
                            		<h4>{!! $item->name ?? '' !!}</h4>
                            	</div>
                            	<div class="center">
                            		<ul>
                            			<li><a href="">
                            				<img src="/assets/images/eye copy.png" alt="">
                            			</a></li>
                            			<li><a href=""><span>2569</span></a></li>
                            		</ul>
                            		<ul>
                            			<li><a href="">
                            				<img src="/assets/images/chat copy.png" alt="">
                            			</a></li>
                            			<li><a href=""><span>9</span></a></li>
                            		</ul>
                            	</div>
                            	<div class="bottom">
                                    <div class="text-box">
                                        <p>Máy hàn điện tử đang dần trở thành sự lựa chọn để thay thế cho những loại máy hàn truyền thống </p>
                                        <a href="">
                                        	<span>Xem thêm</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
            	</div>
	        </div>
        </div>
        <div class="site-main__bottom">
            <div class="container">
                <div class="review">
                    <div class="review-title">
                        <p>Review mới</p>
                    </div>
                    <div class="review-icon">
                        <p>Theo dõi trên</p>
                        <div class="icon-youtube">
                            <img src="/assets/images/youto.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="banner">
                    <div class="col col-12 col-lg-8" style="padding-right: 20px;">
                        <div class="banner-left">
                            <img src="/assets/images/banner1.png" alt="">
                        </div>
                        <div class="banner-title">
                            <p>Review - Máy đục Stanley STHM5KH 1010W | SUPER MRO</p>
                        </div>
                    </div>
                    <div class="col col-12 col-lg-4">
                        <div class="banner-right">
                            <div class="box-right">
                                <img src="/assets/images/kim.png" alt="">
                                <p>Kìm cắt thủy lực ASAKI AK-0621 - 20mm</p>
                            </div>
                            <div class="box-right">
                                <img src="/assets/images/Bitmap (9).png" alt="">
                                <p>Tư vấn - Dụng cụ cầm tay - Những vật dụng cần thiết trong gia đình | SUPER MRO</p>
                            </div>
                            <div class="box-right-all">
                                <img src="/assets/images/Bitmap (10).png" alt="">
                                <p>So sánh - 3 máy khoan búa Bosch GBH 2-26DRE - Dewalt D25143 - Makita HR2630 | SUPER MRO</p>
                            </div>
                            <div class="box-right">
                                <img src="/assets/images/Bitmap Copy 30.png" alt="">
                                <p>Review - Bộ Khẩu Stanley 89-518-1 37 Chi Tiết | SUPER MRO</p>
                            </div>
                        </div>
                        <div class="buttom-all">
                            <p>Xem tất cả</p>
                            <i class="fas fa-caret-right"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</main>
@endsection