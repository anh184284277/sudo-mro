<footer class="site-footer">
	<div class="site-footer__top">
		<div class="container">
			<div class="box">
				<div class="box-content">
					<div class="site-content__supe">
						<div class="title">
							<h4>Super MRO</h4>
						</div>
						<div class="info">
							<ul>
                                <li >
                                	<a href="">Mã số doanh nghiệp:
										<span>{{$config_general['code']}}</span>
									</a>
                                </li>
                                <li>
                                	<a href="">Văn phòng: 
										<span>{{$config_general['adre']}}</span>
									</a>
                                </li>
                                <li>
                                    <a href="">Kho hàng: 
										<span>{{$config_general['warehouse']}}</span>
									</a>
                                </li>
                                <li>
									<a href="">
										Tel: 
										<span>{{$config_general['phone']}}</span>
									</a>
								</li>
								<li>
									<a href="">
										E-mail: 
										<span>{{$config_general['email']}}</span>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="site-content__info">
						<div class="title">
							<h4>Thông tin</h4>
						</div>
						<div class="nav">
							<ul>
								@php
                                    $nav = json_decode($config_menu['menu_info']) ?? [];
                                @endphp
                                @foreach ($nav as $element)
                                    <li>
                                        <a href="">{!!$element->name ?? '' !!}</a>
                                    </li>
                                @endforeach
							</ul>
						</div>
					</div>
					<div class="site-content__support">
						<div class="title">
							<h4>Hỗ trợ khách hàng</h4>
						</div>
						<div class="nav">
							<ul>
								@php
                                    $nav = json_decode($config_menu['menu_suppost']) ?? [];
                                @endphp
                                @foreach ($nav as $element)
                                    <li>
                                        <a href="">{!!$element->name ?? '' !!}</a>
                                    </li>
                                @endforeach
							</ul>
						</div>
					</div>
					<div class="site-content__account">
						<div class="title">
							<h4>Tài khoản</h4>
						</div>
						<div class="nav">
							<ul>
								@php
                                    $nav = json_decode($config_menu['menu_admin']) ?? [];
                                @endphp
                                @foreach ($nav as $element)
                                    <li>
                                        <a href="">{!!$element->name ?? '' !!}</a>
                                    </li>
                                @endforeach
							</ul>
						</div>
						<div class="connect">
							<h3>Kết nối với chúng tôi</h3>
							<ul>
								<li><a href="{{$config_general['facebook']}}"><img src="/assets/images/facebook.png" alt=""></a></li>
								<li><a href="{{$config_general['youtube']}}"><img src="/assets/images/Group 8.png" alt=""></a></li>
								<li><a href="{{$config_general['web']}}"><img src="/assets/images/wordpress.png" alt=""></a></li>
							</ul>
						</div>
						<div class="bottom">
							<img src="/assets/images/Bitmap (5).png" alt="">
						</div>
					</div>
					<div class="site-content__map">
						<div class="title">
							<h4>Hướng dẫn chỉ đường</h4>
						</div>
						<div class="center">
							<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3779.938592706119!2d105.67171981461522!3d18.66675138732404!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3139ce63742e981f%3A0x6449d97000dfe639!2zVG_DoCBuaMOgIEThuqd1IGtow60gUXVhbmcgVHJ1bmc!5e0!3m2!1svi!2s!4v1636970888966!5m2!1svi!2s" width="210" height="208" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="site-footer__bottom">
		<p>© 2013 Powered by IPComs Software. All Rights Reserved</p>
	</div>
</footer>