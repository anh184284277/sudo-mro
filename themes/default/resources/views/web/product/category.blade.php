@extends('Default::web.layouts.app')
@section('content')
	<main class="site-main" id="category">
		<div class="container">
			<div class="site-main__menu">
	            <ul>
	                <li><a href="">Trang chủ</a><i class="far fa-angle-right"></i></li>
	                <li><a href="">Dụng cụ dùng điện</a><i class="far fa-angle-right"></i></li>
	                <li>Máy khoan vặn vít động lực dùng pin Dewalt DCD778D2 18V </li>
	            </ul>
	        </div>
	        <div class="site-main__top">
	        	<div class="container">
	        		<div class="content">
	        			<div class="left">
	                        <div class="left-in">
	                        	<div class="left-in__images">
									<div class="images_large">
										<div class="owl-carousel ">
						                    <div class="item">
						                        <a href="#" class="venobox" dataType="image" data-gall="myGallery" >
						                        	 <img src="/assets/images/Bitmap.png" />
						                        </a> 
						                    </div>
							            </div>				
							        </div>
									<div class="images_thumnail mt-15 owl-carousel">
				                        <div class="item">
				                            <a href="">
				                            	<img src="/assets/images/Bitmap Copy 4.png" />
				                            </a>
				                        </div>
				                        <div class="item">
				                            <a href="#">
				                            	<img src="/assets/images/Bitmap Copy 4.png" />
				                            </a>
				                        </div>
				                        <div class="item">
				                            <a href="#">
				                            	<img src="/assets/images/Bitmap Copy 4.png" />
				                            </a>
				                        </div>
				                        <div class="item">
				                            <a href="#">
				                            	<img src="/assets/images/Bitmap Copy 4.png" />
				                            </a>
				                        </div>
				                        <div class="item">
				                            <a href="#">
				                            	<img src="/assets/images/Bitmap Copy 4.png" />
				                            </a>
				                        </div>
							         </div>
								</div>
	                        </div>
	                    </div>
	                    <div class="right">
	                        <div class="right-title">
	                            <h1>Máy khoan vặn vít động lực dùng pin Dewalt DCD778D2 18V</h1>
	                        </div>
	                        <div class="right-content">
	                            <p>Model: #A0001</p>
	                            <h3>Mô tả:</h3>
	                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	                            <div class="right-content__price">
	                                <h1>4,050,000₫</h1>
	                                <p>(Đơn giá đã bao gồm VAT)</p>
	                            </div>
	                        </div>
	                        <div class="right-purchase">
	                            <div class="right-purchase__left">
									<span>Số lượng</span>
									<div class="quantity">
										<button class="quantity-plus"></button>
										<input type="text" value="1" min="1" max="100" name="quantity" style="border: 1px solid rgb(238, 238, 238);">
										<button class="quantity-minus"></button>
									</div>
								</div>
	                            <div class="right-purchase__right">
	                                <button>
	                                    <a href=""><p>Mua ngay</p></a>
	                                </button>
	                            </div>
	                        </div>
	                        <div class="right-cart">
	                            <button>
	                                <a href=""><p>Thêm vào giỏ hàng</p></a>
	                            </button>
	                        </div>
	                        <div class="right-call">
	                            <h2>ĐẶT HÀNG NGAY CHỈ CẦN ĐỂ LẠI SỐ ĐIỆN THOẠI  !</h2>
	                            <input type="text" placeholder="Nhập số điện thoại yêu cầu gọi lại…">
	                            <button><a href=""><p>Gọi lại cho tôi</p></a></button>
	                        </div>
	                        <div class="right-heart">
	                            <i class="fas fa-heart"></i>
	                            <p>Yêu thích</p>
	                        </div>
	                    </div>
	        		</div>
	        	</div>
	        </div>
	        <div class="site-main__body">
	        	<div class="left">
	        		<div class="left-table">
	        			<h3>Thông số kỹ thuật</h3>
	            		<ul>
							<li>
								<span class="left-title in">Thông số</span>
								<span class="left-content"></span>
							</li>
							<li>
								<span class="left-title">Hãng sản xuất</span>
								<span class="left-content">Bosch</span>
							</li>
							<li>
								<span class="left-title">Xuất xứ</span>
								<span class="left-content">Malaysia</span>
							</li>
							<li>
								<span class="left-title">Tốc độ không tải</span>
								<span class="left-content">0 - 2800 vòng/phút</span>
							</li>
							<li>
								<span class="left-title">Mô-men xoắn (các công việc vặn vít mềm):</span>
								<span class="left-content">10,8 Nm</span>
							</li>
							<li>
								<span class="left-title">Mô men xoắn định mức</span>
								<span class="left-content">1,8 Nm</span>
							</li>
							<li>
								<span class="left-title">Mô-men xoắn (các công việc vặn vít mềm):</span>
								<span class="left-content">10,8 Nm</span>
							</li>
							<li>
								<span class="left-title">Mô-men xoắn định mức:</span>
								<span class="left-content">1,8 Nm</span>
							</li>
						</ul>
	        		</div>
	        		<div class="left-info">
	                    <h1>Chi tiết sản phẩm</h1>
	                    <div class="left-info__content">
	                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	                        <div class="video">
	                            <iframe width="560" height="315" src="https://www.youtube.com/embed/jNvWLC-VU4c" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
	                        </div>
	                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	                        <div class="img">
	                            <img src="/assets/images/mayvit.jpg" alt="">
	                        </div>
	                    </div>
	                </div>
	                <div class="left-comment">
	                    <h2>Đánh giá sản phẩm</h2>
	                    <div class="left-comment__top">
	                        <div class="star">
	                            <i class="fas fa-star"></i>
	                            <i class="fas fa-star"></i>
	                            <i class="fas fa-star"></i>
	                            <i class="fas fa-star"></i>
	                            <i class="fas fa-star"></i>
	                        </div>
	                        <button>
	                            <p>Viết đánh giá</p>
	                        </button>
	                    </div>
	                    <div class="left-comment__center">
	                        <div class="content">
	                            <button>
	                                <p>T</p>
	                            </button>
	                            <p class="content-name">Trần Minh Trọng</p>
	                            <div class="content-star">
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                                <i class="fas fa-star"></i>
	                            </div>
	                        </div>
	                        <div class="comment">
	                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
	                            <div class="comment-feel">
	                                <a href="">Trả lời </a>
	                                <p>-  24 phút trước</p>
	                            </div>
	                        </div>
	                    </div>
	                    <div class="left-comment__bottom">
	                        <div class="logo">
	                            <img src="/assets/images/Bitmap (7).png" alt="">
	                        </div>
	                        <div class="box">
	                            <div class="box-title">
	                                <p>Super MRO</p>
	                                <button><p>Quản trị viên</p></button>
	                            </div>
	                            <div class="box-comment">
	                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, </p>
	                                <div class="box-comment__feel">
	                                    <a href="">Trả lời </a>
	                                    <p>-  24 phút trước</p>
	                                </div>
	                            </div>
	                        </div>
	                    </div>
	                </div>
	        	</div>
	        	<div class="right">
	        		<div class="right-top">
	        			<h2>Bài viết liên quan</h2>
	                    <div class="right-top__list">
	                        <ul>
	                        	@foreach ($posts as $item)
		                            <li>
		                            	<a href="">
		                            		<img src="{{ $item->image}}" alt="">
		                                	<p>{!! $item->name ?? '' !!}</p>
		                            	</a>
		                            </li>
	                            @endforeach
	                        </ul>
	                    </div>
	                </div>
	                <div class="right-top">
	                	<h2>Bài viết mới nhất</h2>
	                	<div class="right-top__list">
	                    	<ul>
	                    		@foreach ($posts as $item)
		                            <li>
		                            	<a href="">
		                            		<img src="{{ $item->image}}" alt="">
		                                	<p>{!! $item->name ?? '' !!}</p>
		                            	</a>
		                            </li>
	                            @endforeach
	                        </ul>
	                    </div> 
	                </div>
	                <div class="right-images">
	                    <img src="/assets/images/Bitmap Copy 7.jpg" alt="">
	                    <img src="/assets/images/Bitmap Copy 14.jpg" alt="">
	                </div> 
	        	</div>
	        </div>
	        <div class="site-main__slide">
	        	@if(isset($product_sellings) && count($product_sellings) > 0)
					<h3>Phụ kiện đi kèm</h3>
	                <div class="owl-carousel owl-theme">
	                   	@foreach ($product_sellings as $c => $product_selling)
		                    <div class="item">
		                    	<div class="item-images">
		                    		<a href="">
		                    			 <img src="{{ $product_selling->image }}" alt="">
		                    		</a>
		                    	</div>
		                        <div class="item-info">
		                        	<a href="">
		                        		<h4>{{ $product_selling->name }}</h4>
		                        	</a>
		                        	<div class="item-info__price">
		                        		<span>{{ formatPrice($product_selling->price) }}</span>
		                        	</div>
		                        </div>
		                    </div>
	                    @endforeach
	                </div>
                @endif
	        </div>
	        <div class="site-main__slide">
	        	@if(isset($product_sellings) && count($product_sellings) > 0)
					<h3>Sản phẩm tương tự</h3>
	                <div class="owl-carousel owl-theme">
	                   	@foreach ($product_sellings as $c => $product_selling)
		                    <div class="item">
		                    	<div class="item-images">
		                    		<a href="">
		                    			 <img src="{{ $product_selling->image }}" alt="">
		                    		</a>
		                    	</div>
		                        <div class="item-info">
		                        	<a href="">
		                        		<h4>{{ $product_selling->name }}</h4>
		                        	</a>
		                        	<div class="item-info__price">
		                        		<span>{{ formatPrice($product_selling->price) }}</span>
		                        	</div>
		                        </div>
		                    </div>
	                    @endforeach
	                </div>
                @endif
	        </div>
		</div>
	</main>
@endsection