<?php

namespace Sudo\Theme\Http\Controllers\Web;

use Illuminate\Http\Request;
use Sudo\Ecommerce\Models\Product;
use Sudo\Ecommerce\Models\ProductCategory;
use Sudo\Ecommerce\Models\ProductCategoryMap;
use Sudo\Ecommerce\Models\Brand;
use Sudo\Theme\Models\Post;

use DB;

class HomeController extends Controller
{
	public function index() {
	\Asset::addStyle(['all','owl-carousel','home','stylesheet','responsive','general'])->addScript(['jquery','owl-carousel','main']);
		getimagesize('https://sudospaces.com/karofi-com/2020/04/quat-dieu-hoa-karofi-kac-18r.png');
		// Seo
		$setting_home = getOption('home');


		$meta_seo = metaSeo('', '', [
			'title' => $setting_home['meta_title'] ?? 'Trang chủ',
			'description' => $setting_home['meta_description'] ?? 'Mô tả trang chủ',
			'image' => $setting_home['meta_image'] ?? getImage(),
		]);
		$brands = Brand::where('status', 1)->get();


		$categories = ProductCategory::select('product_categories.*')
		    ->join('pins', function ($join) {
		        $join->on('pins.type_id','=','product_categories.id');
		        $join->on('pins.type',DB::raw("'product_categories'"));
		        $join->on('pins.place',DB::raw("'home'"));
		    })
		    ->addSelect(DB::raw('(case when pins.value is null then 2147483647 else pins.value end) as home'))
		    ->orderBy('home', 'ASC')
		    ->where('product_categories.status', 1)
		    ->limit(10)->get();

		$posts = Post::get();

		$slides = DB::table('slides')->where('status', 1)->orderBy('orders', 'asc')->get();

		$product_sellings = Product::where('status', 1)
		->orderBy('id', 'desc')->get();
		// dd($product_sellings);

		$admin_bar = route('admin.settings.home');

		return view('Default::web.home', compact(
			'meta_seo', 'admin_bar','slides','brands','product_sellings','categories','posts',
		));
	}
}