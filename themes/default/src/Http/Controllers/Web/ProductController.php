<?php

namespace Sudo\Theme\Http\Controllers\Web;

use Illuminate\Http\Request;
use Sudo\Theme\Models\Post;
use Sudo\Ecommerce\Models\Product;
use DB;

class ProductController extends Controller
{
    public function index() {
	\Asset::addStyle(['all','owl-carousel','category','stylesheet','responsive','general'])->addScript(['jquery','owl-carousel','main']);

		$product_sellings = Product::where('status', 1)
		->orderBy('id', 'desc')->get();

	
		$posts = Post::get();

		return view('Default::web.product.category', compact('posts','product_sellings'));
	}
}
