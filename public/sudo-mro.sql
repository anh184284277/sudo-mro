-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 25, 2021 at 08:57 AM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sudo-mro`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_password_resets`
--

CREATE TABLE `admin_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `admin_users`
--

CREATE TABLE `admin_users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `avatar` text COLLATE utf8mb4_unicode_ci,
  `infomation` longtext COLLATE utf8mb4_unicode_ci,
  `social` text COLLATE utf8mb4_unicode_ci,
  `capabilities` longtext COLLATE utf8mb4_unicode_ci,
  `remember_token` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin_users`
--

INSERT INTO `admin_users` (`id`, `name`, `email`, `password`, `position`, `display_name`, `phone`, `address`, `birthday`, `avatar`, `infomation`, `social`, `capabilities`, `remember_token`, `status`, `created_at`, `updated_at`) VALUES
(1, 'dev', 'dev@sudo.vn', '$2y$10$pK2Ipf.yPgiNgAizkqMPTuowlGX.u6NhpsabrqtTd7ftez34k0li2', NULL, 'Sudo Developer', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '9xH1YV7FjmG6AmXQCXtyMcHvq4utyDUQaejfiX4IyLUlL92MWYVkIwW0Rgkn', 1, '2021-11-22 14:23:22', '2021-11-22 14:23:22'),
(2, 'sudo', 'sudo@sudo.vn', '$2y$10$22VbDkGXHsiAZE8HHbxKfOC/kZ1n.R/kEbykwPoMXGPE/WCD9sOAO', NULL, 'Sudo Ecommerce', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, '2021-11-22 14:23:22', '2021-11-22 14:23:22');

-- --------------------------------------------------------

--
-- Table structure for table `attributes`
--

CREATE TABLE `attributes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_layout` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_searchable` int(11) NOT NULL DEFAULT '0',
  `order` int(11) NOT NULL DEFAULT '99999',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_details`
--

CREATE TABLE `attribute_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `attribute_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '99999',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `attribute_variant_maps`
--

CREATE TABLE `attribute_variant_maps` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL,
  `attribute_detail_id` int(11) NOT NULL,
  `variant_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `image`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 'fluke', 'fluke', '/core/2021/11/bitmap-1-1.png', NULL, 1, '2021-11-23 09:12:46', '2021-11-23 09:12:49'),
(2, 'dewalt', 'dewalt', '/core/2021/11/bitmap-2.png', NULL, 1, '2021-11-23 09:13:10', '2021-11-23 09:13:13'),
(3, 'bosch', 'bosch', '/core/2021/11/bitmap-4.png', NULL, 1, '2021-11-23 09:13:30', '2021-11-23 09:13:32'),
(4, 'asaki', 'asaki', '/core/2021/11/bitmap.png', NULL, 1, '2021-11-23 09:13:49', '2021-11-23 09:13:51'),
(5, 'nikawa', 'nikawa', '/core/2021/11/bitmap-3.png', NULL, 1, '2021-11-23 09:14:09', '2021-11-23 09:14:11'),
(6, 'wiki', 'wiki', '/core/2021/11/bitmap-3.png', NULL, 1, '2021-11-23 09:37:11', '2021-11-23 09:37:14'),
(7, 'bo', 'bo', '/core/2021/11/bitmap-4.png', NULL, 1, '2021-11-23 09:37:30', '2021-11-23 09:37:32'),
(8, 'fl', 'fl', '/core/2021/11/bitmap-1-1.png', NULL, 1, '2021-11-23 09:37:48', '2021-11-23 09:37:50'),
(9, 'de', 'de', '/core/2021/11/bitmap-2.png', NULL, 1, '2021-11-23 09:38:02', '2021-11-23 09:38:04'),
(10, 'ni', 'ni', '/core/2021/11/bitmap-3.png', NULL, 1, '2021-11-23 09:38:18', '2021-11-23 09:38:19');

-- --------------------------------------------------------

--
-- Table structure for table `call_me_backs`
--

CREATE TABLE `call_me_backs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `current_page` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active_status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL DEFAULT '0',
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `email_registers`
--

CREATE TABLE `email_registers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `google_shoppings`
--

CREATE TABLE `google_shoppings` (
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` int(11) NOT NULL,
  `brand` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `in_stock` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `item_condition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `language_metas`
--

CREATE TABLE `language_metas` (
  `lang_table` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_table_id` int(11) NOT NULL,
  `lang_locale` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lang_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language_metas`
--

INSERT INTO `language_metas` (`lang_table`, `lang_table_id`, `lang_locale`, `lang_code`) VALUES
('slides', 1, 'vi', '619cae5504d6d'),
('brands', 1, 'vi', '619cb08e71378'),
('brands', 2, 'vi', '619cb0a6d9e5e'),
('brands', 3, 'vi', '619cb0ba1f195'),
('brands', 4, 'vi', '619cb0cd6571a'),
('brands', 5, 'vi', '619cb0e134cc3'),
('brands', 6, 'vi', '619cb6473cfad'),
('brands', 7, 'vi', '619cb65a2e996'),
('brands', 8, 'vi', '619cb66c1599b'),
('brands', 9, 'vi', '619cb67ac70d9'),
('brands', 10, 'vi', '619cb68a0b15a'),
('product_categories', 1, 'vi', '619cbae16584e'),
('product_categories', 2, 'vi', '619cbafb45f84'),
('product_categories', 3, 'vi', '619cbb0d3b151'),
('product_categories', 4, 'vi', '619cbb413dd6f'),
('product_categories', 5, 'vi', '619cbb5b7f4dc'),
('product_categories', 6, 'vi', '619cbb76d6c20'),
('products', 1, 'vi', '619cbbfc4b06a'),
('products', 2, 'vi', '619ccc9d77d11'),
('products', 3, 'vi', '619ccce85b503'),
('products', 4, 'vi', '619ccd2d6cd4d'),
('post_categories', 1, 'vi', '619e12a58105c'),
('posts', 1, 'vi', '619e12b936d04'),
('post_categories', 2, 'vi', '619e1e6e94b0e'),
('posts', 2, 'vi', '619e1f2f6cc3c'),
('posts', 3, 'vi', '619e1fdd2d76f'),
('posts', 4, 'vi', '619e1ffc78d2c'),
('posts', 5, 'vi', '619e2039303fd'),
('product_categories', 7, 'vi', '619e20bc993d0'),
('product_categories', 8, 'vi', '619e212924c6e'),
('product_categories', 9, 'vi', '619e215cc612d'),
('product_categories', 10, 'vi', '619e21d2577dd'),
('product_categories', 11, 'vi', '619e2226832c8'),
('product_categories', 12, 'vi', '619e24811b322'),
('product_categories', 13, 'vi', '619e24bebaf84'),
('product_categories', 14, 'vi', '619e24e63ad3d'),
('product_categories', 15, 'vi', '619e2501e34e8'),
('product_categories', 16, 'vi', '619e254745555'),
('product_categories', 17, 'vi', '619e25a7994bf'),
('product_categories', 18, 'vi', '619e61cb64308'),
('product_categories', 19, 'vi', '619e61e53ea78'),
('product_categories', 20, 'vi', '619e62126dc07'),
('product_categories', 21, 'vi', '619e622b5dbc1'),
('product_categories', 22, 'vi', '619e6251e5e0e'),
('product_categories', 23, 'vi', '619e6268de7f9'),
('product_categories', 24, 'vi', '619e628d57e81'),
('product_categories', 25, 'vi', '619e62a0ac759'),
('products', 5, 'vi', '619e63a526707'),
('product_categories', 26, 'vi', '619efc404688b'),
('product_categories', 27, 'vi', '619efe90b9e18'),
('product_categories', 28, 'vi', '619efeb467194'),
('product_categories', 29, 'vi', '619efef545be0'),
('products', 6, 'vi', '619f00983ce18'),
('products', 7, 'vi', '619f00df23d63'),
('product_categories', 30, 'vi', '619f05278a599'),
('posts', 6, 'vi', '619f0b05d4757'),
('posts', 7, 'vi', '619f0b3d06279');

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` bigint(20) DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `caption` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `extention` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `medias`
--

INSERT INTO `medias` (`id`, `user_id`, `name`, `size`, `type`, `title`, `caption`, `extention`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'bitmap-1-copy.jpg', 58113, 'image', '', '', 'jpg', 1, '2021-11-23 09:02:27', '2021-11-23 09:02:27'),
(2, 1, 'bitmap-8-copy.png', 11883, 'image', '', '', 'png', 1, '2021-11-23 09:02:27', '2021-11-23 09:02:27'),
(3, 1, 'bitmap-1.png', 14159, 'image', NULL, NULL, 'png', 1, '2021-11-23 09:02:27', '2021-11-23 09:02:33'),
(4, 1, 'bitmap-1-copy.png', 14159, 'image', '', '', 'png', 1, '2021-11-23 09:02:27', '2021-11-23 09:02:27'),
(5, 1, 'bitmap-3-copy.png', 30113, 'image', '', '', 'png', 1, '2021-11-23 09:02:27', '2021-11-23 09:02:27'),
(6, 1, 'bitmap-1-1.png', 2640, 'image', '', '', 'png', 1, '2021-11-23 09:12:19', '2021-11-23 09:12:19'),
(7, 1, 'bitmap-2.png', 5405, 'image', '', '', 'png', 1, '2021-11-23 09:12:19', '2021-11-23 09:12:19'),
(8, 1, 'bitmap-4.png', 6757, 'image', '', '', 'png', 1, '2021-11-23 09:12:19', '2021-11-23 09:12:19'),
(9, 1, 'bitmap.png', 7439, 'image', '', '', 'png', 1, '2021-11-23 09:12:19', '2021-11-23 09:12:19'),
(10, 1, 'bitmap-3.png', 13519, 'image', '', '', 'png', 1, '2021-11-23 09:12:19', '2021-11-23 09:12:19'),
(11, 1, 'bitmap-copy-34.png', 5467, 'image', '', '', 'png', 1, '2021-11-23 11:10:47', '2021-11-23 11:10:47'),
(12, 1, 'bitmap-copy-26.png', 14159, 'image', NULL, NULL, 'png', 1, '2021-11-23 11:10:47', '2021-11-23 11:11:27'),
(13, 1, 'bitmap-copy-25.png', 19228, 'image', NULL, NULL, 'png', 1, '2021-11-23 11:10:47', '2021-11-23 11:12:42'),
(14, 1, 'bitmap-copy-23.png', 24358, 'image', NULL, NULL, 'png', 1, '2021-11-23 11:10:47', '2021-11-23 11:11:01'),
(15, 1, 'bitmap-copy-33.png', 93388, 'image', '', '', 'png', 1, '2021-11-24 03:34:34', '2021-11-24 03:34:34'),
(16, 1, 'bitmap-7.png', 2400, 'image', '', '', 'png', 1, '2021-11-24 08:27:53', '2021-11-24 08:27:53'),
(17, 1, 'bitmap-4-1.png', 35244, 'image', '', '', 'png', 1, '2021-11-24 11:09:49', '2021-11-24 11:09:49'),
(18, 1, 'bitmap-1-2.png', 49596, 'image', '', '', 'png', 1, '2021-11-24 11:09:49', '2021-11-24 11:09:49'),
(19, 1, 'bitmap-5.png', 44019, 'image', '', '', 'png', 1, '2021-11-24 11:09:49', '2021-11-24 11:09:49'),
(20, 1, 'bitmap-5-1.png', 30113, 'image', '', '', 'png', 1, '2021-11-24 11:09:49', '2021-11-24 11:09:49'),
(21, 1, 'bitmap-3-1.png', 46743, 'image', '', '', 'png', 1, '2021-11-24 11:09:49', '2021-11-24 11:09:49'),
(22, 1, 'bitmap-2-1.png', 45982, 'image', '', '', 'png', 1, '2021-11-24 11:09:49', '2021-11-24 11:09:49'),
(23, 1, 'bitmap-5-2.png', 87946, 'image', '', '', 'png', 1, '2021-11-24 11:16:32', '2021-11-24 11:16:32'),
(24, 1, 'bitmap-7-1.png', 61944, 'image', '', '', 'png', 1, '2021-11-24 11:16:32', '2021-11-24 11:16:32'),
(25, 1, 'bitmap-6.png', 74500, 'image', '', '', 'png', 1, '2021-11-24 11:16:32', '2021-11-24 11:16:32'),
(26, 1, 'bitmap-copy-33-1.png', 93388, 'image', '', '', 'png', 1, '2021-11-24 11:16:33', '2021-11-24 11:16:33');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_06_20_010836_create_medias_table', 1),
(4, '2020_02_25_084144_create_admin_password_resets_table', 1),
(5, '2020_03_13_135637_create_settings_table', 1),
(6, '2020_03_13_135759_create_pins_table', 1),
(7, '2020_03_13_135905_create_verify_emails_table', 1),
(8, '2020_03_13_140241_create_seos_table', 1),
(9, '2020_03_13_140434_create_system_logs_table', 1),
(10, '2020_04_14_110612_create_pages_table', 1),
(11, '2020_04_14_110612_create_slides_table', 1),
(12, '2020_04_16_093723_create_language_metas_table', 1),
(13, '2020_04_20_133257_create_posts_table', 1),
(14, '2020_04_20_133315_create_post_categories_table', 1),
(15, '2020_04_20_133553_create_post_category_maps_table', 1),
(16, '2020_04_22_130047_create_tags_table', 1),
(17, '2020_04_22_130149_create_tag_maps_table', 1),
(18, '2020_05_07_084636_create_sync_links_table', 1),
(19, '2020_05_26_112104_create_comments_table', 1),
(20, '2020_07_06_172847_create_email_registers_table', 1),
(21, '2020_07_08_111814_add_redirect_to_sync_links', 1),
(22, '2020_08_20_145125_create_call_me_back_table', 1),
(23, '2021_02_25_084117_create_admin_users_table', 1),
(24, '2021_04_15_035808_create_brands_table', 1),
(25, '2021_04_15_035835_create_orders_table', 1),
(26, '2021_04_15_035901_create_customers_table', 1),
(27, '2021_04_15_035931_create_shippings_table', 1),
(28, '2021_04_15_040000_create_taxes_table', 1),
(29, '2021_04_15_040032_create_order_details_table', 1),
(30, '2021_04_15_040403_create_attribute_details_table', 1),
(31, '2021_04_15_045425_create_attributes_table', 1),
(32, '2021_04_15_045850_create_google_shoppings_table', 1),
(33, '2021_04_15_063319_create_order_histories_table', 1),
(34, '2021_04_15_074430_create_shipping_rules_table', 1),
(35, '2021_04_20_133257_create_products_table', 1),
(36, '2021_04_20_133315_create_product_categories_table', 1),
(37, '2021_04_20_133553_create_product_category_maps_table', 1),
(38, '2021_05_02_041516_create_contacts_table', 1),
(39, '2021_05_28_072050_create_shipping_provinces_table', 1),
(40, '2021_06_03_020831_create_product_attribute_maps_table', 1),
(41, '2021_06_03_021021_create_attribute_variant_maps_table', 1),
(42, '2021_06_03_025230_create_variants_table', 1),
(43, '2021_06_10_092358_create_product_category_attribute_maps_table', 1),
(44, '2021_06_15_152444_update_order_details_table', 1),
(45, '2021_06_15_164453_update_attributes_table', 1),
(46, '2021_06_16_082519_add_order_to_shipping_rules_table', 1),
(47, '2021_06_17_115842_drop_category_id_products_table', 1),
(48, '2021_09_09_205752_create_solutions_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` int(11) NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` int(11) NOT NULL DEFAULT '0',
  `note` text COLLATE utf8mb4_unicode_ci,
  `payment_method` int(11) NOT NULL DEFAULT '1',
  `payment_status` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `refund_money` int(11) NOT NULL DEFAULT '0',
  `refund_reason` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '1',
  `variant_id` int(11) NOT NULL DEFAULT '0',
  `variant_text` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `order_histories`
--

CREATE TABLE `order_histories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `admin_user_id` int(11) NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_ci,
  `time` timestamp NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pins`
--

CREATE TABLE `pins` (
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `place` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pins`
--

INSERT INTO `pins` (`type`, `type_id`, `place`, `value`) VALUES
('product_categories', 1, 'home', 1),
('product_categories', 4, 'home', 2),
('product_categories', 3, 'home', 3),
('product_categories', 5, 'home', 4),
('product_categories', 13, 'home', 5),
('product_categories', 26, 'home', 6),
('product_categories', 30, 'home', 6);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `admin_user_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `admin_user_id`, `name`, `slug`, `image`, `detail`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Các loại máy hàn điện tử thường gặp trên thị trường', 'cac-loai-may-han-dien-tu-thuong-gap-tren-thi-truong', '/core/2021/11/bitmap-copy-33.png', NULL, -1, '2021-11-24 10:23:53', '2021-11-24 10:23:53'),
(2, 2, 'Giá máy hàn điện tử có đắt không? Cần lưu ý gì khi sử dụng?', 'gia-may-han-dien-tu-co-dat-khong-can-luu-y-gi-khi-su-dung', '/core/2021/11/bitmap-6.png', '<p>Máy mài cầm tay makita 125mm là một trong những dòng máy mài tốt nhất trên thế giới được hãng Makita</p>', 1, '2021-11-24 11:17:03', '2021-11-24 11:17:03'),
(3, 2, 'Các loại máy hàn  thường gặp trên thị trường', 'cac-loai-may-han-thuong-gap-tren-thi-truong', '/core/2021/11/bitmap-5-2.png', '<p>Máy mài cầm tay makita 125mm là một trong những dòng máy mài tốt nhất trên thế giới được hãng Makita</p>', 1, '2021-11-24 11:19:57', '2021-11-24 11:19:57'),
(6, 2, 'Các loại máy hàn điện tử thường gặp trên thị trường trong nước', 'cac-loai-may-han-dien-tu-thuong-gap-tren-thi-truong-trong-nuoc', '/core/2021/11/bitmap-6.png', NULL, 1, '2021-11-25 04:03:17', '2021-11-25 04:03:17');

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

CREATE TABLE `post_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '9999',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_categories`
--

INSERT INTO `post_categories` (`id`, `parent_id`, `name`, `slug`, `image`, `detail`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Kiến thức và kĩ năng', 'kien-thuc-va-ki-nang', NULL, NULL, 9999, -1, '2021-11-24 10:23:33', '2021-11-24 10:23:35'),
(2, 0, 'Kiến Thức và Kỹ Năng', 'kien-thuc-va-ky-nang', NULL, NULL, 9999, 1, '2021-11-24 11:13:50', '2021-11-24 11:13:55');

-- --------------------------------------------------------

--
-- Table structure for table `post_category_maps`
--

CREATE TABLE `post_category_maps` (
  `post_id` int(11) NOT NULL,
  `post_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `post_category_maps`
--

INSERT INTO `post_category_maps` (`post_id`, `post_category_id`) VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 2),
(6, 2),
(7, 2);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `brand_id` int(11) DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `slide` text COLLATE utf8mb4_unicode_ci,
  `price` int(11) DEFAULT NULL,
  `price_old` int(11) DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `related_products` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `wide` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `tax_id` int(11) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `brand_id`, `sku`, `name`, `slug`, `image`, `slide`, `price`, `price_old`, `detail`, `related_products`, `quantity`, `length`, `wide`, `height`, `weight`, `tax_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, '001', 'Máy cưa lọng Dewalt DW341K', 'may-cua-long-dewalt-dw341k', '/core/2021/11/bitmap-copy-34.png', NULL, 2350000, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, 1, '2021-11-23 10:01:32', '2021-11-25 03:20:43'),
(3, 0, '003', 'Máy cưa lọng Dewalt DW341KUTF', 'may-cua-long-dewalt-dw341kutf', '/core/2021/11/bitmap-copy-25.png', NULL, 2350000, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, 1, '2021-11-23 11:13:44', '2021-11-25 03:20:31'),
(4, 1, '004', 'Máy cưa lọng bosch DW341K', 'may-cua-long-bosch-dw341k', '/core/2021/11/bitmap-copy-23.png', NULL, 4050000, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, 1, '2021-11-23 11:14:53', '2021-11-25 03:20:21'),
(6, 0, '006', 'Máy cưa lọng Dewalt DW341K0', 'may-cua-long-dewalt-dw341k0', '/core/2021/11/bitmap-copy-34.png', NULL, 2350000, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, 1, '2021-11-25 03:18:48', '2021-11-25 03:18:51'),
(7, 0, '007', 'Máy cưa lọng Dewalt DW341K1', 'may-cua-long-dewalt-dw341k1', '/core/2021/11/bitmap-5.png', NULL, 2350000, 0, NULL, NULL, 0, 0, 0, 0, 0, NULL, 1, '2021-11-25 03:19:59', '2021-11-25 03:20:01');

-- --------------------------------------------------------

--
-- Table structure for table `product_attribute_maps`
--

CREATE TABLE `product_attribute_maps` (
  `product_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '99999',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `parent_id`, `name`, `slug`, `image`, `detail`, `order`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 'Thiết bị dùng pin', 'thiet-bi-dung-pin', '/core/2021/11/bitmap-5.png', NULL, 99999, 1, '2021-11-23 09:56:49', '2021-11-24 11:10:41'),
(2, 0, 'Thiết bị dùng điện', 'thiet-bi-dung-dien', NULL, NULL, 99999, -1, '2021-11-23 09:57:15', '2021-11-23 09:57:17'),
(3, 0, 'Thiết bị chiếu sáng', 'thiet-bi-chieu-sang', '/core/2021/11/bitmap-5-1.png', NULL, 99999, 1, '2021-11-23 09:57:33', '2021-11-24 15:50:56'),
(4, 0, 'Dụng cụ dùng điện', 'dung-cu-dung-dien', '/core/2021/11/bitmap-2-1.png', NULL, 99999, 1, '2021-11-23 09:58:25', '2021-11-24 15:51:30'),
(5, 0, 'Dụng cụ chạy xăng', 'dung-cu-chay-xang', '/core/2021/11/bitmap-3-1.png', NULL, 99999, 1, '2021-11-23 09:58:51', '2021-11-24 15:51:58'),
(6, 0, 'Dụng cụ cầm tay', 'dung-cu-cam-tay', '/core/2021/11/bitmap-1-2.png', NULL, 99999, -1, '2021-11-23 09:59:18', '2021-11-24 11:12:12'),
(10, 1, 'Máy khoan pin', 'may-co-khi-dien', NULL, NULL, 99999, 1, '2021-11-24 11:28:18', '2021-11-24 15:55:53'),
(11, 1, 'Máy mài dùng pin', 'may-han', NULL, NULL, 99999, 1, '2021-11-24 11:29:42', '2021-11-24 15:56:12'),
(12, 1, 'Máy vặn bu lông dùng pin', 'thiet-bi-den', NULL, NULL, 99999, -1, '2021-11-24 11:39:45', '2021-11-24 15:56:19'),
(13, 0, 'Dụng cụ cầm tay', 'dung-cu-cam-tay-don', '/core/2021/11/bitmap-5.png', NULL, 99999, 1, '2021-11-24 11:40:46', '2021-11-24 15:52:39'),
(14, 4, 'Máy khoan điện', 'dung-cu-khoan', NULL, NULL, 99999, 1, '2021-11-24 11:41:26', '2021-11-24 15:58:10'),
(15, 4, 'Máy mài dùng điện', 'dung-cu-cat', NULL, NULL, 99999, 1, '2021-11-24 11:41:53', '2021-11-24 15:58:47'),
(16, 4, 'Máy Vặn Bu Lông Dùng điện', 'dung-cu-co-khi', '/core/2021/11/bitmap-2-1.png', NULL, 99999, 1, '2021-11-24 11:43:03', '2021-11-24 15:59:23'),
(17, 3, 'Đèn led rạng đông', 'may-cam-tay', NULL, NULL, 99999, 1, '2021-11-24 11:44:39', '2021-11-24 16:00:19'),
(18, 3, 'Đèn led Philip', 'den-led-philip', NULL, NULL, 99999, 1, '2021-11-24 16:01:15', '2021-11-24 16:01:18'),
(19, 3, 'Đèn sợi đốt', 'den-soi-dot', NULL, NULL, 99999, 1, '2021-11-24 16:01:41', '2021-11-24 16:01:43'),
(20, 5, 'Máy cắt chạy xăng', 'may-cat-chay-xang', NULL, NULL, 99999, 1, '2021-11-24 16:02:26', '2021-11-24 16:02:28'),
(21, 5, 'Máy mài chạy xăng', 'may-mai-chay-xang', NULL, NULL, 99999, 1, '2021-11-24 16:02:51', '2021-11-24 16:02:53'),
(22, 5, 'Máy vặn chạy xăng', 'may-van-chay-xang', NULL, NULL, 99999, 1, '2021-11-24 16:03:29', '2021-11-24 16:03:33'),
(23, 13, 'kìm cầm tay', 'kim-cam-tay', NULL, NULL, 99999, 1, '2021-11-24 16:03:52', '2021-11-24 16:03:56'),
(24, 13, 'Tua vít cầm tay', 'tua-vit-cam-tay', NULL, NULL, 99999, 1, '2021-11-24 16:04:29', '2021-11-24 16:04:32'),
(25, 13, 'Kéo cầm tay', 'keo-cam-tay', NULL, NULL, 99999, 1, '2021-11-24 16:04:48', '2021-11-24 16:04:50'),
(29, 26, 'Máy nén dầu', 'may-nen-dau', '/core/2021/11/bitmap-copy-23.png', NULL, 99999, -1, '2021-11-25 03:11:49', '2021-11-25 03:11:51');

-- --------------------------------------------------------

--
-- Table structure for table `product_category_attribute_maps`
--

CREATE TABLE `product_category_attribute_maps` (
  `product_category_id` int(11) NOT NULL,
  `attribute_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `product_category_maps`
--

CREATE TABLE `product_category_maps` (
  `product_id` int(11) NOT NULL,
  `product_category_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_category_maps`
--

INSERT INTO `product_category_maps` (`product_id`, `product_category_id`) VALUES
(2, 1),
(2, 3),
(2, 4),
(2, 5),
(2, 13),
(5, 1),
(5, 3),
(5, 4),
(5, 5),
(5, 13),
(6, 1),
(6, 3),
(6, 4),
(6, 5),
(6, 13),
(6, 26),
(7, 1),
(7, 3),
(7, 4),
(7, 5),
(7, 13),
(7, 26),
(4, 1),
(4, 3),
(4, 4),
(4, 5),
(4, 13),
(4, 26),
(3, 1),
(3, 3),
(3, 4),
(3, 5),
(3, 13),
(3, 26),
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(1, 13),
(1, 26);

-- --------------------------------------------------------

--
-- Table structure for table `seos`
--

CREATE TABLE `seos` (
  `type` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type_id` bigint(20) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `robots` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_title` text COLLATE utf8mb4_unicode_ci,
  `social_description` text COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `seos`
--

INSERT INTO `seos` (`type`, `type_id`, `title`, `description`, `robots`, `social_image`, `social_title`, `social_description`) VALUES
('brands', 1, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 2, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 3, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 4, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 5, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 6, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 7, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 8, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 9, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('brands', 10, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 1, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 2, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 3, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 4, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 5, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 6, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 1, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 2, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 3, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 4, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('post_categories', 1, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 1, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('post_categories', 2, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 2, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 3, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 4, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 5, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 7, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 8, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 9, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 10, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 11, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 12, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 13, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 14, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 15, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 16, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 17, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 18, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 19, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 20, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 21, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 22, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 23, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 24, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 25, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 5, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 26, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 27, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 28, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 29, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 6, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('products', 7, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('product_categories', 30, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 6, NULL, NULL, 'Index,Follow', NULL, NULL, NULL),
('posts', 7, NULL, NULL, 'Index,Follow', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `value` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`key`, `locale`, `value`) VALUES
('sudo', NULL, '86a9106ae65537651a8e456835b316ab'),
('general', 'vi', 'eyJsb2dvX2hlYWRlcl9kZXNrdG9wIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTcucG5nIiwiZmFjZWJvb2siOm51bGwsInlvdXR1YmUiOm51bGwsIndlYiI6bnVsbCwiY29kZSI6IjAxMDYzMzA5MTQiLCJhZHJlIjoiVFx1MWVhN25nIDIsIHNcdTFlZDEgNkEgTFx1MDBlYSBWXHUwMTAzbiBUaGlcdTAwZWFtLCBQLiBUaGFuaCBYdVx1MDBlMm4gVHJ1bmcsIFEuIFRoYW5oIFh1XHUwMGUybiwgSFx1MDBlMCBOXHUxZWQ5aSIsIndhcmVob3VzZSI6IlNcdTFlZDEgNkEgTFx1MDBlYSBWXHUwMTAzbiBUaGlcdTAwZWFtLCBQLiBUaGFuaCBYdVx1MDBlMm4gVHJ1bmcsIFEuIFRoYW5oIFh1XHUwMGUybiwgSFx1MDBlMCBOXHUxZWQ5aSIsInBob25lIjoiMDI0IDIyMjQgODg4OCIsImVtYWlsIjoic3VwcG9ydEBzdXBlci1tcm8uY29tIn0='),
('menu', 'vi', 'eyJtZW51X3N1cGVyIjoiW3tcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIjA1MDEwMDAxMjI3NzdcIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9LHtcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIlRcdTFlYTduZyAyLCBzXHUxZWQxIDZBIExcdTAwZWEgVlx1MDEwM24gVGhpXHUwMGVhbSwgUC4gVGhhbmggWHVcdTAwZTJuIFRydW5nLCBRLiBUaGFuaCBYdVx1MDBlMm4sIEhcdTAwZTAgTlx1MWVkOWlcIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9LHtcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIlNcdTFlZDEgNkEgTFx1MDBlYSBWXHUwMTAzbiBUaGlcdTAwZWFtLCBQLiBUaGFuaCBYdVx1MDBlMm4gVHJ1bmcsIFEuIFRoYW5oIFh1XHUwMGUybiwgSFx1MDBlMCBOXHUxZWQ5aVwiLFwidHlwZVwiOlwiY3VzdG9tX2xpbmtcIn0se1wicmVsXCI6XCJcIixcInRhcmdldFwiOlwiX3NlbGZcIixcImxpbmtcIjpcIlwiLFwibmFtZVwiOlwiMDI0IDIyMjQgODg4OFwiLFwidHlwZVwiOlwiY3VzdG9tX2xpbmtcIn0se1wicmVsXCI6XCJcIixcInRhcmdldFwiOlwiX3NlbGZcIixcImxpbmtcIjpcIlwiLFwibmFtZVwiOlwic3VwcG9ydEBzdXBlci1tcm8uY29tXCIsXCJ0eXBlXCI6XCJjdXN0b21fbGlua1wifV0iLCJtZW51X2luZm8iOiJbe1wicmVsXCI6XCJcIixcInRhcmdldFwiOlwiX3NlbGZcIixcImxpbmtcIjpcIlwiLFwibmFtZVwiOlwiR2lcdTFlZGJpIHRoaVx1MWVjN3Ugdlx1MWVjMSBNUk9cIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9LHtcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIiBTXHUxZWU5IG1cdTFlYzduaCB2XHUwMGUwIHRcdTFlYTdtIG5oXHUwMGVjblwiLFwidHlwZVwiOlwiY3VzdG9tX2xpbmtcIn0se1wicmVsXCI6XCJcIixcInRhcmdldFwiOlwiX3NlbGZcIixcImxpbmtcIjpcIlwiLFwibmFtZVwiOlwiTGlcdTAwZWFuIGhcdTFlYzdcIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9LHtcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIkRvd25sb2FkXCIsXCJ0eXBlXCI6XCJjdXN0b21fbGlua1wifSx7XCJyZWxcIjpcIlwiLFwidGFyZ2V0XCI6XCJfc2VsZlwiLFwibGlua1wiOlwiXCIsXCJuYW1lXCI6XCJUcmFuZyB0aW4gdFx1MWVlOWNcIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9XSIsIm1lbnVfc3VwcG9zdCI6Ilt7XCJyZWxcIjpcIlwiLFwidGFyZ2V0XCI6XCJfc2VsZlwiLFwibGlua1wiOlwiXCIsXCJuYW1lXCI6XCJIXHUwMGVjbmggdGhcdTFlZTljIHRoYW5oIHRvXHUwMGUxblwiLFwidHlwZVwiOlwiY3VzdG9tX2xpbmtcIn0se1wicmVsXCI6XCJcIixcInRhcmdldFwiOlwiX3NlbGZcIixcImxpbmtcIjpcIlwiLFwibmFtZVwiOlwiQ2hcdTAwZWRuaCBzXHUwMGUxY2ggdlx1MWVhZG4gY2h1eVx1MWVjM25cIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9LHtcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIkNoXHUwMGVkbmggc1x1MDBlMWNoIFx1MDExMVx1MWVkNWkgdlx1MDBlMCB0clx1MWVhMyBoXHUwMGUwbmdcIixcInR5cGVcIjpcImN1c3RvbV9saW5rXCJ9LHtcInJlbFwiOlwiXCIsXCJ0YXJnZXRcIjpcIl9zZWxmXCIsXCJsaW5rXCI6XCJcIixcIm5hbWVcIjpcIkNoXHUwMGVkbmggc1x1MDBlMWNoIGJcdTFlYTNvIG1cdTFlYWR0XCIsXCJ0eXBlXCI6XCJjdXN0b21fbGlua1wifSx7XCJyZWxcIjpcIlwiLFwidGFyZ2V0XCI6XCJfc2VsZlwiLFwibGlua1wiOlwiXCIsXCJuYW1lXCI6XCJIXHUwMWIwXHUxZWRibmcgZFx1MWVhYm4gbXVhIGhcdTAwZTBuZ1wiLFwidHlwZVwiOlwiY3VzdG9tX2xpbmtcIn1dIiwibWVudV9hZG1pbiI6Ilt7XCJyZWxcIjpcIlwiLFwidGFyZ2V0XCI6XCJfc2VsZlwiLFwibGlua1wiOlwiXCIsXCJuYW1lXCI6XCJUXHUwMGUwaSBraG9cdTFlYTNuIGNcdTFlZTdhIGJcdTFlYTFuXCIsXCJ0eXBlXCI6XCJjdXN0b21fbGlua1wifSx7XCJyZWxcIjpcIlwiLFwidGFyZ2V0XCI6XCJfc2VsZlwiLFwibGlua1wiOlwiXCIsXCJuYW1lXCI6XCJHaVx1MWVjZiBoXHUwMGUwbmcgXHUwMTExXHUwMGUzIGxcdTAxYjB1XCIsXCJ0eXBlXCI6XCJjdXN0b21fbGlua1wifV0ifQ==');

-- --------------------------------------------------------

--
-- Table structure for table `shippings`
--

CREATE TABLE `shippings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `province_id` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_provinces`
--

CREATE TABLE `shipping_provinces` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `shipping_rules`
--

CREATE TABLE `shipping_rules` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shipping_id` int(11) NOT NULL DEFAULT '0',
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from` int(11) NOT NULL DEFAULT '0',
  `to` int(11) NOT NULL DEFAULT '0',
  `price` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '999'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `slides`
--

CREATE TABLE `slides` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `orders` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `slides`
--

INSERT INTO `slides` (`id`, `title`, `link`, `image`, `description`, `orders`, `status`, `created_at`, `updated_at`) VALUES
(1, 'tttt', 'sdsf', '/core/2021/11/bitmap-1-copy.png', NULL, 0, 1, '2021-11-23 09:03:17', '2021-11-23 09:03:20');

-- --------------------------------------------------------

--
-- Table structure for table `solutions`
--

CREATE TABLE `solutions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sync_links`
--

CREATE TABLE `sync_links` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `old` text COLLATE utf8mb4_unicode_ci,
  `new` text COLLATE utf8mb4_unicode_ci,
  `code` text COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `system_logs`
--

CREATE TABLE `system_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `admin_id` int(11) NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time` timestamp NOT NULL,
  `action` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type_id` int(11) NOT NULL DEFAULT '0',
  `detail` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `system_logs`
--

INSERT INTO `system_logs` (`id`, `admin_id`, `ip`, `time`, `action`, `type`, `type_id`, `detail`) VALUES
(1, 1, '127.0.0.1', '2021-11-23 08:57:26', 'login', NULL, 0, NULL),
(2, 1, '127.0.0.1', '2021-11-23 09:03:17', 'create', 'slides', 1, 'eyJmaWVsZHMiOlsidGl0bGUiLCJsaW5rIiwiaW1hZ2UiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7InRpdGxlIjoidHR0dCIsImxpbmsiOiJzZHNmIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMS1jb3B5LnBuZyIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjowMzoxNyIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjAzOjE3In19'),
(3, 1, '127.0.0.1', '2021-11-23 09:12:46', 'create', 'brands', 1, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImZsdWtlIiwic2x1ZyI6ImZsdWtlIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjoxMjo0NiIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjEyOjQ2In19'),
(4, 1, '127.0.0.1', '2021-11-23 09:13:10', 'create', 'brands', 2, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImRld2FsdCIsInNsdWciOiJkZXdhbHQiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0yLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjoxMzoxMCIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjEzOjEwIn19'),
(5, 1, '127.0.0.1', '2021-11-23 09:13:30', 'create', 'brands', 3, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImJvc2NoIiwic2x1ZyI6ImJvc2NoIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNC5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6MTM6MzAiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjoxMzozMCJ9fQ=='),
(6, 1, '127.0.0.1', '2021-11-23 09:13:49', 'create', 'brands', 4, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImFzYWtpIiwic2x1ZyI6ImFzYWtpIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjEzOjQ5IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6MTM6NDkifX0='),
(7, 1, '127.0.0.1', '2021-11-23 09:14:09', 'create', 'brands', 5, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6Im5pa2F3YSIsInNsdWciOiJuaWthd2EiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjoxNDowOSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjE0OjA5In19'),
(8, 1, '127.0.0.1', '2021-11-23 09:37:11', 'create', 'brands', 6, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6Indpa2kiLCJzbHVnIjoid2lraSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjM3OjExIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6Mzc6MTEifX0='),
(9, 1, '127.0.0.1', '2021-11-23 09:37:30', 'create', 'brands', 7, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImJvIiwic2x1ZyI6ImJvIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNC5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6Mzc6MzAiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjozNzozMCJ9fQ=='),
(10, 1, '127.0.0.1', '2021-11-23 09:37:48', 'create', 'brands', 8, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImZsIiwic2x1ZyI6ImZsIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjozNzo0OCIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjM3OjQ4In19'),
(11, 1, '127.0.0.1', '2021-11-23 09:38:02', 'create', 'brands', 9, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6ImRlIiwic2x1ZyI6ImRlIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMi5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6Mzg6MDIiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjozODowMiJ9fQ=='),
(12, 1, '127.0.0.1', '2021-11-23 09:38:18', 'create', 'brands', 10, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6Im5pIiwic2x1ZyI6Im5pIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMy5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6Mzg6MTgiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjozODoxOCJ9fQ=='),
(13, 1, '127.0.0.1', '2021-11-23 09:56:49', 'create', 'product_categories', 1, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJ0aGlldC1iaS1kdW5nLXBpbiIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjU2OjQ5IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6NTY6NDkifX0='),
(14, 1, '127.0.0.1', '2021-11-23 09:57:15', 'create', 'product_categories', 2, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgZFx1MDBmOW5nIFx1MDExMWlcdTFlYzduIiwic2x1ZyI6InRoaWV0LWJpLWR1bmctZGllbiIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjU3OjE1IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6NTc6MTUifX0='),
(15, 1, '127.0.0.1', '2021-11-23 09:57:33', 'create', 'product_categories', 3, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgY2hpXHUxZWJmdSBzXHUwMGUxbmciLCJzbHVnIjoidGhpZXQtYmktY2hpZXUtc2FuZyIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjU3OjMzIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6NTc6MzMifX0='),
(16, 1, '127.0.0.1', '2021-11-23 09:58:11', 'quick_delete', 'product_categories', 2, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(17, 1, '127.0.0.1', '2021-11-23 09:58:25', 'create', 'product_categories', 4, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBkXHUwMGY5bmcgXHUwMTExaVx1MWVjN24iLCJzbHVnIjoiZHVuZy1jdS1kdW5nLWRpZW4iLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjo1ODoyNSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjU4OjI1In19'),
(18, 1, '127.0.0.1', '2021-11-23 09:58:51', 'create', 'product_categories', 5, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBjaFx1MWVhMXkgeFx1MDEwM25nIiwic2x1ZyI6ImR1bmctY3UtY2hheS14YW5nIiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6NTg6NTEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjo1ODo1MSJ9fQ=='),
(19, 1, '127.0.0.1', '2021-11-23 09:59:18', 'create', 'product_categories', 6, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBjXHUxZWE3bSB0YXkiLCJzbHVnIjoiZHVuZy1jdS1jYW0tdGF5IiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6NTk6MTgiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjo1OToxOCJ9fQ=='),
(20, 1, '127.0.0.1', '2021-11-23 10:01:32', 'create', 'products', 1, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwMSIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWIwYSBsXHUxZWNkbmcgRGV3YWx0IERXMzQxSyIsInNsdWciOiJtYXktY3VhLWxvbmctZGV3YWx0LWR3MzQxayIsImltYWdlIjoiIiwic2xpZGUiOiIiLCJwcmljZSI6IjIzNTAwMDAiLCJwcmljZV9vbGQiOiIwIiwiZGV0YWlsIjoiIiwicmVsYXRlZF9wcm9kdWN0cyI6IiIsInF1YW50aXR5IjoiMCIsImxlbmd0aCI6IjAiLCJ3aWRlIjoiMCIsImhlaWdodCI6IjAiLCJ3ZWlnaHQiOiIwIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE3OjAxOjMyIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTc6MDE6MzIifX0='),
(21, 1, '127.0.0.1', '2021-11-23 11:11:11', 'update', 'products', 1, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJicmFuZF9pZCI6IjAiLCJza3UiOiIwMDEiLCJuYW1lIjoiTVx1MDBlMXkgY1x1MDFiMGEgbFx1MWVjZG5nIERld2FsdCBEVzM0MUsiLCJzbHVnIjoibWF5LWN1YS1sb25nLWRld2FsdC1kdzM0MWsiLCJpbWFnZSI6IiIsInNsaWRlIjoiIiwicHJpY2UiOiIyMzUwMDAwIiwicHJpY2Vfb2xkIjoiMCIsImRldGFpbCI6IiIsInJlbGF0ZWRfcHJvZHVjdHMiOiIiLCJxdWFudGl0eSI6IjAiLCJsZW5ndGgiOiIwIiwid2lkZSI6IjAiLCJoZWlnaHQiOiIwIiwid2VpZ2h0IjoiMCIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNzowMTozNCJ9LCJuZXciOnsiYnJhbmRfaWQiOiIwIiwic2t1IjoiMDAxIiwibmFtZSI6Ik1cdTAwZTF5IGNcdTAxYjBhIGxcdTFlY2RuZyBEZXdhbHQgRFczNDFLIiwic2x1ZyI6Im1heS1jdWEtbG9uZy1kZXdhbHQtZHczNDFrIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtY29weS0zNC5wbmciLCJzbGlkZSI6IiIsInByaWNlIjoiMjM1MDAwMCIsInByaWNlX29sZCI6IjAiLCJkZXRhaWwiOiIiLCJyZWxhdGVkX3Byb2R1Y3RzIjoiIiwicXVhbnRpdHkiOiIwIiwibGVuZ3RoIjoiMCIsIndpZGUiOiIwIiwiaGVpZ2h0IjoiMCIsIndlaWdodCI6IjAiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTg6MTE6MTEifX0='),
(22, 1, '127.0.0.1', '2021-11-23 11:12:29', 'create', 'products', 2, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwMiIsIm5hbWUiOiJNXHUwMGUxeSBraG9hbiB2XHUxZWI3biB2XHUwMGVkdCBcdTAxMTFcdTFlZDluZyBsXHUxZWYxYyBkXHUwMGY5bmcgcGluIERld2FsdCBEQ0Q3NzhEMiAxOFYiLCJzbHVnIjoibWF5LWtob2FuLXZhbi12aXQtZG9uZy1sdWMtZHVuZy1waW4tZGV3YWx0LWRjZDc3OGQyLTE4diIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMjYucG5nIiwic2xpZGUiOiIiLCJwcmljZSI6IjQwNTAwMDAiLCJwcmljZV9vbGQiOiIwIiwiZGV0YWlsIjoiIiwicmVsYXRlZF9wcm9kdWN0cyI6IiIsInF1YW50aXR5IjoiMCIsImxlbmd0aCI6IjAiLCJ3aWRlIjoiMCIsImhlaWdodCI6IjAiLCJ3ZWlnaHQiOiIwIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE4OjEyOjI5IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTg6MTI6MjkifX0='),
(23, 1, '127.0.0.1', '2021-11-23 11:13:44', 'create', 'products', 3, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwMyIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWIwYSBsXHUxZWNkbmcgRGV3YWx0IERXMzQxS1VURiIsInNsdWciOiJtYXktY3VhLWxvbmctZGV3YWx0LWR3MzQxa3V0ZiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMjUucG5nIiwic2xpZGUiOiIiLCJwcmljZSI6IjIzNTAwMDAiLCJwcmljZV9vbGQiOiIwIiwiZGV0YWlsIjoiIiwicmVsYXRlZF9wcm9kdWN0cyI6IiIsInF1YW50aXR5IjoiMCIsImxlbmd0aCI6IjAiLCJ3aWRlIjoiMCIsImhlaWdodCI6IjAiLCJ3ZWlnaHQiOiIwIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTIzIDE4OjEzOjQ0IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTg6MTM6NDQifX0='),
(24, 1, '127.0.0.1', '2021-11-23 11:14:53', 'create', 'products', 4, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwNCIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWIwYSBsXHUxZWNkbmcgYm9zY2ggRFczNDFLIiwic2x1ZyI6Im1heS1jdWEtbG9uZy1ib3NjaC1kdzM0MWsiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTIzLnBuZyIsInNsaWRlIjoiIiwicHJpY2UiOiI0MDUwMDAwIiwicHJpY2Vfb2xkIjoiMCIsImRldGFpbCI6IiIsInJlbGF0ZWRfcHJvZHVjdHMiOiIiLCJxdWFudGl0eSI6IjAiLCJsZW5ndGgiOiIwIiwid2lkZSI6IjAiLCJoZWlnaHQiOiIwIiwid2VpZ2h0IjoiMCIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yMyAxODoxNDo1MyIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE4OjE0OjUzIn19'),
(25, 1, '127.0.0.1', '2021-11-24 10:04:29', 'update', 'product_categories', 1, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIGRcdTAwZjluZyBwaW4iLCJzbHVnIjoidGhpZXQtYmktZHVuZy1waW4iLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjo1Njo1MSJ9LCJuZXciOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJ0aGlldC1iaS1kdW5nLXBpbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE3OjA0OjI5In19'),
(26, 1, '127.0.0.1', '2021-11-24 10:23:33', 'create', 'post_categories', 1, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJLaVx1MWViZm4gdGhcdTFlZTljIHZcdTAwZTAga1x1MDEyOSBuXHUwMTAzbmciLCJzbHVnIjoia2llbi10aHVjLXZhLWtpLW5hbmciLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAxNzoyMzozMyIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE3OjIzOjMzIn19'),
(27, 1, '127.0.0.1', '2021-11-24 10:23:53', 'create', 'posts', 1, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6IkNcdTAwZTFjIGxvXHUxZWExaSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIHRoXHUwMWIwXHUxZWRkbmcgZ1x1MWViN3AgdHJcdTAwZWFuIHRoXHUxZWNiIHRyXHUwMWIwXHUxZWRkbmciLCJzbHVnIjoiY2FjLWxvYWktbWF5LWhhbi1kaWVuLXR1LXRodW9uZy1nYXAtdHJlbi10aGktdHJ1b25nIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtY29weS0zMy5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTc6MjM6NTMiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxNzoyMzo1MyJ9fQ=='),
(28, 1, '127.0.0.1', '2021-11-24 10:23:55', 'update', 'posts', 1, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJDXHUwMGUxYyBsb1x1MWVhMWkgbVx1MDBlMXkgaFx1MDBlMG4gXHUwMTExaVx1MWVjN24gdFx1MWVlZCB0aFx1MDFiMFx1MWVkZG5nIGdcdTFlYjdwIHRyXHUwMGVhbiB0aFx1MWVjYiB0clx1MDFiMFx1MWVkZG5nIiwic2x1ZyI6ImNhYy1sb2FpLW1heS1oYW4tZGllbi10dS10aHVvbmctZ2FwLXRyZW4tdGhpLXRydW9uZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE3OjIzOjUzIn0sIm5ldyI6eyJhZG1pbl91c2VyX2lkIjoiMiIsIm5hbWUiOiJDXHUwMGUxYyBsb1x1MWVhMWkgbVx1MDBlMXkgaFx1MDBlMG4gXHUwMTExaVx1MWVjN24gdFx1MWVlZCB0aFx1MDFiMFx1MWVkZG5nIGdcdTFlYjdwIHRyXHUwMGVhbiB0aFx1MWVjYiB0clx1MDFiMFx1MWVkZG5nIiwic2x1ZyI6ImNhYy1sb2FpLW1heS1oYW4tZGllbi10dS10aHVvbmctZ2FwLXRyZW4tdGhpLXRydW9uZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE3OjIzOjUzIn19'),
(29, 1, '127.0.0.1', '2021-11-24 11:10:04', 'update', 'product_categories', 1, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIGRcdTAwZjluZyBwaW4iLCJzbHVnIjoidGhpZXQtYmktZHVuZy1waW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTMzLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxNzowNDozMCJ9LCJuZXciOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJ0aGlldC1iaS1kdW5nLXBpbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MTA6MDQifX0='),
(30, 1, '127.0.0.1', '2021-11-24 11:10:41', 'update', 'product_categories', 1, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIGRcdTAwZjluZyBwaW4iLCJzbHVnIjoidGhpZXQtYmktZHVuZy1waW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjEwOjA3In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IlRoaVx1MWViZnQgYlx1MWVjYiBkXHUwMGY5bmcgcGluIiwic2x1ZyI6InRoaWV0LWJpLWR1bmctcGluIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MTA6NDEifX0='),
(31, 1, '127.0.0.1', '2021-11-24 11:11:13', 'update', 'product_categories', 3, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIGNoaVx1MWViZnUgc1x1MDBlMW5nIiwic2x1ZyI6InRoaWV0LWJpLWNoaWV1LXNhbmciLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yMyAxNjo1NzozNSJ9LCJuZXciOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgY2hpXHUxZWJmdSBzXHUwMGUxbmciLCJzbHVnIjoidGhpZXQtYmktY2hpZXUtc2FuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MTE6MTMifX0='),
(32, 1, '127.0.0.1', '2021-11-24 11:11:39', 'update', 'product_categories', 4, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgZFx1MDBmOW5nIFx1MDExMWlcdTFlYzduIiwic2x1ZyI6ImR1bmctY3UtZHVuZy1kaWVuIiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTY6NTg6MjcifSwibmV3Ijp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgZFx1MDBmOW5nIFx1MDExMWlcdTFlYzduIiwic2x1ZyI6ImR1bmctY3UtZHVuZy1kaWVuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMi0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoxMTozOSJ9fQ=='),
(33, 1, '127.0.0.1', '2021-11-24 11:11:54', 'update', 'product_categories', 5, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY2hcdTFlYTF5IHhcdTAxMDNuZyIsInNsdWciOiJkdW5nLWN1LWNoYXkteGFuZyIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjU4OjU0In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNoXHUxZWExeSB4XHUwMTAzbmciLCJzbHVnIjoiZHVuZy1jdS1jaGF5LXhhbmciLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjExOjU0In19'),
(34, 1, '127.0.0.1', '2021-11-24 11:12:13', 'update', 'product_categories', 6, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MWVhN20gdGF5Iiwic2x1ZyI6ImR1bmctY3UtY2FtLXRheSIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTIzIDE2OjU5OjIxIn0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNcdTFlYTdtIHRheSIsInNsdWciOiJkdW5nLWN1LWNhbS10YXkiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0xLTIucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjEyOjEyIn19'),
(35, 1, '127.0.0.1', '2021-11-24 11:13:01', 'quick_delete', 'post_categories', 1, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(36, 1, '127.0.0.1', '2021-11-24 11:13:50', 'create', 'post_categories', 2, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJLaVx1MWViZm4gVGhcdTFlZTljIHZcdTAwZTAgS1x1MWVmOSBOXHUwMTAzbmciLCJzbHVnIjoia2llbi10aHVjLXZhLWt5LW5hbmciLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjAiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoxMzo1MCIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjEzOjUwIn19'),
(37, 1, '127.0.0.1', '2021-11-24 11:14:11', 'quick_delete', 'posts', 1, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(38, 1, '127.0.0.1', '2021-11-24 11:14:47', 'quick_update', 'post_categories', 2, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIwIn0sIm5ldyI6eyJzdGF0dXMiOiIxIn19'),
(39, 1, '127.0.0.1', '2021-11-24 11:17:03', 'create', 'posts', 2, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6IkdpXHUwMGUxIG1cdTAwZTF5IGhcdTAwZTBuIFx1MDExMWlcdTFlYzduIHRcdTFlZWQgY1x1MDBmMyBcdTAxMTFcdTFlYWZ0IGtoXHUwMGY0bmc/IENcdTFlYTduIGxcdTAxYjB1IFx1MDBmZCBnXHUwMGVjIGtoaSBzXHUxZWVkIGRcdTFlZTVuZz8iLCJzbHVnIjoiZ2lhLW1heS1oYW4tZGllbi10dS1jby1kYXQta2hvbmctY2FuLWx1dS15LWdpLWtoaS1zdS1kdW5nIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNi5wbmciLCJkZXRhaWwiOiI8cD5NXHUwMGUxeSBtXHUwMGUwaSBjXHUxZWE3bSB0YXkgbWFraXRhIDEyNW1tIGxcdTAwZTAgbVx1MWVkOXQgdHJvbmcgbmhcdTFlZWZuZyBkXHUwMGYybmcgbVx1MDBlMXkgbVx1MDBlMGkgdFx1MWVkMXQgbmhcdTFlYTV0IHRyXHUwMGVhbiB0aFx1MWViZiBnaVx1MWVkYmkgXHUwMTExXHUwMWIwXHUxZWUzYyBoXHUwMGUzbmcgTWFraXRhPFwvcD4iLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MTc6MDMiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoxNzowMyJ9fQ=='),
(40, 1, '127.0.0.1', '2021-11-24 11:17:06', 'update', 'posts', 2, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJHaVx1MDBlMSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIGNcdTAwZjMgXHUwMTExXHUxZWFmdCBraFx1MDBmNG5nPyBDXHUxZWE3biBsXHUwMWIwdSBcdTAwZmQgZ1x1MDBlYyBraGkgc1x1MWVlZCBkXHUxZWU1bmc/Iiwic2x1ZyI6ImdpYS1tYXktaGFuLWRpZW4tdHUtY28tZGF0LWtob25nLWNhbi1sdXUteS1naS1raGktc3UtZHVuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTYucG5nIiwiZGV0YWlsIjoiPHA+TVx1MDBlMXkgbVx1MDBlMGkgY1x1MWVhN20gdGF5IG1ha2l0YSAxMjVtbSBsXHUwMGUwIG1cdTFlZDl0IHRyb25nIG5oXHUxZWVmbmcgZFx1MDBmMm5nIG1cdTAwZTF5IG1cdTAwZTBpIHRcdTFlZDF0IG5oXHUxZWE1dCB0clx1MDBlYW4gdGhcdTFlYmYgZ2lcdTFlZGJpIFx1MDExMVx1MDFiMFx1MWVlM2MgaFx1MDBlM25nIE1ha2l0YTxcL3A+Iiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjE3OjAzIn0sIm5ldyI6eyJhZG1pbl91c2VyX2lkIjoiMiIsIm5hbWUiOiJHaVx1MDBlMSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIGNcdTAwZjMgXHUwMTExXHUxZWFmdCBraFx1MDBmNG5nPyBDXHUxZWE3biBsXHUwMWIwdSBcdTAwZmQgZ1x1MDBlYyBraGkgc1x1MWVlZCBkXHUxZWU1bmc/Iiwic2x1ZyI6ImdpYS1tYXktaGFuLWRpZW4tdHUtY28tZGF0LWtob25nLWNhbi1sdXUteS1naS1raGktc3UtZHVuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTYucG5nIiwiZGV0YWlsIjoiPHA+TVx1MDBlMXkgbVx1MDBlMGkgY1x1MWVhN20gdGF5IG1ha2l0YSAxMjVtbSBsXHUwMGUwIG1cdTFlZDl0IHRyb25nIG5oXHUxZWVmbmcgZFx1MDBmMm5nIG1cdTAwZTF5IG1cdTAwZTBpIHRcdTFlZDF0IG5oXHUxZWE1dCB0clx1MDBlYW4gdGhcdTFlYmYgZ2lcdTFlZGJpIFx1MDExMVx1MDFiMFx1MWVlM2MgaFx1MDBlM25nIE1ha2l0YTxcL3A+Iiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjE3OjAzIn19'),
(41, 1, '127.0.0.1', '2021-11-24 11:19:57', 'create', 'posts', 3, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6IkNcdTAwZTFjIGxvXHUxZWExaSBtXHUwMGUxeSBoXHUwMGUwbiAgdGhcdTAxYjBcdTFlZGRuZyBnXHUxZWI3cCB0clx1MDBlYW4gdGhcdTFlY2IgdHJcdTAxYjBcdTFlZGRuZyIsInNsdWciOiJjYWMtbG9haS1tYXktaGFuLXRodW9uZy1nYXAtdHJlbi10aGktdHJ1b25nIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0yLnBuZyIsImRldGFpbCI6IjxwPk1cdTAwZTF5IG1cdTAwZTBpIGNcdTFlYTdtIHRheSBtYWtpdGEgMTI1bW0gbFx1MDBlMCBtXHUxZWQ5dCB0cm9uZyBuaFx1MWVlZm5nIGRcdTAwZjJuZyBtXHUwMGUxeSBtXHUwMGUwaSB0XHUxZWQxdCBuaFx1MWVhNXQgdHJcdTAwZWFuIHRoXHUxZWJmIGdpXHUxZWRiaSBcdTAxMTFcdTAxYjBcdTFlZTNjIGhcdTAwZTNuZyBNYWtpdGE8XC9wPiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoxOTo1NyIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjE5OjU3In19'),
(42, 1, '127.0.0.1', '2021-11-24 11:19:59', 'update', 'posts', 3, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJDXHUwMGUxYyBsb1x1MWVhMWkgbVx1MDBlMXkgaFx1MDBlMG4gIHRoXHUwMWIwXHUxZWRkbmcgZ1x1MWViN3AgdHJcdTAwZWFuIHRoXHUxZWNiIHRyXHUwMWIwXHUxZWRkbmciLCJzbHVnIjoiY2FjLWxvYWktbWF5LWhhbi10aHVvbmctZ2FwLXRyZW4tdGhpLXRydW9uZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUtMi5wbmciLCJkZXRhaWwiOiI8cD5NXHUwMGUxeSBtXHUwMGUwaSBjXHUxZWE3bSB0YXkgbWFraXRhIDEyNW1tIGxcdTAwZTAgbVx1MWVkOXQgdHJvbmcgbmhcdTFlZWZuZyBkXHUwMGYybmcgbVx1MDBlMXkgbVx1MDBlMGkgdFx1MWVkMXQgbmhcdTFlYTV0IHRyXHUwMGVhbiB0aFx1MWViZiBnaVx1MWVkYmkgXHUwMTExXHUwMWIwXHUxZWUzYyBoXHUwMGUzbmcgTWFraXRhPFwvcD4iLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MTk6NTcifSwibmV3Ijp7ImFkbWluX3VzZXJfaWQiOiIyIiwibmFtZSI6IkNcdTAwZTFjIGxvXHUxZWExaSBtXHUwMGUxeSBoXHUwMGUwbiAgdGhcdTAxYjBcdTFlZGRuZyBnXHUxZWI3cCB0clx1MDBlYW4gdGhcdTFlY2IgdHJcdTAxYjBcdTFlZGRuZyIsInNsdWciOiJjYWMtbG9haS1tYXktaGFuLXRodW9uZy1nYXAtdHJlbi10aGktdHJ1b25nIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0yLnBuZyIsImRldGFpbCI6IjxwPk1cdTAwZTF5IG1cdTAwZTBpIGNcdTFlYTdtIHRheSBtYWtpdGEgMTI1bW0gbFx1MDBlMCBtXHUxZWQ5dCB0cm9uZyBuaFx1MWVlZm5nIGRcdTAwZjJuZyBtXHUwMGUxeSBtXHUwMGUwaSB0XHUxZWQxdCBuaFx1MWVhNXQgdHJcdTAwZWFuIHRoXHUxZWJmIGdpXHUxZWRiaSBcdTAxMTFcdTAxYjBcdTFlZTNjIGhcdTAwZTNuZyBNYWtpdGE8XC9wPiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoxOTo1NyJ9fQ=='),
(43, 1, '127.0.0.1', '2021-11-24 11:20:28', 'create', 'posts', 4, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6Ik1cdTAwZTF5IG1cdTAwZTBpIGNcdTFlYTdtIHRheSBtYWtpdGEgMTI1bW0gbFx1MDBlMCBtXHUxZWQ5dCB0cm9uZyBuaFx1MWVlZm5nIGRcdTAwZjJuZyBtXHUwMGUxeSBtXHUwMGUwaSB0XHUxZWQxdCBuaFx1MWVhNXQgdHJcdTAwZWFuIHRoXHUxZWJmIGdpXHUxZWRiaSBcdTAxMTFcdTAxYjBcdTFlZTNjIGhcdTAwZTNuZyBNYWtpdGEiLCJzbHVnIjoibWF5LW1haS1jYW0tdGF5LW1ha2l0YS0xMjVtbS1sYS1tb3QtdHJvbmctbmh1bmctZG9uZy1tYXktbWFpLXRvdC1uaGF0LXRyZW4tdGhlLWdpb2ktZHVvYy1oYW5nLW1ha2l0YSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MjA6MjgiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoyMDoyOCJ9fQ=='),
(44, 1, '127.0.0.1', '2021-11-24 11:20:30', 'update', 'posts', 4, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBjXHUxZWE3bSB0YXkgbWFraXRhIDEyNW1tIGxcdTAwZTAgbVx1MWVkOXQgdHJvbmcgbmhcdTFlZWZuZyBkXHUwMGYybmcgbVx1MDBlMXkgbVx1MDBlMGkgdFx1MWVkMXQgbmhcdTFlYTV0IHRyXHUwMGVhbiB0aFx1MWViZiBnaVx1MWVkYmkgXHUwMTExXHUwMWIwXHUxZWUzYyBoXHUwMGUzbmcgTWFraXRhIiwic2x1ZyI6Im1heS1tYWktY2FtLXRheS1tYWtpdGEtMTI1bW0tbGEtbW90LXRyb25nLW5odW5nLWRvbmctbWF5LW1haS10b3QtbmhhdC10cmVuLXRoZS1naW9pLWR1b2MtaGFuZy1tYWtpdGEiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTMzLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjIwOjI4In0sIm5ldyI6eyJhZG1pbl91c2VyX2lkIjoiMiIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBjXHUxZWE3bSB0YXkgbWFraXRhIDEyNW1tIGxcdTAwZTAgbVx1MWVkOXQgdHJvbmcgbmhcdTFlZWZuZyBkXHUwMGYybmcgbVx1MDBlMXkgbVx1MDBlMGkgdFx1MWVkMXQgbmhcdTFlYTV0IHRyXHUwMGVhbiB0aFx1MWViZiBnaVx1MWVkYmkgXHUwMTExXHUwMWIwXHUxZWUzYyBoXHUwMGUzbmcgTWFraXRhIiwic2x1ZyI6Im1heS1tYWktY2FtLXRheS1tYWtpdGEtMTI1bW0tbGEtbW90LXRyb25nLW5odW5nLWRvbmctbWF5LW1haS10b3QtbmhhdC10cmVuLXRoZS1naW9pLWR1b2MtaGFuZy1tYWtpdGEiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTMzLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjIwOjI4In19'),
(45, 1, '127.0.0.1', '2021-11-24 11:21:29', 'create', 'posts', 5, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6Ik1cdTAwZTF5IG1cdTAwZTBpIG1ha2l0YSAxMjVtbSBsXHUwMGUwIG1cdTFlZDl0IHRyb25nIG5oXHUxZWVmbmcgZFx1MDBmMm5nIG1cdTAwZTF5IG1cdTAwZTBpIHRcdTFlZDF0IG5oXHUxZWE1dCB0clx1MDBlYW4gdGhcdTFlYmYgZ2lcdTFlZGJpIFx1MDExMVx1MDFiMFx1MWVlM2MgaFx1MDBlM25nIE1ha2l0YSIsInNsdWciOiJtYXktbWFpLW1ha2l0YS0xMjVtbS1sYS1tb3QtdHJvbmctbmh1bmctZG9uZy1tYXktbWFpLXRvdC1uaGF0LXRyZW4tdGhlLWdpb2ktZHVvYy1oYW5nLW1ha2l0YSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMtMS5wbmciLCJkZXRhaWwiOiI8cD5NXHUwMGUxeSBtXHUwMGUwaSBjXHUxZWE3bSB0YXkgbWFraXRhIDEyNW1tIGxcdTAwZTAgbVx1MWVkOXQgdHJvbmcgbmhcdTFlZWZuZyBkXHUwMGYybmcgbVx1MDBlMXkgbVx1MDBlMGkgdFx1MWVkMXQgbmhcdTFlYTV0IHRyXHUwMGVhbiB0aFx1MWViZiBnaVx1MWVkYmkgXHUwMTExXHUwMWIwXHUxZWUzYyBoXHUwMGUzbmcgTWFraXRhPFwvcD4iLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MjE6MjkiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoyMToyOSJ9fQ=='),
(46, 1, '127.0.0.1', '2021-11-24 11:21:33', 'update', 'posts', 5, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBtYWtpdGEgMTI1bW0gbFx1MDBlMCBtXHUxZWQ5dCB0cm9uZyBuaFx1MWVlZm5nIGRcdTAwZjJuZyBtXHUwMGUxeSBtXHUwMGUwaSB0XHUxZWQxdCBuaFx1MWVhNXQgdHJcdTAwZWFuIHRoXHUxZWJmIGdpXHUxZWRiaSBcdTAxMTFcdTAxYjBcdTFlZTNjIGhcdTAwZTNuZyBNYWtpdGEiLCJzbHVnIjoibWF5LW1haS1tYWtpdGEtMTI1bW0tbGEtbW90LXRyb25nLW5odW5nLWRvbmctbWF5LW1haS10b3QtbmhhdC10cmVuLXRoZS1naW9pLWR1b2MtaGFuZy1tYWtpdGEiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTMzLTEucG5nIiwiZGV0YWlsIjoiPHA+TVx1MDBlMXkgbVx1MDBlMGkgY1x1MWVhN20gdGF5IG1ha2l0YSAxMjVtbSBsXHUwMGUwIG1cdTFlZDl0IHRyb25nIG5oXHUxZWVmbmcgZFx1MDBmMm5nIG1cdTAwZTF5IG1cdTAwZTBpIHRcdTFlZDF0IG5oXHUxZWE1dCB0clx1MDBlYW4gdGhcdTFlYmYgZ2lcdTFlZGJpIFx1MDExMVx1MDFiMFx1MWVlM2MgaFx1MDBlM25nIE1ha2l0YTxcL3A+Iiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjIxOjI5In0sIm5ldyI6eyJhZG1pbl91c2VyX2lkIjoiMiIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBtYWtpdGEgMTI1bW0gbFx1MDBlMCBtXHUxZWQ5dCB0cm9uZyBuaFx1MWVlZm5nIGRcdTAwZjJuZyBtXHUwMGUxeSBtXHUwMGUwaSB0XHUxZWQxdCBuaFx1MWVhNXQgdHJcdTAwZWFuIHRoXHUxZWJmIGdpXHUxZWRiaSBcdTAxMTFcdTAxYjBcdTFlZTNjIGhcdTAwZTNuZyBNYWtpdGEiLCJzbHVnIjoibWF5LW1haS1tYWtpdGEtMTI1bW0tbGEtbW90LXRyb25nLW5odW5nLWRvbmctbWF5LW1haS10b3QtbmhhdC10cmVuLXRoZS1naW9pLWR1b2MtaGFuZy1tYWtpdGEiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTMzLTEucG5nIiwiZGV0YWlsIjoiPHA+TVx1MDBlMXkgbVx1MDBlMGkgY1x1MWVhN20gdGF5IG1ha2l0YSAxMjVtbSBsXHUwMGUwIG1cdTFlZDl0IHRyb25nIG5oXHUxZWVmbmcgZFx1MDBmMm5nIG1cdTAwZTF5IG1cdTAwZTBpIHRcdTFlZDF0IG5oXHUxZWE1dCB0clx1MDBlYW4gdGhcdTFlYmYgZ2lcdTFlZGJpIFx1MDExMVx1MDFiMFx1MWVlM2MgaFx1MDBlM25nIE1ha2l0YTxcL3A+Iiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjIxOjI5In19'),
(47, 1, '127.0.0.1', '2021-11-24 11:23:40', 'create', 'product_categories', 7, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJQaFx1MWVlNSBraVx1MWVjN24gZFx1MDBmOW5nIHBpbiIsInNsdWciOiJwaHUta2llbi1kdW5nLXBpbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjIzOjQwIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MjM6NDAifX0='),
(48, 1, '127.0.0.1', '2021-11-24 11:24:03', 'quick_delete', 'product_categories', 7, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(49, 1, '127.0.0.1', '2021-11-24 11:25:29', 'create', 'product_categories', 8, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJQaFx1MWVlNSBraVx1MWVjN24gbVx1MDBlMXkgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJwaHUta2llbi1tYXktZHVuZy1waW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoyNToyOSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjI1OjI5In19'),
(50, 1, '127.0.0.1', '2021-11-24 11:26:20', 'create', 'product_categories', 9, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNCIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWExIGtoXHUwMGVkIiwic2x1ZyI6Im1heS1jby1raGkiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjI2OjIwIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MjY6MjAifX0='),
(51, 1, '127.0.0.1', '2021-11-24 11:26:54', 'quick_delete', 'product_categories', 8, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(52, 1, '127.0.0.1', '2021-11-24 11:26:58', 'quick_delete', 'product_categories', 9, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(53, 1, '127.0.0.1', '2021-11-24 11:27:02', 'quick_delete', 'product_categories', 6, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(54, 1, '127.0.0.1', '2021-11-24 11:28:18', 'create', 'product_categories', 10, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNCIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWExIGtoXHUwMGVkIFx1MDExMWlcdTFlYzduIiwic2x1ZyI6Im1heS1jby1raGktZGllbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6Mjg6MTgiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoyODoxOCJ9fQ=='),
(55, 1, '127.0.0.1', '2021-11-24 11:29:42', 'create', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNCIsIm5hbWUiOiJNXHUwMGUxeSBoXHUwMGUwbiIsInNsdWciOiJtYXktaGFuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNy0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODoyOTo0MiIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjI5OjQyIn19'),
(56, 1, '127.0.0.1', '2021-11-24 11:37:59', 'update', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiTVx1MDBlMXkgaFx1MDBlMG4iLCJzbHVnIjoibWF5LWhhbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTctMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6Mjk6NDQifSwibmV3Ijp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiTVx1MDBlMXkgaFx1MDBlMG4iLCJzbHVnIjoibWF5LWhhbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjM3OjU5In19'),
(57, 1, '127.0.0.1', '2021-11-24 11:39:01', 'update', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiTVx1MDBlMXkgaFx1MDBlMG4iLCJzbHVnIjoibWF5LWhhbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLWNvcHktMzMucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjM3OjU5In0sIm5ldyI6eyJwYXJlbnRfaWQiOiI0IiwibmFtZSI6Ik1cdTAwZTF5IGhcdTAwZTBuIiwic2x1ZyI6Im1heS1oYW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC00LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjM5OjAxIn19'),
(58, 1, '127.0.0.1', '2021-11-24 11:39:45', 'create', 'product_categories', 12, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMyIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgXHUwMTExXHUwMGU4biIsInNsdWciOiJ0aGlldC1iaS1kZW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjM5OjQ1IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6Mzk6NDUifX0='),
(59, 1, '127.0.0.1', '2021-11-24 11:40:46', 'create', 'product_categories', 13, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNSIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBjXHUxZWE3bSB0YXkgXHUwMTExXHUwMWExbiIsInNsdWciOiJkdW5nLWN1LWNhbS10YXktZG9uIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDA6NDYiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0MDo0NiJ9fQ=='),
(60, 1, '127.0.0.1', '2021-11-24 11:41:26', 'create', 'product_categories', 14, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBraG9hbiIsInNsdWciOiJkdW5nLWN1LWtob2FuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDE6MjYiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0MToyNiJ9fQ=='),
(61, 1, '127.0.0.1', '2021-11-24 11:41:53', 'create', 'product_categories', 15, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNCIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBjXHUxZWFmdCIsInNsdWciOiJkdW5nLWN1LWNhdCIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDE6NTMiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0MTo1MyJ9fQ=='),
(62, 1, '127.0.0.1', '2021-11-24 11:43:03', 'create', 'product_categories', 16, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNCIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSBjXHUwMWExIGtoXHUwMGVkIiwic2x1ZyI6ImR1bmctY3UtY28ta2hpIiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDM6MDMiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0MzowMyJ9fQ=='),
(63, 1, '127.0.0.1', '2021-11-24 11:44:39', 'create', 'product_categories', 17, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJNXHUwMGUxeSBjXHUxZWE3bSB0YXkiLCJzbHVnIjoibWF5LWNhbS10YXkiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ0OjM5IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDQ6MzkifX0='),
(64, 1, '127.0.0.1', '2021-11-24 11:44:53', 'update', 'product_categories', 16, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MDFhMSBraFx1MDBlZCIsInNsdWciOiJkdW5nLWN1LWNvLWtoaSIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQzOjA0In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNcdTAxYTEga2hcdTAwZWQiLCJzbHVnIjoiZHVuZy1jdS1jby1raGkiLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NDo1MyJ9fQ=='),
(65, 1, '127.0.0.1', '2021-11-24 11:45:04', 'update', 'product_categories', 13, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjUiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MWVhN20gdGF5IFx1MDExMVx1MDFhMW4iLCJzbHVnIjoiZHVuZy1jdS1jYW0tdGF5LWRvbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQwOjQ5In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNcdTFlYTdtIHRheSBcdTAxMTFcdTAxYTFuIiwic2x1ZyI6ImR1bmctY3UtY2FtLXRheS1kb24iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NTowNCJ9fQ=='),
(66, 1, '127.0.0.1', '2021-11-24 11:45:14', 'update', 'product_categories', 15, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MWVhZnQiLCJzbHVnIjoiZHVuZy1jdS1jYXQiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQxOjU1In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNcdTFlYWZ0Iiwic2x1ZyI6ImR1bmctY3UtY2F0IiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMy0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NToxNCJ9fQ=='),
(67, 1, '127.0.0.1', '2021-11-24 11:45:23', 'update', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiTVx1MDBlMXkgaFx1MDBlMG4iLCJzbHVnIjoibWF5LWhhbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTQtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6Mzk6MDEifSwibmV3Ijp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiTVx1MDBlMXkgaFx1MDBlMG4iLCJzbHVnIjoibWF5LWhhbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTQtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDU6MjMifX0='),
(68, 1, '127.0.0.1', '2021-11-24 11:45:46', 'update', 'product_categories', 10, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiTVx1MDBlMXkgY1x1MDFhMSBraFx1MDBlZCBcdTAxMTFpXHUxZWM3biIsInNsdWciOiJtYXktY28ta2hpLWRpZW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjI4OjIwIn0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6Ik1cdTAwZTF5IGNcdTAxYTEga2hcdTAwZWQgXHUwMTExaVx1MWVjN24iLCJzbHVnIjoibWF5LWNvLWtoaS1kaWVuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMy0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NTo0NiJ9fQ=='),
(69, 1, '127.0.0.1', '2021-11-24 11:45:55', 'update', 'product_categories', 12, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjMiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIFx1MDExMVx1MDBlOG4iLCJzbHVnIjoidGhpZXQtYmktZGVuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODozOTo0OSJ9LCJuZXciOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgXHUwMTExXHUwMGU4biIsInNsdWciOiJ0aGlldC1iaS1kZW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ1OjU1In19'),
(70, 1, '127.0.0.1', '2021-11-24 11:46:02', 'update', 'product_categories', 14, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUga2hvYW4iLCJzbHVnIjoiZHVuZy1jdS1raG9hbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQxOjI4In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGtob2FuIiwic2x1ZyI6ImR1bmctY3Uta2hvYW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NjowMiJ9fQ=='),
(71, 1, '127.0.0.1', '2021-11-24 11:46:46', 'update', 'product_categories', 16, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MDFhMSBraFx1MDBlZCIsInNsdWciOiJkdW5nLWN1LWNvLWtoaSIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ0OjUzIn0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNcdTAxYTEga2hcdTAwZWQiLCJzbHVnIjoiZHVuZy1jdS1jby1raGkiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0yLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ2OjQ2In19'),
(72, 1, '127.0.0.1', '2021-11-24 15:11:19', 'update', 'product_categories', 3, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIGNoaVx1MWViZnUgc1x1MDBlMW5nIiwic2x1ZyI6InRoaWV0LWJpLWNoaWV1LXNhbmciLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjExOjEzIn0sIm5ldyI6eyJwYXJlbnRfaWQiOiIxIiwibmFtZSI6IlRoaVx1MWViZnQgYlx1MWVjYiBjaGlcdTFlYmZ1IHNcdTAwZTFuZyIsInNsdWciOiJ0aGlldC1iaS1jaGlldS1zYW5nIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjoxMToxOSJ9fQ=='),
(73, 1, '127.0.0.1', '2021-11-24 15:14:26', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiIxIiwicGxhY2UiOiJob21lIiwidmFsdWUiOiIxIn19'),
(74, 1, '127.0.0.1', '2021-11-24 15:14:37', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiI0IiwicGxhY2UiOiJob21lIiwidmFsdWUiOiIyIn19'),
(75, 1, '127.0.0.1', '2021-11-24 15:15:09', 'update', 'product_categories', 5, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY2hcdTFlYTF5IHhcdTAxMDNuZyIsInNsdWciOiJkdW5nLWN1LWNoYXkteGFuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6MTE6NTQifSwibmV3Ijp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY2hcdTFlYTF5IHhcdTAxMDNuZyIsInNsdWciOiJkdW5nLWN1LWNoYXkteGFuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjI6MTU6MDkifX0='),
(76, 1, '127.0.0.1', '2021-11-24 15:50:56', 'update', 'product_categories', 3, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIGNoaVx1MWViZnUgc1x1MDBlMW5nIiwic2x1ZyI6InRoaWV0LWJpLWNoaWV1LXNhbmciLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIyOjExOjE5In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IlRoaVx1MWViZnQgYlx1MWVjYiBjaGlcdTFlYmZ1IHNcdTAwZTFuZyIsInNsdWciOiJ0aGlldC1iaS1jaGlldS1zYW5nIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1MDo1NiJ9fQ=='),
(77, 1, '127.0.0.1', '2021-11-24 15:51:18', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiIzIiwicGxhY2UiOiJob21lIiwidmFsdWUiOiIzIn19'),
(78, 1, '127.0.0.1', '2021-11-24 15:51:58', 'update', 'product_categories', 5, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY2hcdTFlYTF5IHhcdTAxMDNuZyIsInNsdWciOiJkdW5nLWN1LWNoYXkteGFuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjI6MTU6MDkifSwibmV3Ijp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY2hcdTFlYTF5IHhcdTAxMDNuZyIsInNsdWciOiJkdW5nLWN1LWNoYXkteGFuZyIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjI6NTE6NTgifX0='),
(79, 1, '127.0.0.1', '2021-11-24 15:52:11', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiI1IiwicGxhY2UiOiJob21lIiwidmFsdWUiOiI0In19'),
(80, 1, '127.0.0.1', '2021-11-24 15:52:39', 'update', 'product_categories', 13, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MWVhN20gdGF5IFx1MDExMVx1MDFhMW4iLCJzbHVnIjoiZHVuZy1jdS1jYW0tdGF5LWRvbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ1OjA0In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIwIiwibmFtZSI6IkRcdTFlZTVuZyBjXHUxZWU1IGNcdTFlYTdtIHRheSIsInNsdWciOiJkdW5nLWN1LWNhbS10YXktZG9uIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjI6NTI6MzkifX0='),
(81, 1, '127.0.0.1', '2021-11-24 15:53:26', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiIxMyIsInBsYWNlIjoiaG9tZSIsInZhbHVlIjoiNSJ9fQ=='),
(82, 1, '127.0.0.1', '2021-11-24 15:53:55', 'update', 'product_categories', 10, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiTVx1MDBlMXkgY1x1MDFhMSBraFx1MDBlZCBcdTAxMTFpXHUxZWM3biIsInNsdWciOiJtYXktY28ta2hpLWRpZW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ1OjQ2In0sIm5ldyI6eyJwYXJlbnRfaWQiOiIxIiwibmFtZSI6Ik1cdTAwZTF5IGtob2FuIHBpbiIsInNsdWciOiJtYXktY28ta2hpLWRpZW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIyOjUzOjU1In19'),
(83, 1, '127.0.0.1', '2021-11-24 15:54:23', 'update', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiTVx1MDBlMXkgaFx1MDBlMG4iLCJzbHVnIjoibWF5LWhhbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTQtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDU6MjMifSwibmV3Ijp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiTVx1MDBlMXkgbVx1MDBlMGkgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJtYXktaGFuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNC0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1NDoyMyJ9fQ==');
INSERT INTO `system_logs` (`id`, `admin_id`, `ip`, `time`, `action`, `type`, `type_id`, `detail`) VALUES
(84, 1, '127.0.0.1', '2021-11-24 15:54:37', 'update', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiTVx1MDBlMXkgbVx1MDBlMGkgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJtYXktaGFuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNC0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1NDoyMyJ9LCJuZXciOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBkXHUwMGY5bmcgcGluIiwic2x1ZyI6Im1heS1oYW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC00LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIyOjU0OjM3In19'),
(85, 1, '127.0.0.1', '2021-11-24 15:55:34', 'update', 'product_categories', 12, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiVGhpXHUxZWJmdCBiXHUxZWNiIFx1MDExMVx1MDBlOG4iLCJzbHVnIjoidGhpZXQtYmktZGVuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NTo1NSJ9LCJuZXciOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJNXHUwMGUxeSB2XHUxZWI3biBidSBsXHUwMGY0bmcgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJ0aGlldC1iaS1kZW4iLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIyOjU1OjM0In19'),
(86, 1, '127.0.0.1', '2021-11-24 15:55:53', 'update', 'product_categories', 10, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiTVx1MDBlMXkga2hvYW4gcGluIiwic2x1ZyI6Im1heS1jby1raGktZGllbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTMtMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjI6NTM6NTUifSwibmV3Ijp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiTVx1MDBlMXkga2hvYW4gcGluIiwic2x1ZyI6Im1heS1jby1raGktZGllbiIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIyOjU1OjUzIn19'),
(87, 1, '127.0.0.1', '2021-11-24 15:56:12', 'update', 'product_categories', 11, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiTVx1MDBlMXkgbVx1MDBlMGkgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJtYXktaGFuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNC0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1NDozNyJ9LCJuZXciOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBkXHUwMGY5bmcgcGluIiwic2x1ZyI6Im1heS1oYW4iLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1NjoxMiJ9fQ=='),
(88, 1, '127.0.0.1', '2021-11-24 15:56:19', 'update', 'product_categories', 12, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjEiLCJuYW1lIjoiTVx1MDBlMXkgdlx1MWViN24gYnUgbFx1MDBmNG5nIGRcdTAwZjluZyBwaW4iLCJzbHVnIjoidGhpZXQtYmktZGVuIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1NTozNCJ9LCJuZXciOnsicGFyZW50X2lkIjoiMSIsIm5hbWUiOiJNXHUwMGUxeSB2XHUxZWI3biBidSBsXHUwMGY0bmcgZFx1MDBmOW5nIHBpbiIsInNsdWciOiJ0aGlldC1iaS1kZW4iLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1NjoxOSJ9fQ=='),
(89, 1, '127.0.0.1', '2021-11-24 15:58:11', 'update', 'product_categories', 14, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUga2hvYW4iLCJzbHVnIjoiZHVuZy1jdS1raG9hbiIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ2OjAyIn0sIm5ldyI6eyJwYXJlbnRfaWQiOiI0IiwibmFtZSI6Ik1cdTAwZTF5IGtob2FuIFx1MDExMWlcdTFlYzduIiwic2x1ZyI6ImR1bmctY3Uta2hvYW4iLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMjo1ODoxMCJ9fQ=='),
(90, 1, '127.0.0.1', '2021-11-24 15:58:47', 'update', 'product_categories', 15, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MWVhZnQiLCJzbHVnIjoiZHVuZy1jdS1jYXQiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDE4OjQ1OjE0In0sIm5ldyI6eyJwYXJlbnRfaWQiOiI0IiwibmFtZSI6Ik1cdTAwZTF5IG1cdTAwZTBpIGRcdTAwZjluZyBcdTAxMTFpXHUxZWM3biIsInNsdWciOiJkdW5nLWN1LWNhdCIsImltYWdlIjoiIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIyOjU4OjQ3In19'),
(91, 1, '127.0.0.1', '2021-11-24 15:59:23', 'update', 'product_categories', 16, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiRFx1MWVlNW5nIGNcdTFlZTUgY1x1MDFhMSBraFx1MDBlZCIsInNsdWciOiJkdW5nLWN1LWNvLWtoaSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTItMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMTg6NDY6NDYifSwibmV3Ijp7InBhcmVudF9pZCI6IjQiLCJuYW1lIjoiTVx1MDBlMXkgVlx1MWViN24gQnUgTFx1MDBmNG5nIERcdTAwZjluZyBcdTAxMTFpXHUxZWM3biIsInNsdWciOiJkdW5nLWN1LWNvLWtoaSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTItMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjI6NTk6MjMifX0='),
(92, 1, '127.0.0.1', '2021-11-24 16:00:09', 'update', 'product_categories', 17, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjAiLCJuYW1lIjoiTVx1MDBlMXkgY1x1MWVhN20gdGF5Iiwic2x1ZyI6Im1heS1jYW0tdGF5IiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMy0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAxODo0NDo0MyJ9LCJuZXciOnsicGFyZW50X2lkIjoiMyIsIm5hbWUiOiJcdTAxMTBcdTAwZThuIGxlZCByXHUxZWExbmcgXHUwMTExXHUwMGY0bmciLCJzbHVnIjoibWF5LWNhbS10YXkiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC0zLTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIzOjAwOjA4In19'),
(93, 1, '127.0.0.1', '2021-11-24 16:00:19', 'update', 'product_categories', 17, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsInVwZGF0ZWRfYXQiXSwib2xkIjp7InBhcmVudF9pZCI6IjMiLCJuYW1lIjoiXHUwMTEwXHUwMGU4biBsZWQgclx1MWVhMW5nIFx1MDExMVx1MDBmNG5nIiwic2x1ZyI6Im1heS1jYW0tdGF5IiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMy0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMDowOCJ9LCJuZXciOnsicGFyZW50X2lkIjoiMyIsIm5hbWUiOiJcdTAxMTBcdTAwZThuIGxlZCByXHUxZWExbmcgXHUwMTExXHUwMGY0bmciLCJzbHVnIjoibWF5LWNhbS10YXkiLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMDoxOSJ9fQ=='),
(94, 1, '127.0.0.1', '2021-11-24 16:01:15', 'create', 'product_categories', 18, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMyIsIm5hbWUiOiJcdTAxMTBcdTAwZThuIGxlZCBQaGlsaXAiLCJzbHVnIjoiZGVuLWxlZC1waGlsaXAiLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMToxNSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIzOjAxOjE1In19'),
(95, 1, '127.0.0.1', '2021-11-24 16:01:41', 'create', 'product_categories', 19, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMyIsIm5hbWUiOiJcdTAxMTBcdTAwZThuIHNcdTFlZTNpIFx1MDExMVx1MWVkMXQiLCJzbHVnIjoiZGVuLXNvaS1kb3QiLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMTo0MSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIzOjAxOjQxIn19'),
(96, 1, '127.0.0.1', '2021-11-24 16:02:26', 'create', 'product_categories', 20, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNSIsIm5hbWUiOiJNXHUwMGUxeSBjXHUxZWFmdCBjaFx1MWVhMXkgeFx1MDEwM25nIiwic2x1ZyI6Im1heS1jYXQtY2hheS14YW5nIiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjM6MDI6MjYiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMjoyNiJ9fQ=='),
(97, 1, '127.0.0.1', '2021-11-24 16:02:51', 'create', 'product_categories', 21, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNSIsIm5hbWUiOiJNXHUwMGUxeSBtXHUwMGUwaSBjaFx1MWVhMXkgeFx1MDEwM25nIiwic2x1ZyI6Im1heS1tYWktY2hheS14YW5nIiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjM6MDI6NTEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMjo1MSJ9fQ=='),
(98, 1, '127.0.0.1', '2021-11-24 16:03:29', 'create', 'product_categories', 22, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiNSIsIm5hbWUiOiJNXHUwMGUxeSB2XHUxZWI3biBjaFx1MWVhMXkgeFx1MDEwM25nIiwic2x1ZyI6Im1heS12YW4tY2hheS14YW5nIiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjM6MDM6MjkiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMzoyOSJ9fQ=='),
(99, 1, '127.0.0.1', '2021-11-24 16:03:52', 'create', 'product_categories', 23, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMTMiLCJuYW1lIjoia1x1MDBlY20gY1x1MWVhN20gdGF5Iiwic2x1ZyI6ImtpbS1jYW0tdGF5IiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjM6MDM6NTIiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowMzo1MiJ9fQ=='),
(100, 1, '127.0.0.1', '2021-11-24 16:04:29', 'create', 'product_categories', 24, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMTMiLCJuYW1lIjoiVHVhIHZcdTAwZWR0IGNcdTFlYTdtIHRheSIsInNsdWciOiJ0dWEtdml0LWNhbS10YXkiLCJpbWFnZSI6IiIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowNDoyOSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI0IDIzOjA0OjI5In19'),
(101, 1, '127.0.0.1', '2021-11-24 16:04:48', 'create', 'product_categories', 25, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMTMiLCJuYW1lIjoiS1x1MDBlOW8gY1x1MWVhN20gdGF5Iiwic2x1ZyI6Imtlby1jYW0tdGF5IiwiaW1hZ2UiOiIiLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjM6MDQ6NDgiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowNDo0OCJ9fQ=='),
(102, 1, '127.0.0.1', '2021-11-24 16:09:09', 'create', 'products', 5, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwNSIsIm5hbWUiOiJNXHUwMGUxeSBjXHUxZWFmdCBib3NjaCAyMDAwdyIsInNsdWciOiJtYXktY2F0LWJvc2NoLTIwMDB3IiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtY29weS0yNi5wbmciLCJzbGlkZSI6IiIsInByaWNlIjoiMzAwMDAwMCIsInByaWNlX29sZCI6IjAiLCJkZXRhaWwiOiIiLCJyZWxhdGVkX3Byb2R1Y3RzIjoiIiwicXVhbnRpdHkiOiIwIiwibGVuZ3RoIjoiMCIsIndpZGUiOiIwIiwiaGVpZ2h0IjoiMCIsIndlaWdodCI6IjAiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjQgMjM6MDk6MDkiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowOTowOSJ9fQ=='),
(103, 1, '127.0.0.1', '2021-11-24 16:09:28', 'update', 'products', 4, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJicmFuZF9pZCI6IjAiLCJza3UiOiIwMDQiLCJuYW1lIjoiTVx1MDBlMXkgY1x1MDFiMGEgbFx1MWVjZG5nIGJvc2NoIERXMzQxSyIsInNsdWciOiJtYXktY3VhLWxvbmctYm9zY2gtZHczNDFrIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtY29weS0yMy5wbmciLCJzbGlkZSI6IiIsInByaWNlIjoiNDA1MDAwMCIsInByaWNlX29sZCI6IjAiLCJkZXRhaWwiOiIiLCJyZWxhdGVkX3Byb2R1Y3RzIjoiIiwicXVhbnRpdHkiOiIwIiwibGVuZ3RoIjoiMCIsIndpZGUiOiIwIiwiaGVpZ2h0IjoiMCIsIndlaWdodCI6IjAiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjMgMTg6MTU6MTMifSwibmV3Ijp7ImJyYW5kX2lkIjoiMSIsInNrdSI6IjAwNCIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWIwYSBsXHUxZWNkbmcgYm9zY2ggRFczNDFLIiwic2x1ZyI6Im1heS1jdWEtbG9uZy1ib3NjaC1kdzM0MWsiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTIzLnBuZyIsInNsaWRlIjoiIiwicHJpY2UiOiI0MDUwMDAwIiwicHJpY2Vfb2xkIjoiMCIsImRldGFpbCI6IiIsInJlbGF0ZWRfcHJvZHVjdHMiOiIiLCJxdWFudGl0eSI6IjAiLCJsZW5ndGgiOiIwIiwid2lkZSI6IjAiLCJoZWlnaHQiOiIwIiwid2VpZ2h0IjoiMCIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNCAyMzowOToyOCJ9fQ=='),
(104, 1, '127.0.0.1', '2021-11-24 16:14:33', 'quick_delete', 'product_categories', 12, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(105, 1, '127.0.0.1', '2021-11-24 16:15:07', 'quick_update', 'product_categories', 10, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiIwIn19'),
(106, 1, '127.0.0.1', '2021-11-24 16:15:07', 'quick_update', 'product_categories', 11, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiIwIn19'),
(107, 1, '127.0.0.1', '2021-11-24 16:15:25', 'quick_update', 'product_categories', 10, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIwIn0sIm5ldyI6eyJzdGF0dXMiOiIxIn19'),
(108, 1, '127.0.0.1', '2021-11-24 16:15:26', 'quick_update', 'product_categories', 11, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIwIn0sIm5ldyI6eyJzdGF0dXMiOiIxIn19'),
(109, 1, '127.0.0.1', '2021-11-24 16:16:59', 'quick_update', 'products', 2, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiIwIn19'),
(110, 1, '127.0.0.1', '2021-11-24 16:17:01', 'quick_update', 'products', 2, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIwIn0sIm5ldyI6eyJzdGF0dXMiOiIxIn19'),
(111, 1, '127.0.0.1', '2021-11-24 16:17:03', 'quick_update', 'products', 2, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiIwIn19'),
(112, 1, '127.0.0.1', '2021-11-25 03:00:16', 'create', 'product_categories', 26, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJUaGlcdTFlYmZ0IGJcdTFlY2IgZFx1MWVhN3UiLCJzbHVnIjoidGhpZXQtYmktZGF1IiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtMy0xLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMDowMDoxNiIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDEwOjAwOjE2In19'),
(113, 1, '127.0.0.1', '2021-11-25 03:06:14', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiIyNiIsInBsYWNlIjoiaG9tZSIsInZhbHVlIjoiNiJ9fQ=='),
(114, 1, '127.0.0.1', '2021-11-25 03:10:08', 'create', 'product_categories', 27, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMjYiLCJuYW1lIjoiTVx1MDBlMXkgbVx1MDBlMGkgZFx1MWVhN3UiLCJzbHVnIjoibWF5LW1haS1kYXUiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTI1LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMDoxMDowOCIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDEwOjEwOjA4In19'),
(115, 1, '127.0.0.1', '2021-11-25 03:10:44', 'create', 'product_categories', 28, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMjYiLCJuYW1lIjoiTVx1MDBlMXkga2hvYW4gZFx1MWVhN3UiLCJzbHVnIjoibWF5LWtob2FuLWRhdSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTUucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsImNyZWF0ZWRfYXQiOiIyMDIxLTExLTI1IDEwOjEwOjQ0IiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjUgMTA6MTA6NDQifX0='),
(116, 1, '127.0.0.1', '2021-11-25 03:11:49', 'create', 'product_categories', 29, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMjYiLCJuYW1lIjoiTVx1MDBlMXkgblx1MDBlOW4gZFx1MWVhN3UiLCJzbHVnIjoibWF5LW5lbi1kYXUiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC1jb3B5LTIzLnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMDoxMTo0OSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDEwOjExOjQ5In19'),
(117, 1, '127.0.0.1', '2021-11-25 03:18:48', 'create', 'products', 6, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwNiIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWIwYSBsXHUxZWNkbmcgRGV3YWx0IERXMzQxSzAiLCJzbHVnIjoibWF5LWN1YS1sb25nLWRld2FsdC1kdzM0MWswIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtY29weS0zNC5wbmciLCJzbGlkZSI6IiIsInByaWNlIjoiMjM1MDAwMCIsInByaWNlX29sZCI6IjAiLCJkZXRhaWwiOiIiLCJyZWxhdGVkX3Byb2R1Y3RzIjoiIiwicXVhbnRpdHkiOiIwIiwibGVuZ3RoIjoiMCIsIndpZGUiOiIwIiwiaGVpZ2h0IjoiMCIsIndlaWdodCI6IjAiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjUgMTA6MTg6NDgiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMDoxODo0OCJ9fQ=='),
(118, 1, '127.0.0.1', '2021-11-25 03:19:59', 'create', 'products', 7, 'eyJmaWVsZHMiOlsiYnJhbmRfaWQiLCJza3UiLCJuYW1lIiwic2x1ZyIsImltYWdlIiwic2xpZGUiLCJwcmljZSIsInByaWNlX29sZCIsImRldGFpbCIsInJlbGF0ZWRfcHJvZHVjdHMiLCJxdWFudGl0eSIsImxlbmd0aCIsIndpZGUiLCJoZWlnaHQiLCJ3ZWlnaHQiLCJzdGF0dXMiLCJjcmVhdGVkX2F0IiwidXBkYXRlZF9hdCJdLCJkYXRhIjp7ImJyYW5kX2lkIjoiMCIsInNrdSI6IjAwNyIsIm5hbWUiOiJNXHUwMGUxeSBjXHUwMWIwYSBsXHUxZWNkbmcgRGV3YWx0IERXMzQxSzEiLCJzbHVnIjoibWF5LWN1YS1sb25nLWRld2FsdC1kdzM0MWsxIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNS5wbmciLCJzbGlkZSI6IiIsInByaWNlIjoiMjM1MDAwMCIsInByaWNlX29sZCI6IjAiLCJkZXRhaWwiOiIiLCJyZWxhdGVkX3Byb2R1Y3RzIjoiIiwicXVhbnRpdHkiOiIwIiwibGVuZ3RoIjoiMCIsIndpZGUiOiIwIiwiaGVpZ2h0IjoiMCIsIndlaWdodCI6IjAiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjUgMTA6MTk6NTkiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMDoxOTo1OSJ9fQ=='),
(119, 1, '127.0.0.1', '2021-11-25 03:28:45', 'quick_delete', 'product_categories', 26, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(120, 1, '127.0.0.1', '2021-11-25 03:28:50', 'quick_delete', 'product_categories', 27, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(121, 1, '127.0.0.1', '2021-11-25 03:28:55', 'quick_delete', 'product_categories', 28, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(122, 1, '127.0.0.1', '2021-11-25 03:29:00', 'quick_delete', 'product_categories', 29, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(123, 1, '127.0.0.1', '2021-11-25 03:38:15', 'create', 'product_categories', 30, 'eyJmaWVsZHMiOlsicGFyZW50X2lkIiwibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsicGFyZW50X2lkIjoiMCIsIm5hbWUiOiJEXHUxZWU1bmcgY1x1MWVlNSB0YXkiLCJzbHVnIjoiZHVuZy1jdS10YXkiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC01LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMDozODoxNSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDEwOjM4OjE1In19'),
(124, 1, '127.0.0.1', '2021-11-25 03:38:48', 'create', 'pins', 0, 'eyJmaWVsZHMiOlsidHlwZSIsInR5cGVfaWQiLCJwbGFjZSIsInZhbHVlIl0sImRhdGEiOnsidHlwZSI6InByb2R1Y3RfY2F0ZWdvcmllcyIsInR5cGVfaWQiOiIzMCIsInBsYWNlIjoiaG9tZSIsInZhbHVlIjoiNiJ9fQ=='),
(125, 1, '127.0.0.1', '2021-11-25 03:39:25', 'quick_delete', 'product_categories', 30, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(126, 1, '127.0.0.1', '2021-11-25 03:56:53', 'quick_delete', 'posts', 5, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(127, 1, '127.0.0.1', '2021-11-25 03:56:56', 'quick_delete', 'posts', 4, 'eyJmaWVsZHMiOlsic3RhdHVzIl0sIm9sZCI6eyJzdGF0dXMiOiIxIn0sIm5ldyI6eyJzdGF0dXMiOiItMSJ9fQ=='),
(128, 1, '127.0.0.1', '2021-11-25 04:03:17', 'create', 'posts', 6, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6IkNcdTAwZTFjIGxvXHUxZWExaSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIHRoXHUwMWIwXHUxZWRkbmcgZ1x1MWViN3AgdHJcdTAwZWFuIHRoXHUxZWNiIHRyXHUwMWIwXHUxZWRkbmcgdHJvbmcgblx1MDFiMFx1MWVkYmMiLCJzbHVnIjoiY2FjLWxvYWktbWF5LWhhbi1kaWVuLXR1LXRodW9uZy1nYXAtdHJlbi10aGktdHJ1b25nLXRyb25nLW51b2MiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC02LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJjcmVhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMTowMzoxNyIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDExOjAzOjE3In19'),
(129, 1, '127.0.0.1', '2021-11-25 04:03:20', 'update', 'posts', 6, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJDXHUwMGUxYyBsb1x1MWVhMWkgbVx1MDBlMXkgaFx1MDBlMG4gXHUwMTExaVx1MWVjN24gdFx1MWVlZCB0aFx1MDFiMFx1MWVkZG5nIGdcdTFlYjdwIHRyXHUwMGVhbiB0aFx1MWVjYiB0clx1MDFiMFx1MWVkZG5nIHRyb25nIG5cdTAxYjBcdTFlZGJjIiwic2x1ZyI6ImNhYy1sb2FpLW1heS1oYW4tZGllbi10dS10aHVvbmctZ2FwLXRyZW4tdGhpLXRydW9uZy10cm9uZy1udW9jIiwiaW1hZ2UiOiJcL2NvcmVcLzIwMjFcLzExXC9iaXRtYXAtNi5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwidXBkYXRlZF9hdCI6IjIwMjEtMTEtMjUgMTE6MDM6MTcifSwibmV3Ijp7ImFkbWluX3VzZXJfaWQiOiIyIiwibmFtZSI6IkNcdTAwZTFjIGxvXHUxZWExaSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIHRoXHUwMWIwXHUxZWRkbmcgZ1x1MWViN3AgdHJcdTAwZWFuIHRoXHUxZWNiIHRyXHUwMWIwXHUxZWRkbmcgdHJvbmcgblx1MDFiMFx1MWVkYmMiLCJzbHVnIjoiY2FjLWxvYWktbWF5LWhhbi1kaWVuLXR1LXRodW9uZy1nYXAtdHJlbi10aGktdHJ1b25nLXRyb25nLW51b2MiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC02LnBuZyIsImRldGFpbCI6IiIsInN0YXR1cyI6IjEiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMTowMzoxNyJ9fQ=='),
(130, 1, '127.0.0.1', '2021-11-25 04:04:13', 'create', 'posts', 7, 'eyJmaWVsZHMiOlsibmFtZSIsInNsdWciLCJpbWFnZSIsImRldGFpbCIsInN0YXR1cyIsImNyZWF0ZWRfYXQiLCJ1cGRhdGVkX2F0Il0sImRhdGEiOnsibmFtZSI6IkdpXHUwMGUxIG1cdTAwZTF5IGhcdTAwZTBuIFx1MDExMWlcdTFlYzduIHRcdTFlZWQgY1x1MDBmMyBcdTAxMTFcdTFlYWZ0IGtoXHUwMGY0bmc/IENcdTFlYTduIGNoXHUwMGZhIFx1MDBmZCBnXHUwMGVjIiwic2x1ZyI6ImdpYS1tYXktaGFuLWRpZW4tdHUtY28tZGF0LWtob25nLWNhbi1jaHUteS1naSIsImltYWdlIjoiXC9jb3JlXC8yMDIxXC8xMVwvYml0bWFwLTctMS5wbmciLCJkZXRhaWwiOiIiLCJzdGF0dXMiOiIxIiwiY3JlYXRlZF9hdCI6IjIwMjEtMTEtMjUgMTE6MDQ6MTMiLCJ1cGRhdGVkX2F0IjoiMjAyMS0xMS0yNSAxMTowNDoxMyJ9fQ=='),
(131, 1, '127.0.0.1', '2021-11-25 04:04:15', 'update', 'posts', 7, 'eyJmaWVsZHMiOlsiYWRtaW5fdXNlcl9pZCIsIm5hbWUiLCJzbHVnIiwiaW1hZ2UiLCJkZXRhaWwiLCJzdGF0dXMiLCJ1cGRhdGVkX2F0Il0sIm9sZCI6eyJhZG1pbl91c2VyX2lkIjoiMCIsIm5hbWUiOiJHaVx1MDBlMSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIGNcdTAwZjMgXHUwMTExXHUxZWFmdCBraFx1MDBmNG5nPyBDXHUxZWE3biBjaFx1MDBmYSBcdTAwZmQgZ1x1MDBlYyIsInNsdWciOiJnaWEtbWF5LWhhbi1kaWVuLXR1LWNvLWRhdC1raG9uZy1jYW4tY2h1LXktZ2kiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC03LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDExOjA0OjEzIn0sIm5ldyI6eyJhZG1pbl91c2VyX2lkIjoiMiIsIm5hbWUiOiJHaVx1MDBlMSBtXHUwMGUxeSBoXHUwMGUwbiBcdTAxMTFpXHUxZWM3biB0XHUxZWVkIGNcdTAwZjMgXHUwMTExXHUxZWFmdCBraFx1MDBmNG5nPyBDXHUxZWE3biBjaFx1MDBmYSBcdTAwZmQgZ1x1MDBlYyIsInNsdWciOiJnaWEtbWF5LWhhbi1kaWVuLXR1LWNvLWRhdC1raG9uZy1jYW4tY2h1LXktZ2kiLCJpbWFnZSI6IlwvY29yZVwvMjAyMVwvMTFcL2JpdG1hcC03LTEucG5nIiwiZGV0YWlsIjoiIiwic3RhdHVzIjoiMSIsInVwZGF0ZWRfYXQiOiIyMDIxLTExLTI1IDExOjA0OjEzIn19');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `detail` longtext COLLATE utf8mb4_unicode_ci,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tag_maps`
--

CREATE TABLE `tag_maps` (
  `tag_table` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag_table_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `percentage` int(11) NOT NULL DEFAULT '0',
  `priority` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `variants`
--

CREATE TABLE `variants` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `product_id` int(11) NOT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `price_old` int(11) DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `quantity` int(11) DEFAULT NULL,
  `length` int(11) DEFAULT NULL,
  `wide` int(11) DEFAULT NULL,
  `height` int(11) DEFAULT NULL,
  `weight` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `verify_emails`
--

CREATE TABLE `verify_emails` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_users`
--
ALTER TABLE `admin_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `attributes`
--
ALTER TABLE `attributes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `attributes_slug_unique` (`slug`);

--
-- Indexes for table `attribute_details`
--
ALTER TABLE `attribute_details`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attribute_details_attribute_id_index` (`attribute_id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `brands_slug_unique` (`slug`);

--
-- Indexes for table `call_me_backs`
--
ALTER TABLE `call_me_backs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_registers`
--
ALTER TABLE `email_registers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `google_shoppings`
--
ALTER TABLE `google_shoppings`
  ADD KEY `google_shoppings_type_type_id_index` (`type`,`type_id`);

--
-- Indexes for table `language_metas`
--
ALTER TABLE `language_metas`
  ADD KEY `language_metas_lang_table_lang_locale_lang_code_index` (`lang_table`,`lang_locale`,`lang_code`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_histories`
--
ALTER TABLE `order_histories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_category_maps`
--
ALTER TABLE `post_category_maps`
  ADD KEY `post_category_maps_post_id_index` (`post_id`),
  ADD KEY `post_category_maps_post_category_id_index` (`post_category_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `products_slug_unique` (`slug`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_categories_slug_unique` (`slug`);

--
-- Indexes for table `product_category_maps`
--
ALTER TABLE `product_category_maps`
  ADD KEY `product_category_maps_product_id_index` (`product_id`),
  ADD KEY `product_category_maps_product_category_id_index` (`product_category_id`);

--
-- Indexes for table `shippings`
--
ALTER TABLE `shippings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_provinces`
--
ALTER TABLE `shipping_provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_rules`
--
ALTER TABLE `shipping_rules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `solutions`
--
ALTER TABLE `solutions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `solutions_slug_unique` (`slug`);

--
-- Indexes for table `sync_links`
--
ALTER TABLE `sync_links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `system_logs`
--
ALTER TABLE `system_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variants`
--
ALTER TABLE `variants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `verify_emails`
--
ALTER TABLE `verify_emails`
  ADD UNIQUE KEY `verify_emails_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_users`
--
ALTER TABLE `admin_users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `attributes`
--
ALTER TABLE `attributes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `attribute_details`
--
ALTER TABLE `attribute_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `call_me_backs`
--
ALTER TABLE `call_me_backs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email_registers`
--
ALTER TABLE `email_registers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_histories`
--
ALTER TABLE `order_histories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `shippings`
--
ALTER TABLE `shippings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shipping_provinces`
--
ALTER TABLE `shipping_provinces`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shipping_rules`
--
ALTER TABLE `shipping_rules`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `slides`
--
ALTER TABLE `slides`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `solutions`
--
ALTER TABLE `solutions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sync_links`
--
ALTER TABLE `sync_links`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `system_logs`
--
ALTER TABLE `system_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `variants`
--
ALTER TABLE `variants`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
