# Hướng dẫn sử dụng SudoCore V3
* Clone source code từ bitbucket
* Xóa folder .git trong project để tránh push nhầm code lên Core
* Tạo file .env, nội dung copy từ file .env.example
* Cấu hình và tạo DB
* Kiểm tra file /root/composer.json và gọi các module cần thiết cho dự án
* Chạy composer install
* Chạy php artisan key:generate để tạo APP_KEY
* Chạy php artisan migrate để tạo bảng CSDL theo từng package
* Chạy php artisan admin_users:seeds để tạo tài khoản quản trị
* Chạy php artisan license:seeds để khởi tạo dữ liệu license (Tính năng này dùng để giảm thiểu khả năng người ngoài có thể sử dụng cms của Sudo)
* Mặc định core khi bắt đầu sẽ là đa ngôn ngữ, nếu website dùng 1 ngôn ngữ thì xóa nội dung sau tại file root/config/app.php
	`
		'language' => [
	        'vi' => [
	            'name' => 'Tiếng việt',
	            'flag' => '/admin_assets/images/flags/vn.jpg',
	            'locale' => 'vi_VN',
	        ],
	        'en' => [
	            'name' => 'English',
	            'flag' => '/admin_assets/images/flags/us.jpg',
	            'locale' => 'en_EN',
	        ],
	    ],
	`
Trên đây là các bước để khởi tạo core, Hướng dẫn chi tiết của từng package sẽ được viết tại file readme.md của package tương ứng 

Tai khoan quan tri da duoc tao tu dong:
dev - (qMiTLfDARMp
sudo - eW^FA*WZJGKw
