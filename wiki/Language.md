# Hướng dẫn cấu hình Đa ngôn ngữ cho Site MultiLanguage #
Đa ngôn ngữ là một tính năng quan trọng của một trang web, muốn vươn ra thị trường thế giới thì không thể thể thiếu 1 website đa ngôn ngữ phải không nào? Vì vậy SudoCore cũng đã hỗ trợ đa ngôn ngữ nếu như được cấu hình. Cụ thể chúng ra sẽ cấu hình như sau:
### Cấu hình ngôn ngữ ###
Chúng ta sẽ cấu hình ngôn ngữ config/app.php

    ...
    // Đa ngôn ngữ
    'language' => [
        'vi' => [
            'name' => 'Tiếng việt',
            'flag' => '/core/img/flags/vn.png',
        ],
        'en' => [
            'name' => 'English',
            'flag' => '/core/img/flags/en.png',
        ],
    ],
    ...
    
Từng ngôn ngữ sẽ có các giá trị là tên (name) và hình ảnh cờ (flag) cùng với đó là key của ngôn ngữ đó, key của ngôn ngữ là rất quan trọng trong việc xác định đây là ngôn ngữ gì vì thế nó sẽ phải là duy nhất.

### Ngôn ngữ cho Package ###
Chúng ta sẽ định nghĩa module hiện tại có sử dụng đa ngôn ngữ hay không bằng cách xác định giá trị has_locale tại hàm __construct()

    function __construct() {
        ...
        $this->has_locale = true;
        ...
    }
    
### Thêm cột ngôn ngữ tại bảng ###
Để có thể hiển thị cột đa ngôn ngữ tại bảng thì chúng ta sẽ thêm vào hàm add() của ListData()

    $listdata->add('', 'Language', 0, 'lang');
    
### Thêm bản ghi sử dụng ngôn ngữ khác tại Form ###
Một bài viết hay danh mục khi sử dụng đa ngôn ngữ đều sẽ một bản ghi tương ứng với ngôn ngữ khác, VD: Danh mục tiếng việt là "Công nghệ" thì sẽ có bản ghi đa ngôn ngữ tiếng anh là "Technology", tương tự như tiếng pháp, tiếng nhật, và để có thể thêm ngôn ngữ cho 1 bài viết thì chúng ra sẽ làm như sau. 
Tại link thêm (Thêm bài viết, sản phẩm, danh mục...) thì chúng ta sẽ truyền thêm 2 param là `lang_referer` và `lang_locale`, trong đó `lang_referer`: là ID của bản ghi gốc và `lang_locale`: là ngôn ngữ muốn thêm cho bản ghi gốc. VD: Thêm ngôn ngữ tiếng anh cho danh mục tin tức có id là 1 thì ta sẽ truy cập link sau:

    /{admin_dir}/posts/create?lang_referer=1&lang_locale=en
    
và để hiển thị bản ghi gốc tại form tạo thì chúng ta sẽ thêm vào Form
    
    $form->lang($this->table_name);

Lúc này sẽ thêm 1 form text disable hiển thị tên của bản ghi gốc