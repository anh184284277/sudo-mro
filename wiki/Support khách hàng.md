# Hướng dẫn support khách hàng trên hệ thống support.sudo.vn #

## Quy trình support khi có ticket mới ##
### Bước 1: Người phụ trách support ###
* Tạo phản hồi theo mẫu "Tiếp nhận yêu cầu từ khách hàng"
* Tạo issue trên Gitlab, Assignee dev dựa vào [Danh sách website trên server](https://docs.sudo.company/doku.php?id=dev:danh_sach_cac_website_n%E1%BA%B1m_tren_cac_minion)
* Đánh giá độ nghiêm trọng, nếu là lỗi không truy cập được, bảo mật nhắn trực tiếp qua Slack
### Bước 2: Dev phụ trách ###
* Ưu tiên check và xử lý luôn ticket nghiêm trọng
* Kiểm tra issue assignee cho mình vào đầu giờ làm mỗi buổi sáng (hoặc thời gian rảnh chờ việc nếu có)
* Comment, hỏi đáp với khách trên ticket nếu chưa rõ yêu cầu
* Thông báo bắt đầu xử lý theo mẫu "Đang thực hiện yêu cầu, hẹn deadline hoàn thành"
* Lỗi phát sinh hoặc chỉnh sửa giao diện nhỏ, của khách còn bảo hành, thời gian xử lý muộn nhất vào ngày thứ ba hoặc thứ sáu gần nhất
* Yêu cầu nâng cấp tính năng (tất cả khác hàng), yêu cầu fix lỗi của khách hết bảo hành, ước tính thời gian thực hiện thực tế theo giờ (chia nhỏ công việc và tính thời gian với các tính năng lớn) nhắn cho supporter qua Slack (theo giờ thực hiện)
### Phase 3: Người phụ trách support ###
* Dựa vào hệ số mà công ty cung cấp cho mỗi dev, supporter sẽ tính chi phí và tạo báo giá cho khách, chờ khách xác nhận thực hiện
* Khi khách xác nhận thực hiện supporter sẽ comment thông tin lại trên issue, dev hỗ trợ sẽ báo thời điểm xong với khách
* Sau khi hoàn thành công việc cần xác nhận với khách trên ticket theo mẫu "Đã thực hiện xong, chờ xác nhận từ khách hàng"
* Các yêu cầu phức tạp về kỹ thuật, kéo dài về tính năng có thể tham khảo xin trợ giúp từ trưởng nhóm, trưởng phòng