# Hãy truy cập link dưới đây để biết thêm các Helper được hỗ trợ tại SudoCore #

[https://bitbucket.org/taitt/sudo-base/src/master/base/helpers/functions.php](https://bitbucket.org/taitt/sudo-base/src/master/base/helpers/functions.php)

Ngoài link được đặt trên có thể truy cập trực tiếp Foler `helpers` của các Package để xem thêm về các hàm hỗ trợ

# Xuất Excel #

Export Excel là một tính năng không thể thiếu dùng để xuất File định dạng Excel phục vụ cho việc thống kê và tính toán của người dùng. Và SudoCore đã hỗ trợ xuất File Excel đơn giản theo định dạng nhất định một cách cực kỳ đơn giản. Cụ thể File Excel sẽ có các thành phần như sau:

- Hàng đầu tiên: Chứa các Tên cột hay Tiêu đề biểu thị thông tin của cột đó.
- Các hàng tiếp theo sẽ chứa dữ liệu tương ứng với từng cột Tiêu đề ở trên.

Chúng ta sẽ đi vào Ví dụ để có thể hiểu hơn về xuất excel này nhé. VD: Chúng ta có dữ liệu được lưu trong Database có dạng như sau:

- Nguyễn Văn A - 18 tuổi - Học sinh
- Nguyễn Văn B - 24 tuổi - Nhân viên văng phòng
- Nguyễn Văn C - 30 tuổi - Nhân viên Xây dựng

Và để xuất các dữ liệu trên ra Excel chúng ta sẽ làm như sau:

    // Mảng export
    $data = [
        // Tên File Excel muốn xuất ra
        'file_name' => 'demo-'.time(),
        // Mảng chứa tên cột tương ứng với từng cột trong Excel tại hàng đầu tiên
        'fields' => [
            __('STT'),
            __('Họ Và tên'),
            __('Tuổi'),
            __('Nghề nghiệp'),
        ],
        // Mảng dữ liệu tương ứng từng cột (Thường là dữ liệu được lấy tại Database)
        'data' => [
            [ 1, 'Nguyễn Văn A', 18, 'Học sinh' ],
            [ 2, 'Nguyễn Văn B', 24, 'Nhân viên văng phòng' ],
            [ 3, 'Nguyễn Văn C', 30, 'Nhân viên Xây dựng' ],
        ]
    ];
    // Hàm return dùng để download file Excel
    return \Excel::download(new \Sudo\Base\Export\GeneralExports($data), $data['file_name'].'.xlsx');

Dữ liệu tại `$data['data']` phía trên là dữ liệu Demo, còn trên thực tế thì dữ liệu phải được lấy tại Database, dùng hàm `for` hay `foreach` để đưa vào giá trị tương ứng

    foreach ($demo as $key => $value) {
        $data['data'][] = [
            $loop->index+1,
            $value->name,
            $value->age,
            $value->jobs,
        ];
    }