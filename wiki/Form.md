# Hướng dẫn sử dụng Sudo Form #

Form là một phần không thể thiếu và là phần quan trọng nhất của một trang quản trị, nó dùng để chỉnh sửa các thông tin trên trang một cách nhanh chóng và SudoCore có hỗ trợ tạo form một cách dễ dàng, đơn giản cũng như hỗ trợ hầu hết các đạng Form phổ biến hiện nay.

## Khai báo để sử dụng ##

Trước tiên chúng ta sẽ nói về cách cài đặt để sử dụng, có 2 cách để có thể sử dụng Form Module:

**Cách 1:**

B1: chúng ta `use Form` ở dưới khai báo Namespace và trên đoạn khai báo Class

	<?php

	namespace Namespace;

	use Form;
	
	class DemoController {
		
	}

B2: Chúng ra sẽ sử dụng cú pháp dưới đây tại function của controller

	public function create() {
        // Khởi tạo form
        $form = new Form;

		// Các hàm tạo Form viết ở đây

		// Hiển thị form tại view
        return $form->render('create');
    }

**Cách 2:** Chúng ta sẽ sử dụng trực tiếp cú pháp khai báo nhanh của laravel và không cần `use Form`

	public function create() {
        // Khởi tạo form
        $form = new \Form;

		// Các hàm tạo Form viết ở đây

		// Hiển thị form tại view
        return $form->render('create');
    }

## Chi tiết cách dùng các dạng form được hỗ trợ ##

Trước khi vào chi tiết thì chúng ta sẽ thống nhất với nhau các param (Thuộc tính) chung dùng cho toàn Form, một vài param riêng thì sẽ được giải thích tại phần "Trong đó", các param chung hay dùng:

- $name: (string) Tên trường, thường là tên cột trong bảng
- $value: (string | array) Giá trị
- $required: (number) bắt buộc (0 Không | 1 có)
- $label: (string) Tiêu đề hiển thị
- $placeholder: (string) Gợi ý nhập
- $options: là các mảng giá trị có dạng [ key => value ]

### title() ###

Dùng để phân tách các phần, giúp quản trị nhận biết được các phần được phân tách với nhau.

	$form->title($label);



### note() ###

Dùng để gợi ý nhập 1 form nào đó

	$form->note($label);



### text() ###

Dùng để nhập các giá trị chuỗi

	$form->text($name, $value, $required, $label, $placeholder);



### smallText() ###

Dùng để nhập các giá trị chuỗi, nhưng ô nhập ngắn hơn text()

	$form->smallText($name, $value, $required, $label, $placeholder);



### hidden() ###

Dùng để set các giá trị mặc định nhưng ẩn


	$form->hidden($name, $value);



### password() ###

Dùng để điền các giá trị như mật khẩu

	$form->password($name, $value, $required, $label, $placeholder, $confirm);

Trong đó

- $confirm: ID của box muốn check trùng, dùng để kiểm tra trùng giá trị thường dùng cho "nhập lại mật khẩu"



### passwordGenerate() ###

Dùng để điền các giá trị như mật khẩu, mật khẩu sẽ tự đông bắt theo cấu trúc hợp lệ

	$form->passwordGenerate($name, $value, $required, $label, $placeholder, $confirm);

Trong đó

- $confirm: ID của box muốn check trùng, dùng để kiểm tra trùng giá trị thường dùng cho "nhập lại mật khẩu"



### slug() ###

Dùng để tạo đường đãn (slug), có thể bắt theo trường được chỉ định để tạo, có thể check giá trị trùng trong bảng

	$form->slug($name, $value, $required, $label, $extends, $unique, $table);

Trong đó

- $extends: ID trường bắt giá trị tự động VD: name
- $unique: có check tồn tại hay không (true | false)
- $table: nếu có check tồn tại thì check ở bảng nào



### textarea() ###

Dùng để tạo ô nhập chuỗi dài, VD: mô tả, ...

	$form->textarea($name, $value, $required, $label, $placeholder, $row);

Trong đó

- $row: số dòng nhập



### editor() ###

Dùng để tạo vùng nhập nội dung sử dụng tinymce

	$form->editor($name, $value, $required, $label);



### select() ###

Dùng để tạo form chọn, form này dùng để chọn 1 giá trị

	$form->select($name, $value, $required, $label, $options, $select2, $disabled);

Trong đó

- $select2: Có sử dụng select2 hay không (Mặc định có) (0 Không | 1 có)
- $disabled: mảng giá trị muốn disabled



### multiSelect() ###

Giống như form select nhưng có thể chọn nhiều giá trị

	$form->multiSelect($name, $value, $required, $label, $options, $placeholder, $select2, $disabled);

Trong đó

- $value: là mảng chứa các giá trị là key của $options
- $select2: Có sử dụng select2 hay không (Mặc định có) (0 Không | 1 có)
- $disabled: mảng giá trị disabled



### checkbox() ###

Dùng để tạo form checkbox, chỉ cho 1 giá trị

	$form->checkbox($name, $value, $checked, $label);

Trong đó:

- $checked: nếu $checked bằng với $value thì sẽ checked



### multiCheckbox() ###

Giống như form checkbox nhưng có thể chọn nhiều giá trị

	$form->multiCheckbox($name, $value, $required, $label, $options);

Trong đó

- $value: là mảng chứa các giá trị là key của $options



### radio() ###

Dùng để tạo form radio cho phép chọn 1 giá trị

	$form->radio($name, $value, $label, $options);



### tags() ###

Dùng để nhập tags liên quan, thường dùng cho module bài viết

	$form->tags($name, $value, $required, $label, $placeholder, $auto_click);

Trong đó

- $value: là mảng chứa các giá trị là tên của tags
- $auto_click: có tự động click sau 1 khoảng thời gian hay không (0 Không | 1 có) (Mặc định có)



### image() ###

Dùng để chọn ảnh tại module Media, form này lấy ra 1 ảnh

	$form->image($name, $value, $required, $label, $title_btn);

Trong đó

- $title_btn: text tại nút chọn ảnh



### multiImage() ###

Dùng để chọn ảnh tại module Media, form này lấy ra nhiều ảnh

	$form->multiImage($name, $value, $required, $label, $title_btn);

Trong đó

- $value: là mảng chứa các giá trị là link của ảnh
- $title_btn: text tại nút chọn ảnh



### datepicker() ###

Dùng để chọn ngày

	$form->datepicker($name, $value, $required, $label, $placeholder);



### datetimepicker() ###

Dùng để chọn ngày giờ

	$form->datetimepicker($name, $value, $required, $label, $placeholder);



### suggest() ###

Dùng để tìm kiếm và chọn 1 giá trị tại bảng chỉ định

	$form->suggest($name, $value, $required, $label, $placeholder, $suggest_table, $suggest_id, $suggest_name);

Trong đó

- $value: là $suggest_id thuộc bảng $suggest_table
- $suggest_table: tên bảng muốn tìm kiếm
- $suggest_id: trường muốn lấy (mặc định là id)
- $suggest_name: tên giá trị hiển thị (mặc định là name)



### multiSuggest() ###

Dùng để tìm kiếm và chọn nhiều giá trị tại bảng chỉ định

	$form->multiSuggest($name, $value, $required, $label, $placeholder, $suggest_table, $suggest_id, $suggest_name);

Trong đó

- $value: là mảng $suggest_id thuộc bảng $suggest_table
- $suggest_table: tên bảng muốn tìm kiếm
- $suggest_id: trường muốn lấy (mặc định là id)
- $suggest_name: tên giá trị hiển thị (mặc định là name)



### custom() ###

Dùng để nhúng form tự phát triển vì một vài form đặc biệt cần tạo riêng

	$form->custom($template, $param);

Trong đó

- $template: là view chứa form
- $param: giá trị truyền vào của form đó

Phần này có một form đặt biệt đó là form chuyên dùng để cấu hình Infinity Add, cụ thể form này sẽ tạo ra 1 form có cấu trúc được quy định sẵn và sẽ có nút để thêm vô hạn. Dưới đây là demo cấu hình của form vô hạn chứa: 1 ảnh, 2 text và 1 textarea

	$form->custom('Form::custom.form_custom', [
        'has_full' => true,
        'name' => 'list',
        'value' => $data['list'] ?? [],
        'label' => 'Cấu hình Demo',
        'generate' => [
            [ 'type' => 'image', 'name' => 'image', 'size' => 'Ảnh có kích thước '.'XXXxXXX', ],
            [ 'type' => 'custom', 'generate' => [
                    [ 'type' => 'text', 'name' => 'text_1', 'placeholder' => 'Tiêu đề', ],
                    [ 'type' => 'text', 'name' => 'text_2', 'placeholder' => 'Link', ],
                ]
            ],
            [ 'type' => 'textarea', 'name' => 'textarea', 'placeholder' => 'Mô tả', ],
        ],
    ]);



### customMenu() ###

Form chuyên dùng để tại menu kéo thả

	$form->custom($name, $value, $label, $template);

Trong đó

- $template: là view muốn tự custom thay vì dùng view mặc định của core

Phần menu có hộ trợ thêm các link giúp người quản trị dễ dạng chọn được link theo đúng ý mình, cụ thể chúng ta sẽ cấu hình tại `config/app.php`, dưới đây là phần demo cấu hình cho customMenu

	// Menu form
    'menu_form' => [
        // Link cố định
        'menu_link' => [
            'vi' => [
                '/' => 'Trang chủ',
                '/lien-he' => 'Liên hệ',
            ],
            'en' => [
                '/' => 'Home Page',
                '/contact' => 'Contact',
            ],
        ],
        // Theo bảng
        'table_link' => [
            'pages' => [
                'name' => 'Trang đơn',
                'models' => 'Sudo\Theme\Models\Page',
                'has_locale' => true
            ],
            'post_categories' => [
                'name' => 'Danh mục bài viết',
                'models' => 'Sudo\Theme\Models\PostCategory',
                'has_locale' => true
            ],
        ],
    ],

Cụ thể cấu hình sẽ có 2 dạng là: cấu hình link được fix cứng (VD: Trang chủ, liên hệ, giới thiệu, các trang landing page, ...) và cấu hình link lấy theo bảng (Danh mục, trang đơn, ...)

**Link được fix cứng:** 

Link sẽ được cấu hình theo ngôn ngữ (Dù web 1 ngôn ngữ vẫn sẽ cần đặt đúng theo cấu trúc).

Cấu trúc theo dạng [ key => value ], trong đó: key là link và value là tên hiển thị thay cho link

**Link theo bảng**

Các link sẽ được lấy với toàn bộ record của bảng được quy định và sẽ có cấu trúc như ví dụ ở trên.

Cấu trúc theo dạng sau:

	'ten_cua_bang' => [
        'name' => 'Tên của bảng',
        'models' => 'Model có chứa hàm getCustomMenu()',
        'has_locale' => true // Có đa ngôn ngữ hay không
    ],

Hàm `getCustomMenu()` mặc định đã được hỗ trợ tại BaseModel nên nếu model được extends BaseModel thì sẽ có hàm này nhưng nếu không extends lại BaseModel hoặc muốn tự custom lại thì sẽ phải viết lại hàm và trả về giá trị có dạng như sau

	[
		'link_1' => [
			'id'        => id,
            'name'      => 'Tên hiển thị'
		],
		'link_2' => [
			'id'        => id,
            'name'      => 'Tên hiển thị'
		]
	]


### action() ###

Đây là phần tạo các nút submit cho form

	$form->action($type, $preview, $exit_url, $custom);

Trong đó

- $type: là tên các button hiển thị mặc định được core hỗ trợ, và hiện tại core đang hỗ trợ 3 dạng chính: add (Form thêm), edit (form sửa), editconfig (form cấu hình)
- $preview: là link của button "xem"
- $exit_url: là link của button "thoát"
- $custom: phần này sẽ nói chi tiết ở phần dưới đây

Có một vài form đặc biệt ngoài trừ các button submit được hỗ trợ thì vẫn muốn có thêm các button khác với mục đích nâng cao trải nghiệm người dùng VD: như button "Thêm và thêm mới" (Core hiện tại không hỗ trợ button này) và do đó SudoCore có hỗ trợ cấu hình thêm các nút tại các form mặc định cũng như form tự mình custom, ta có ví dụ sau:

	$form->action('custom', '', '', [
        [ 
            'type' => 'submit', 
            'label' => 'Lưu',
            'btn_type' => 'success',
            'icon' => 'fas fa-save',
            'value' => 'edit',
        ],
        [ 
            'type' => 'link',
            'label' => 'Thoát',
            'btn_type' => 'danger',
            'icon' => '',
            'link' => '#',
        ],
    ]);

Như ví dụ trên thì chúng ta sẽ tạo ra được 2 button là "Lưu" và "Thoát", thay vì sử dụng type được hỗ trợ mặc định (add | edit | editconfig). Các type mặc định cũng hỗ trợ dạng custom này.

**Tông kết**

- Trên đây là toàn bộ các Form có sẵn mà SudoCore hỗ trợ, nếu như Form được hỗ trợ không đúng với dạng Form mà mọi người cần thì mọi người có thể dùng Hàm custom() để viết lại.
- Mọi người có thể vào [đây](https://bitbucket.org/taitt/sudo-base/src/master/form/src/MyClass/FormBuilder.php) để xem lại các thuộc tính của form

## Form Horizontal và Form Vetical ##

**Form Horizontal**

- Đây là form mặc định của core
- Form có 2 cột: Cột bên phải (label) chia theo lưới 3 và cột bên trải (input, select, ...) chia theo lưới 8

**Form Vetical**

- Đây là form dạng 1 cột
- Form có label và input full-width
- Form Vetical thường đi với Form nhiều cột
- Để có thể chuyển từ Form Horizontal sang Form Vetical thì chúng ra sẽ sử dụng hàm sau trước khi `return` và sau khai báo $form:

		$form->hasFullForm();

## Form dạng 1 cột và nhiều cột ##

### Form dạng 1 cột ###

Đây là form theo dạng cũ và là form mặc định của core.

	public function create()
    {   
        // Khởi tạo form
        $form = new \Form;

        $form->text('name', '', 1, 'Tiêu đề');
        $form->slug('slug', '', 1, 'Đường dẫn');
        $form->editor('detail', '', 0, 'Nội dung');
        $form->tags('tags', [], 0, 'Tags', 'Điền tên tags và nhấn Thêm');

        // Hiển thị form tại view
        return $form->render('create');
    }

### Form dạng nhiều cột (Form Main + Form sidebar) ###

Đây là form theo dạng cột và để sử dụng thì chúng ra sẽ xử dụng hàm:

	$form->card('class của cột dùng lưới bootstrap VD: col-lg-9', 'Ghi chú');
		// Đặt $form ở đây
	$form->endCard();

Demo:

	public function create()
    {   
        // Khởi tạo form
        $form = new \Form;
		
		// Form Main
        $form->card('col-lg-9');
            $form->text('name', '', 1, 'Tiêu đề');
        $form->endCard();

		// Form sidebar
        $form->card('col-lg-3', '');
            $form->action('add');
        $form->endCard();

        // Hiển thị form tại view
        $form->hasFullForm();
        return $form->render('create_multi_col');
    }


## Form Render ##


Mặc định SudoCore đã hỗ trợ view để đổ dữ liệu cụ thể chúng ra sẽ sử dụng hàm render của Module Form:

	return $form->render($view, $compact, $custom_view);

Trong đó:

- $type: là tên dạng form được hỗ trợ mặc định sẵn tại Core (create | create_multi_col | edit | edit_multi_col)
- $compact: giá trị truyền vào view
- $custom_view: view custom trả về nếu không muốn sử dụng view mặc định

**Render Form dạng 1 cột**
	
	return $form->render('create');
	return $form->render('edit');

**Render Form dạng nhiều cột**

	return $form->render('create_multi_col');
	return $form->render('edit_multi_col');

**Render Form Custom**

	return $form->render('custom', $compact, $view_custom);