# Checklist hoàn thành dự án
## Trước khi đẩy test
* Đảm bảo tất cả các url khả dụng. Tất cả các url truy cập không có lỗi.

* Đảm bảo tất cả các trang giao diện đúng thiết kế.

* Đảm bảo giao diện hoạt động tốt trên tất cả các thiết bị có kích thước màn hình khác nhau, các hệ điều hành khác nhau.

* File robots.txt ở trạng thái chặn

```
#!txt

User-agent: *
Disallow: /
```

* File favicon.ico 

Cắt ảnh png có kích thước 16x16. Sử dụng công cụ convert sang ico tại trang [https://convertico.com/](https://convertico.com/)


* Sitemap.xml 

Tìm hiểu sitemap là gì: [https://nguyencaotu.com/file-sitemap-xml-la-gi.html](https://nguyencaotu.com/file-sitemap-xml-la-gi.html)

Package Sitemap sử dụng trên SudoCore: [https://github.com/Sudo-E-Commerce/sitemap](https://github.com/Sudo-E-Commerce/sitemap)

* RSS feed 

Tìm hiểu Rss là gì:  trên google hoặc [wiki](https://vi.wikipedia.org/wiki/RSS_(%C4%91%E1%BB%8Bnh_d%E1%BA%A1ng_t%E1%BA%ADp_tin)) 

Package Rss cho SudoCore: [https://github.com/Sudo-E-Commerce/rss](https://github.com/Sudo-E-Commerce/rss) 

* Đủ title, description

Đảm bảo có đủ các thẻ title, description cho tất cả các trang. Sudo core đã hỗ trợ sẵn module MetaSeos. 

Lấy dữ liệu MetaSeos thông qua hàm `metaSeo` (demo tại Sudo\Theme\Http\Controllers\Web\HomeController@index). 

Lưu ý blade layout sử dụng phải include layout.seo default `@include('Default::general.layouts.seo')`


* Thẻ canonical cho tất cả các trang

Thẻ canonical là thẻ chỉ định URL chuẩn, tránh trùng lặp, loại bỏ giá trị seo ở các url có nội dung trùng lặp hoặc tương tự. VD: `<link rel="canonical" href="https://sudo.vn" />`.

* Nhập liệu dữ liệu mỗi loại ít nhất 1 bản ghi đầy đủ tất cả các trường

* Khai báo thêm các thẻ og FB

Phần này đã được hỗ trợ tự động nếu dùng layout.seo default trên startproject

```
#!html

<meta property="og:locale" content="vi_VN" />
<meta property="og:site_name" content="SudoCMS" />
<meta property="og:type" content="website" />
<meta property="og:title" content="Sudo E-Commerce" />
<meta property="og:url" content="https://sudo.vn" />
<meta property="og:image" content="https://sudo.vn/public/assets/img/logo.png" />
```


* Thẻ hreflang với website đa ngôn ngữ

Chỉ định các ngôn ngữ tương ứng của trang hiện tại nếu có. VD: `<link rel="alternate" hreflang="en" href="https://sudo.vn/en" />`

* Gọi đúng resize ảnh theo kích thước thiết kế

Các kích thước khung ảnh trong thiết kế thường sẽ nhỏ hơn ảnh gốc người dùng upload lên, nên gọi các resize gần nhất của ảnh với kích thước thiết kế để tối ưu dung lượng load website.

* Các thẻ img có nội dung thẻ alt

* Sử dung thẻ H1 với các tiêu đề danh mục, tiêu đề sản phẩm, tin tức, trang. Sử dụng H3 cho các tiêu đề sản phẩm, tin tức ở trang danh sách

* Phân trang tất cả các trang danh sách

* Có Trang 404

Sử dụng cách Handler mặc định của Laravel với các link 404. Custom giao diện trang 404 theo [hướng dẫn](https://laravel.com/docs/7.x/errors#custom-http-error-pages)

* Chặn thẻ meta robots ở trang tìm kiếm

`<meta name="robots" content="noindex,nofollow" />`

* Tốc độ load web ẩn danh dưới 3s

* Tối ưu pagespeed (tối ưu ảnh + check gzip)

* Đánh dấu dữ liệu có cấu trúc cho google shoping nếu có

* Tắt debug

## Trước khi đẩy domain chính
* Check www, non-www

Đảm bảo www.domain.com redirect 301 về domain.com

* Mở robot

```
#!txt

User-agent: *
Allow:/
Disallow:/tim-kiem/
Sitemap: https://domain.com/sitemap.xml

```