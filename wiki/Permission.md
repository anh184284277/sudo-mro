# Hướng dẫn cấu hình phân quyền #

Một route có thể một người hay nhiều người truy cập và để quản lý được quyền truy cập của từng cá nhân tới mỗi route sẽ được SudoCore cấu hình tự động một cách nhanh chóng. 

Cụ thể, chúng ta sẽ cấu hình Quyền tại file `config/SudoModule.php` (Được tạo ra sau khi Publish Core) và dưới đây sẽ là cách để cấu hình quyền truy cập cho từng route theo từng module

## Cách cấu hình ##

Chúng ta sẽ cấu hình phân quyền tại mảng `modules` thuộc `config/SudoModule.php`. Ví dụ:

![permision.PNG](https://bitbucket.org/repo/xXzd9GA/images/1497653019-permision.PNG)

	'admin_users' => [
		'name' 			=> 'Tài khoản quản trị',
		'permision' 	=> [
			[ 'type' => 'index', 'name' => 'Truy cập' ],
			[ 'type' => 'create', 'name' => 'Thêm' ],
			[ 'type' => 'edit', 'name' => 'Sửa' ],
			[ 'type' => 'restore', 'name' => 'Lấy lại' ],
			[ 'type' => 'delete', 'name' => 'Xóa' ],
		],
	],

Cấu hình trên là ví dụ cho toàn bộ quyền của "Tài khoản quản trị". Trong đó:

* key: Là key module, thường là tên bảng và là duy nhất. Ở VD trên thì "key" là "admin_users"
* name: Là tên hiển thị của module
* permision: là các quyền của module
	* type: là giá trị sau dấu chấm cuối cùng của route name. Như VD trên, chúng ta muốn phân quyền cho 1 tài khoản xem danh sách "Tài khoản quản trị" thì phải có 1 route có name là "admin.admin_users.index" được định nghĩa từ trước và type trong trường hợp này sẽ là "index"
	* name: là tên hiển thị của route tại phân quyền, nếu không có thì sẽ lấy tại mảng `name` thuộc `config/SudoModule.php` với "key" là "type"

## Cách xác định role ##

role là các quyền truy của 1 tài khoản với 1 route. VD: ta có một mảng phân quyền module cho trước

	'system_logs' => [
		'name' 			=> 'Lịch sử hệ thống',
		'permision' 	=> [
			[ 'type' => 'index', 'name' => 'Truy cập' ],
			[ 'type' => 'show', 'name' => 'Xem chi tiết' ],
		],
	],

role sẽ được xác định là key nối với type của permision bằng dấu gạch dưới (_). Như trong VD trên thì ta sẽ có 2 role được xác định là: `system_logs_index` (Truy cập lịch sử hệ thống) và `system_logs_show` (Xem chi tiết lịch sử hệ thống)

## Hàm kiểm tra phân quyền checkRole($role) ##

Hàm này sẽ giúp chúng ra kiểm tra xem tài khoản hiện tại có quyền truy cập vào route có $role hay không.

Như ví dụ ở phần "Cách xác định role" thì chúng ra đã xác định role chứa quyền truy cập "Lịch sử hệ thống" là `system_logs_index` và để kiểm tra người dùng hiện tại có quyền truy cập vào role này hay không thì chúng ra sẽ xử dụng hàm checkRole($role)

	checkRole('system_logs_index')
	Trả về true nếu có quyền và false nếu không có quyền

	if (checkRole('system_logs_index')) {
	    // Có quyền
	} else {
		// Không có quyền
	}

Một vài controller và view đặc biệt muốn kiểm tra phân quyền thì phải dùng hàm checkRole() để kiểm tra. VD như một vài widget ở dashboard chỉ muốn hiển thị cho một vài tài khoản.