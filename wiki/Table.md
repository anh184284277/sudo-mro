# Hướng dẫn sử dụng Sudo Table #

Bảng hay danh sách là cách để người dùng quản lý toàn bộ dữ liệu, nó dùng để xem, sửa hay thay đổi trạng thái một cách đơn giản và hiệu quả chính vì vậy SudoCore có hỗ trợ tạo bảng một cách nhanh chóng, và dưới đây chúng ta sẽ cùng đi sâu vào phần bảng và quản lý dữ liệu nhé.

## Khai báo để sử dụng ##

Trước tiên chúng ta sẽ nói về cách cài đặt để sử dụng, có 2 cách để có thể sử dụng Form Table:

**Cách 1:**

B1: chúng ta `use Listdata` ở dưới khai báo Namespace và trên đoạn khai báo Class

	<?php

	namespace Namespace;

	use Listdata;
	
	class DemoController {
		
	}

B2: Chúng ra sẽ sử dụng cú pháp dưới đây tại function của controller

	public function index(Request $requests) {
        $listdata = new ListData($requests, $model, $view, $has_locale);
        return $listdata->render();
    }

**Cách 2:** Chúng ta sẽ sử dụng trực tiếp cú pháp khai báo nhanh của laravel và không cần `use Listdata`

	public function index(Request $requests) {
        $listdata = new \ListData($requests, $model, $view, $has_locale);
        return $listdata->render();
    }

## Chi tiết cách dùng ##

### ListData() ###

Đây là hàm khai báo dữ liệu bảng cơ bản, tham số được khai báo tại đây là rất quan trọng

	$listdata = new \ListData($request, $model_name, $view, $has_locale, $page_size, $order);

Trong đó:

- $request: là hàm lấy các giá trị paramt trên url để sử dụng, thường sẽ phục vụ cho "Tìm kiếm" và "Sắp xếp"
- $model_name: là nơi khai báo models của modules
- $view: là view chứa các thẻ <td></td> của bảng dùng để hiển thị dữ liệu, vì mỗi modules sẽ có cách hiển thị khác nhau
- $has_locale: bảng có sử dụng đa ngôn ngữ hay không, nếu có thì dữ liệu sẽ được lấy theo đa ngôn ngữ (Mặc định không)
- $page_size: số lượng bản ghi trên 1 trang (Mặc định là 30)
- $order: sắp xếp mặc định, (Mặc định sắp xếp theo id DESC)



### ListDataCategory() ###

Đây là hàm khai báo dữ liệu của các bảng là danh mục (Thường là các bảng có parent_id), bảng không phân trang và hiển thị theo cấp cha con, các phần tiếp theo dùng giống các hàm thuộc ListData()

	$listdata = new \ListDataCategory($request, $model_name, $view, $has_locale, $page_size, $order);

Thuộc tính của ListDataCategory() giống với ListData() cũng như các thành phần phía dưới



### Tìm kiếm ###

Sử dụng hàm search() để tạo các input Search cho bảng:

	$listdata->search($field_name, $label, $field_type, $option);

Trong đó:

- $field_name: Tên trường tìm kiếm, thường là tên bảng
- $label: Text hiển thị
- $field_type: loại input dùng để search, hiện tại hỗ trợ 3 dạng input đó là:
	- string: input text - chuỗi thường 
	- array: select - mảng
	- range: input rangerpicker - khoảng thời gian
- $option: Trường này hỗ trợ cho các trường có $field_type là array, chứa giá trị dạng [key => value]

Demo:

	$listdata->search('name', 'Tên', 'string');
    $listdata->search('created_at', 'Ngày tạo', 'range');
    $listdata->search('status', 'Trạng thái', 'array', config('app.status'));

Có một vài hành động đặc biệt muốn sử dụng Form Seach để thực hiện hành động tiếp theo, VD: Xuất excel, thì chúng ta có thể sử dụng hàm searchBtn() để thêm 1 hoặc nhiều button submit cạnh button "Tìm kiếm":

	$listdata->searchBtn($label, $url, $btn_type, $btn_icon);

Trong đó

- $label: Text hiển thị nút
- $url: route url xử lý hành động, thường là route Post xử lý form
- $btn_type: Loại nút hiển thị (success | danger | primary | warning | default | info | secondary)
- $btn_icon: icon hiển thị nút (lấy theo icon font-awesome)



### Các nút thao tác nhanh ###

Sử dụng hàm btnAction() để tạo các button thao tác nhanh VD: đổi trạng thái hoặc xóa các ô được chọn

	$listdata->btnAction($field_name, $value, $label, $btnType, $icon, $url);

Trong đó

- $field_name: tên cột
- $value: giá trị cần thay đổi
- $label: tên button hiển thị
- $btnType: class màu sắc của nút, lấy theo tên rút ngắn btn- của bs3 (primary | default | danger | warning | ...)
- $icon: icon hiển thị
- $url: Có thể tự cấu hình route sử lý tại button

Demo:

	$listdata->btnAction('status', 1, __('Translate::table.active'), 'success', 'fas fa-edit');
    $listdata->btnAction('status', 0, __('Translate::table.no_active'), 'info', 'fas fa-window-close');
    $listdata->btnAction('delete', -1, __('Translate::table.trash'), 'danger', 'fas fa-trash');

	// Xóa nhanh sử dụng hàm destroy của chính controller module
	$listdata->btnAction('delete_custom', -1, __('Translate::table.trash'), 'danger', 'fas fa-trash');

	// Update trạng thái sử dụng route('admin.table.change_status')
	$listdata->btnAction('status', 1, __('Translate::table.active'), 'success', 'fas fa-edit', route('admin.table.change_status'));


### Thêm cột vào bảng ###

Để thêm cột vào bảng chúng ta sử dụng hàm add():

	$listdata->add($field_name, $label, $has_order, $type, $option)

Trong đó:

- $field_name: tên cột
- $label: tên cột hiển thị
- $has_order: có sắp xếp theo cột hay không (1 có | 0 không)
- $type: Loại hiển thị mặc định, các cột được core hỗ trợ sẵn
	- pin: Hiển thị giá trị ghim
	- time: Hiển thị thời gian thêm và cập nhật
	- status: Hiển thị form sửa nhanh
	- lang: Cột đa ngôn ngữ và kiểm tra tồn tại ngôn ngữ hay chưa (Có tác dụng khi có cấu hình đa ngôn ngữ)
	- order: Form sắp xếp
	- show: Link đến trang chi tiết của bản ghi
	- edit: Link đến trang sửa của bản ghi
	- delete: Xóa sử dụng quick_delete
	- delete_custom: Xóa sử dụng hàm destroy của chính controller module
	- restore: Lấy lại nhanh
- $option: mảng option theo type là status

Demo:

	$listdata->add('image', 'Ảnh', 0);
    $listdata->add('name', 'Tên', 1);
    $listdata->add('', 'Thời gian', 0, 'time');
    $listdata->add('status', 'Trạng thái', 1, 'status');
    $listdata->add('', 'Language', 0, 'lang');
    $listdata->add('', 'Sửa', 0, 'edit');
    $listdata->add('', 'Xóa', 0, 'delete');



### Ẩn button Thêm ###

Dành cho bảng không sử dụng thêm VD: Liên hệ, đơn hàng,....

	$listdata->no_add();



### Ẩn button xem thùng rác ###

Dành cho bảng không sử dụng thùng rác

	$listdata->no_trash();



### Listdata Render ###

Để đổ ra view thì chúng ta đã có hàm render():

	return $listdata->render($compact, $view);

Trong đó:

- $compact: chứa các giá trị muốn truyên vào
- $view: đổ về view nào, Mặc định đổ vể view có sẵn của core của core nếu không muốn dùng thì phải tự custom view hiển thị



### Thêm các view vào trên và dưới bảng ###

Một vài bảng đặc biệt cần hiển thị 1 view nào đó ở trên hoặc dưới bảng, VD: một vài modules cần thêm nhanh ở ngay tại bảng, thì SudoCore cũng đã có hỗ trợ:

	
	// Thêm ở trên bảng
	$include_view_top => [
		'view_1' => [
			// paramt của view_1
		],
		'view_2' => [
			// paramt của view_2
		],
	];

	// Thêm ở dưới bảng
	$include_view_bottom => [
		'view_1' => [
			// paramt của view_1
		],
		'view_2' => [
			// paramt của view_2
		],
	];

	// Sử dụng compact để truyền giá trị sang view table
	return $listdata->render(compact('include_view_top', 'include_view_bottom'));

Demo:

	$include_view_top => [
		'Theme::demo' => [
			'title' => 'Đây là tiêu đề',
		],
	];
	$include_view_bottom => [
		'Theme::demo' => [
			'title' => 'Đây là tiêu đề',
		],
	];
	return $listdata->render(compact('include_view_top', 'include_view_bottom'));



### Một vài View Include hỗ trợ ###

Cột thời gian

	@include('Table::components.time')

Sửa giá trị text mảng nhanh

	@include('Table::components.edit_array',[
		'name' => 'Tên cột',
		'width' => 'Độ dài của cột px',
	])

Sửa giá trị mảng nhanh

	@include('Table::components.edit_array',[
		'name' => 'Tên cột',
		'value' => 'Giá trị là key của mảng $option', 
		'options' => [
			// Mảng giá trị dạng [key => value]		
		]
	])

Cột hành động

	@include('Table::components.action',['type' => $type])

Trong đó: $type là các hành động được hỗ trợ, và chứa các giá trị dưới đây:

- show: Cột link xem chi tiết
- edit: Cột sửa chi tiết
- delete: Cột xóa, sử dụng quick_delete
- delete_custom: Cột xóa sử dụng hàm "destroy" của controller modules
- restore: Cột lấy lại bản ghi đã xóa

Cột ảnh

	@include('Table::components.image', [
		'image' => 'Link ảnh'
	)])

Cột Link

	@include('Table::components.link', [
		'text' => 'Tên hiển thị',
		'url' => 'Link'
	)])

Cột text

	@include('Table::components.text', [
		'text' => 'Tên hiển thị'
	)])