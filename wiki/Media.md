# Hướng dẫn cấu hình Media Module #

Media hay File manager là một phần không thiếu của một trang quản trị, nó dùng để quản lý hình ảnh và File cho toàn trang một cách thuận tiên và dễ sử dụng nhất và chính vì vậy SudoCore cũng không thể thiếu Module quan trọng này.

Media Module tạo ra giao diện quản lý được đặt tại `/{admin_dir}/media/view` , trong đó admin_dir là đường dẫn admin được đặt tại `config('app.admin_dir')`

## Cấu hình tại Menu ##

	[
    	'type' 		=> 'single',
		'name' 		=> 'Quản lý tập tin',
		'icon' 		=> 'fas fa-photo-video',
		'route' 	=> 'media.view',
		'role'		=> 'media_view'
	],

- Vị trí cấu hình được đặt tại `config/SudoMenu.php`
- Để có thể hiển thị tại menu, chúng ta có thể đặt đoạn cấu hình trên tại `config('SudoMenu.menu')`



## Publish ##

Để tạo ra File cấu hình và các file css/js hỗ trợ ta có thể sử dụng các lệnh dưới đây: 

* Khởi tạo chung theo core
	- Tạo assets và configs `php artisan vendor:publish --tag=sudo/core`
	- Chỉ tạo assets `php artisan vendor:publish --tag=sudo/core/assets`
	- Chỉ tạo configs `php artisan vendor:publish --tag=sudo/core/config`
* Khởi tạo riêng theo package
	- Tạo cả assets và configs `php artisan vendor:publish --tag=sudo/media`
	- Chỉ tạo assets `php artisan vendor:publish --tag=sudo/media/assets`
	- Chỉ tạo configs `php artisan vendor:publish --tag=sudo/media/config`



## Cấu hình nơi lưu trữ ##

### Local ###

Để muốn sử dụng được tại Local thì chúng ra làm theo các bước sau:

**B1:** chúng ra phải sửa `config('filesystems.disks.local.root')` thành `public_path()`

**B2:** Chúng ta sẽ đặt nơi lưu trữ

- Cách 1: Đổi `config('SudoMedia.storage_type')` thành `local` 
- Cách 2: Thêm `STORAGE_TYPE=local` tại `.env`

**B3:** Chúng ta sẽ đặt tên Folder lưu trữ 

- Cách 1: Đổi tên thư mục lưu trữ tại `config('SudoMedia.folder')`
- Cách 2: Thêm `FOLDER=ten_folder` tại `.env`, VD: `FOLDER=uploads`

### Các bên thứ 3 ###

Các bên thứ 3 thường là các bên có gói dịch vụ lưu trữ online, VD: Digital Ocean (DO), ..., và để Media Module kết nối đến các dịch vụ này thì chúng ta làm theo các bước sau:

Ví dụ cho kết nối đến DO:

**B1:** Chúng ra sẽ set cấu hình kết nối tới các gói dịch vụ tại `config('filesystems')`

	// config/filesystems.php

	<?php

	return [
		...
		'disks' => [
			...

	        'do' => [
	            'driver' => 's3',
	            'key' => env('DO_KEY'),
	            'secret' => env('DO_SECRET'),
	            'region' => env('DO_REGION'),
	            'bucket' => env('DO_BUCKET'),
	            'endpoint' => env('DO_ENDPOINT'),
	            'domain' => env('DO_DOMAIN'),
	            'folder' => env('FOLDER'),// Đặt tên theo domain kiểu sudo-vn (đổi . thành -)
	        ],

	    ],
		...
	];

**B2:** Chúng ta sẽ đặt nơi lưu trữ

- Cách 1: Đổi `config('SudoMedia.storage_type')` thành `do` (Là key của cấu hình do tại B1) 
- Cách 2: Thêm `STORAGE_TYPE=do` tại `.env`

**B3:** Chúng ta sẽ đặt tên Folder lưu trữ 

- Cách 1: Đổi tên thư mục lưu trữ tại `config('SudoMedia.folder')` VD: `'folder' => env('FOLDER','ten-du-an'),`
- Cách 2: Thêm `FOLDER=ten_folder` tại `.env`, VD: `FOLDER=ten-du-an`

## Các cấu hình khác ##

### Cấu hình thay thế đường dẫn ảnh ###

Đây là nơi chúng ra đặt lại các link ảnh cũ đã được lưu trữ tại DB (Đổi nơi lưu trữ, nâng cấp server, chuyển link dạng CDN, ...) thành link ảnh mới để không bị lỗi ảnh, và để sử dụng tính năng này thì link ảnh sẽ phải được đặt trong hàm getImage($link, $size). Ta có ví như sau:

	/* Linh ảnh cũ sẽ được thay bằng link ảnh mới đặt tại image_new nếu qua hàm getImage() */
    'image_old' => [
		'https://chanhtuoi.com/',
		'https://www.chanhtuoi.com/'
	],

    /* Linh ảnh mới sẽ được thay bằng link ảnh cũ đặt tại image_old nếu qua hàm getImage() */
    'image_new' => 'https://cdn.chanhtuoi.com/',

Như ví dụ trên chúng ra có thể thấy nếu đi qua hàm getImage() thì toàn bộ link ảnh tại image_old sẽ được đổi sang link ảnh mới image_new

### Cấu hình resize hình ảnh ###

1 ảnh để có thể hiển thị tối ưu trên thì chúng ta cần phải resize ảnh, đây là bước rất quan trọng vì ảnh sau khi được resize có dung lượng nhỏ hơn ảnh gốc rất nhiều và có rất nhiều lợi ích như dụng lượng web giảm, web load nhanh hơn, chúng ta có cấu hình demo như sau:

	 /* Các kích thước resize Ảnh */
    'imageSize' => [
        'large' => 600,
        'medium' => 300,
        'small' => 150,
        'tiny' => 80
    ],

Như cấu hình trên khi uploads thì chúng ta sẽ sinh ra 4 dạng ảnh được resize theo từng kích thước chiều ngang. Như VD trên, khi chúng ta uploads ảnh có tên là image.png thì chúng ra sẽ nhận được 4 dạng ảnh có tên là: image-large.png, image-medium.png, image-small.png, image-tiny.png

### Cấu hình đuôi extension của file/ảnh cho phép uploads ###

Hãy hạn chế người dùng uploads file có đuôi độc (.php, ...) lên để bảo mật server. Chúng ta sẽ cấu hình tại `config('SudoMedia.allowed_extension_image')` và `config('SudoMedia.allowed_extension_file')`

	/* Đuôi Ảnh chấp nhận cho up */
    'allowed_extension_image' => ['jpg','jpeg','png','gif'],

	/* Đuôi file chấp nhận cho up */
    'allowed_extension_file' => ['pdf','xlsx','xls'],

### Cấu hình kích thước file được uploads tối đa ###

Giới hạn kích thước uploads lên tại `config('SudoMedia.allowed_size')`

	/* Kích thước tối đa từng File (2MB) */
    'allowed_size' => 2097152,

### Số lượng phân trang hiển thị ảnh ###

	/* Ảnh hiển thị trong 1 trang */
    'paginate' => 50,

### Cấu hình bảo mật middleware và cấu hình đường dẫn admin ###

	/* middleware. VD ['web', ...] */
    'middleware' => ['web', 'auth-admin'],

    /* đường dẫn admin */
    'admin_dir' => env('ADMIN_DIR', 'admin'),




## Sử dụng tại Form ##

Có thể sử dụng tại bất cứ thẻ nào chỉ cần thẻ có thuộc tính data-image và thuộc tính data-image có giá trị là `link_media`

**link_media**

Cấu trúc của link

	{domain}/{admin_dir}/media?uploads={type}&field_id={id_field_name}&only={file_type}

Để sinh `link_media` ta có thể sử dụng hàm dưới đây

	route('media.index', [
        'uploads' => {type},
        'field_id' => {id_field_name},
        'only' => {file_type}
    ])

Trong đó:

- `{domain}`: là domain hiện tại của trang thường là env('APP_URL')
- `{admin_dir}`: là đường dẫn admin được quy định tại config('SudoMedia.admin_dir')
- `{type}`: Hình thức uploads, hiện tại đang có 4 load hình thức
	+ direct: Đổ trực tiếp vào thẻ `img` có ID là `#{id_field_name}` và input có ID là `#input-{id_field_name}`
	+ single: Đổ trực tiếp HTML vào `DIV` có ID là `#{id_field_name}_box` nhưng chỉ có 1 ảnh
	+ multiple: Đổ trực tiếp HTML vào `DIV` có ID là `#{id_field_name}_box` có thể 1 hoặc nhiều ảnh
	+ editor: Đổ trực tiếp vào `Tinymce` có ID là `#{id_field_name}`
- `{id_field_name}`: ID của form đại diện
- `{file_type}`: Loại file lấy ra (image | file)








