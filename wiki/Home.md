# Trang hệ thống tài liệu web development tại Sudo Ecommerce #


![logo.png](https://bitbucket.org/repo/xXzd9GA/images/2347485118-logo.png)



Workflow:

* [Quy trình thực hiện dự án](https://bitbucket.org/taitt/sudo-startproject/wiki/WebDesignWorkflow)
* [Checklist hoàn thành dự án](https://bitbucket.org/taitt/sudo-startproject/wiki/WebDesignChecklist)
* [Hướng dẫn hỗ trợ khách hàng trên support.sudo.vn](https://bitbucket.org/taitt/sudo-startproject/wiki/Support%20kh%C3%A1ch%20h%C3%A0ng)
* Hướng dẫn code module thuộc core
	* [Getting Started](https://bitbucket.org/taitt/sudo-startproject/wiki/GettingStarted)
	* [Menu](https://bitbucket.org/taitt/sudo-startproject/wiki/Menu)
	* [Permission](https://bitbucket.org/taitt/sudo-startproject/wiki/Permission)
	* [Form](https://bitbucket.org/taitt/sudo-startproject/wiki/Form)
	* [Table](https://bitbucket.org/taitt/sudo-startproject/wiki/Table)
	* [Pins](https://bitbucket.org/taitt/sudo-startproject/wiki/Pins)
	* [BaseModel](https://bitbucket.org/taitt/sudo-startproject/wiki/BaseModel)
	* [Media](https://bitbucket.org/taitt/sudo-startproject/wiki/Media)
	* [Helper](https://bitbucket.org/taitt/sudo-startproject/wiki/Helper)
	* [Language](https://bitbucket.org/taitt/sudo-startproject/wiki/Language)
	* [Log](https://bitbucket.org/taitt/sudo-startproject/wiki/Log) (Hướng dẫn sau)
	* [Widget](https://bitbucket.org/taitt/sudo-startproject/wiki/Widget)
* Github thư viện module [https://github.com/Sudo-E-Commerce](https://github.com/Sudo-E-Commerce)
* [Tối ưu Core Web Vital](https://bitbucket.org/taitt/sudo-startproject/wiki/core_web_vital)