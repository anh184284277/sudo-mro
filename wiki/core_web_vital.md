# Tối ưu Core Web Vital #

## I. 6 chí số chính cần cải thiện ##

**1. First Contentful Paint (FCP)** (https://web.dev/first-contentful-paint)

- Đây là chỉ số đo lường thời gian hiển thị phần nội dung DOM đầu tiên khi người dùng điều hướng đến trang của bạn.
- Trong các dự án thì các yếu tố có thể ảnh hưởng đến FCP có thể là:
    + Đặt các file js (jquery, bootstrap,...) tại thẻ Head nhưng không có thuộc tính "asyn" hay "defer"
    + Các File css/js đặt tại thẻ head load quá lâu gây ảnh hưởng quá trình render HTML

**2. Time to Interactive (TTI)** (https://web.dev/interactive/)

- Thời điểm tương tác là khoảng thời gian mà trang cần để trở nên hoàn toàn tương tác.
- Trang được gọi là tương tác hoàn toàn khi:
    + Trang hiển thị nội dung hữu ích, được đánh giá bằng FCP
    + Các sự kiện được đăng ký cho hầu hết các phần tử tại trang
    + Thời gian phản hồi tương tác của người dùng trong vòng 50ms
- Điểm TTI được so sánh giữa chỉ số trang hiện tại với chỉ số tốc độ của các trang web thực.
- Chỉ số dùng để đo lương theo mức độ như sau (23/12/2020):
    + Nhỏ hơn 3.8s: Nhanh
    + Từ 3.9s đến 7.3s: Vừa phải
    + Lớn hơn 7.3: Chậm

**3. Speed Index** (https://web.dev/speed-index)

- Chỉ số tốc độ cho biết nội dung của một trang hiển thị nhanh chóng đến mức nào.
- Điểm chỉ số tốc độ được so sánh giữa chỉ số trang hiện tại với chỉ số tốc độ của các trang web thực.
- Chỉ số dùng để đo lương theo mức độ như sau (23/12/2020):
    + Nhỏ hơn 4.3s: Nhanh
    + Từ 4.4s đến 5.8s: Vừa phải
    + Lớn hơn 5.8: Chậm

**4. Total Blocking Time (TBT)** (https://web.dev/lighthouse-total-blocking-time)

- Tổng tất cả các khoảng thời gian giữa thời điểm Hiển thị nội dung đầu tiên (FCP) và Thời điểm tương tác khi thời gian của nhiệm vụ vượt quá 50 mili giây được biểu thị bằng đơn vị mili giây.
- Điểm TBT là sự so sánh giữa thời gian TBT của trang và thời gian TBT của 10.000 trang web hàng đầu khi được tải trên thiết bị di động.
- Chỉ số dùng để đo lương theo mức độ như sau (23/12/2020):
    + Nhỏ hơn 300ms: Nhanh
    + Từ 300ms đến 600ms: Vừa phải
    + Lớn hơn 600ms: Chậm

**5. Largest Contentful Paint (LCP)** (https://web.dev/lcp/)

- Đây là chỉ số báo cáo thời gian hiển thị của hình ảnh hoặc khối văn bản lớn nhất được nhìn thấy trong chế độ xem tại màn hình đầu tiên.
- Nó thường được xác định thông qua các thẻ: img, video, svg, khối văn bản và phần tử background được tải thông qua hàm url()
- Chỉ số dùng để đo lường theo mức độ như sau:
    + Nhỏ hơn 2.5s: Tốt
    + Từ 2.5 đến 4s: Cần cải thiện
    + Lớn hơn 4s: Kém

**6. Cumulative Layout Shift (cls)** (https://web.dev/cls)

- CLS là chỉ số đo lường mức độ thay đổi bố cục được xảy ra trên trang.
- Sự thay đổi bố cục xảy ra bất cứ khi nào có một phần tử hiển thị thay đổi vị trí hiển thị của nó trên trang.
- Các chỉ số đùng để đo lường theo mức độ như sau:
    + Nhỏ hơn 0.1: Tốt
    + Từ 0.1 đến 0.25: Cần cải thiện
    + Lớn hơn 0.25: Kém

## II. Các lỗi đáng chú ý thường bị cảnh báo và cách tối ưu ##

### 1. Hình ảnh ###

- Phân phối hình ảnh ở định dạng mới và hiệu quả hơn
- Mã hóa hình ảnh hiệu quả
- Trì hoãn tải các hình ảnh ngoài màn hình
- Thay đổi kích thước hình ảnh cho phù hợp

**Các lỗi liên quan đến hình ảnh này thường được cảnh báo khi:**

- Kích thước và dung lượng ảnh quá lớn so với box hiển thị: box 200x200 nhưng lại load ảnh 800x800 gây lãng phí tài nguyên và chậm trang web.
- Hình ảnh sử dụng định dạng nặng khiến cho dung lượng tăng so với các định dạng khác.
- Hình ảnh trên trang không được sử dụng Lazyload gây lãng phí tài nguyên.

**Các cách tối ưu**

- Các định dạng hình ảnh như JPEG 2000, JPEG XR và WebP thường nén tốt hơn PNG hoặc JPEG => Ưu tiên sử dụng định dạng ảnh có dung lượng nhẹ có độ nén tốt
- Kích thước ảnh không được quá lớn so với box hiển thị => Box 200x200 thì có thể load ảnh 200x200 hoặc 300x300, những ảnh quá lớn như 600x600 không được phép. Nên ghi chú kích thước tại admin. Lưu ý: khi làm theo cách này những ảnh để cover mà không đúng kích thước có thể sẽ bị mờ do bị phóng to ra.
- Lazyload toàn bộ hình ảnh trên trang loại trừ ảnh được xác định là LCP.
- Sử dụng CDN để tăng tốc tải ảnh.

### 2. css/js ###
- Loại bỏ css không sử dụng
- Loại bỏ JS không sử dụng
- Loại bỏ các tài nguyên chặn hiển thị
- Giảm thời gian thực thi js
- Rút gọn CSS

**Các lỗi liên quan đến css/js này thường được cảnh báo khi:**

- css/js chưa được minify, vẫn chứa các khoảng trắng gây tốn tài nguyên.
- css
    + css được khai báo nhưng không được sử dụng. VD: css của Slideshow chỉ có ở trang chủ nhưng các trang chi tiết vẫn có.
- js
    + js được khai báo nhưng không được sử dụng. VD: jquery tác động đến class `.slideshow` nhưng thực thế không có class `.slideshow` tại trang hiện tại
    + js được khai báo tại thẻ head nhưng không có thuộc tính "async" hoặc "defer"
    + js không tác động đến DOM nhưng lại được gọi tại hàm `$(document).ready(function(){})` của jquery làm tăng thời gian thực thi js

**Các cách tối ưu**

- Các file css/js cần được minify lại trước khi đẩy lên server
- Tại 1 trang chỉ sử dụng css/js cần thiết. Không dùng css/js thừa. VD: css chỉ có ở trang chủ thì sẽ không được có tại các trang khác.
- Dùng các kĩ thuật gộp File css/js thành 1 file duy nhất để giảm thiểu request khi load (trình duyệt chỉ load tối đa 8 file css hoặc js cùng 1 lúc)
    + Với css thì có thể sử dụng sass
    + Với js thì có thể sử dụng Laravel mix
- Giảm thời gian excute script
    + Chỉ sử dụng các mã script ở document ready cho các mã liên quan trực tiếp đến DOM.
    + Slide/Iframe cũng gây ra tình trạng excute script lâu. Nếu không cần thiết thì cũng cần loại bỏ. Với Iframe thì có thể dùng Lazyload

### 3. Giảm thời gian phản hồi ban đầu của máy chủ ###
- Tối ưu lại Query để giảm thời gian phản hồi của máy chủ.
- Query quá nặng thì có thể cache lại hoặc đánh index để giảm thời gian thực thi.
- Query trên 1 trang được đề xuất là nhỏ hơn 30 (không tính các query được cache).
- Tổng thời gian thực thi Query trên 1 trang nên nhỏ hơn 100ms

### 4. Bật tính năng nén văn bản (Gzip và Http2) ###
- Cần server bật tính năng Gzip cho trang web và cho toàn bộ file css/js trên trang để giảm dung lượng và tiết kiệm băng thông
- Cần server bật tính năng Http2 để tăng tốc độ duyệt web

### 5. Font chữ bên thứ 3 ###
- Các font chữ tại các bên thứ 3 là font chữ mà trình duyệt không hỗ trợ. Để dùng thì chúng ta phải tải về hoặc load thông qua CDN.
- Các font chữ thường có kích thươc rất lớn nên phải mất rất nhiều thời gian để tải từ đó ảnh hưởng tới cả 3 điểm hiệu suất:
    + First Contentful Paint (FCP)
    + Largest Contentful Paint (LCP)
    + Cumulative Layout Shift (CLS)

**Các cách tối ưu**

- Nếu sử dụng @font-face thì thêm thuộc tính `font-display: swap;`. Tuy nhiên cũng có một vài trình duyệt không hỗ trợ thuộc tính này.
- Nếu sử dụng API fonts google thì thêm `&display=swap` vào đường dẫn. VD

        https://fonts.googleapis.com/css?family=Roboto:400,700&display=swap

- Sử dụng Font mặc định.