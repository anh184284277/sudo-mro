# Hướng dẫn cài đặt widget tại trang Dashboard #
Dashboard là một phần không thể thiếu của một trang quản trị, nó giúp cho QTV có cái nhìn tổng quát về các hoạt động của website cũng như các số liệu quan trọng. Và với lý do đó SudoCore cũng hỗ trợ để tạo ra một trang Dashboard giúp QTV có thể quản theo dõi các số liệu.

Cụ thể Dashboard của SudoCore được ghép nối lại với từng phần riêng biệt được gọi là widget. Mỗi widget sẽ hiển thị một số liệu cụ thể, VD: widget hiển thị số người đang trực tuyến, widget hiển thị số bài viết mới trong ngày hoặc đơn hàng mới...và tất nhiên SudoCore không hỗ trợ sẵn các widget như trong ví dụ vì mỗi Dashboard là khác nhau, nhưng DEV có thể dễ dàng tự thêm, tự định nghĩa widget theo ý của mình.
### Cấu hình để hiển thị ###
Hãy xem demo dưới đây để hiểu hơn về cách cấu hình

    // config/SudoWidget.php
    <?php 
    return  [
    	'default' => [
    		'view' => 'Core::widget.default',
    		'autoload' => false,
    	],
    ];

Trong đó 

- key: là key của widget và là duy nhất, sẽ có rất nhiều key được cấu hình cho widget và trong demo trên là "Default"
- view: đây là giá trị bắt buộc, chính là view hiển thị
- other value: là giá trị cấu hình khác sẽ được sử dụng tại view, là cấu hình thêm không nhất thiết phải có, trong trường hợp trên là auto "autoload".

### Cấu hình để hiển thị trên 1 hàng ##
Như Demo ở trên chúng ta đã có một widget có giao diện được quy định tại  view('Core::widget.default') được hiển thị trên 1 hàng. Chúng ta cũng có thể cấu hình như sau để có thể hiển thị 2 widget trên 1 row:

	'default' => [
		'view' => 'Core::widget.group',
		'include' => [
			'default_1' => [
				'view' => 'Demo::widget.default_1',
    		    'option_1' => false,
    		    'option_2' => true,
			],
			'default_2' => [
				'view' => 'Demo::widget.default_2',
			],
		],
	],
	
view('Core::widget.group') sẽ đưa 2 view tại include vào trong thẻ div có class là row

### Cấu hình để phân quyền hiển thị ##

	'widget' => [
		'name' 			=> 'Widget',
		'permision' 	=> [
			[ 'type' => 'active_visitors', 'name' => 'Số người truy cập' ],
		],
	],

Role sẽ được xác định là key nối với type của permision bằng dấu gạch dưới `_`. Như trong VD trên thì ta sẽ có role được xác định là: `widget_active_visitors` (Xem số người đang truy cập thời điểm hiện tại).
Tại các view sẽ sử dụng hàm checkRole() để kiểm tra users có quyền xem widget này không
