# Hướng dẫn sử dụng module pins - ghim vị trí của 1 hàng dữ liệu #

Ghim là tính năng thường xuyên gặp trong quá trình thực hiện và phát triển website. Core web đã hỗ trợ tính năng ghim cho bất kỳ bảng dữ liệu nào, không giới hạn số lượng vị trí chỉ định mong muốn.

## Thiết kế database cho tính năng ghim ##
![Screen Shot 2020-07-07 at 16.45.20.png](https://bitbucket.org/repo/xXzd9GA/images/521366741-Screen%20Shot%202020-07-07%20at%2016.45.20.png)

* **type**: lưu tên bảng dữ liệu, ví dụ: posts, products, ...
* **type_id**: lưu id của row được ghim trong bảng dữ liệu tương ứng
* **place**: là vị trí mà lập trình viên đăt tên, vd: home, hot, featured, ...
* **value**: là thứ tự được ghim, số tự nhiên lớn hơn 0

Các row không được ghim, hoặc bị bỏ ghim sẽ không có dữ liệu trong bảng này.

## Cách sử dụng ở backend ##

Sử dụng tính năng ở trang danh sách, bằng cách add col hiển thị dạng pin vào listdata:


```
#!php

$listdata->add('home', 'Ghim trang chủ', 1, 'pin');
```
Trong đó: `home` là vị trí bạn đặt tên(place), `Ghim trang chủ` là label hiển thị, `1` là option có sắp xếp trong trang danh sách, `pin` là kiểu data add vào list. Mọi thứ sẽ được hiển thị và thực hiện tự động.

## Cách sử dụng (queries lấy dữ liệu) ghim ở front-end ##

Các row dữ liệu không được ghim sẽ ko lưu ở bảng pins nên khi thực hiện phép JOIN thường sẽ chỉ nhận được những row có ghim, việc này ảnh hưởng đến sắp xếp thứ tự theo value.

Trong ví dụ dưới sẽ thể hiện 1 cách JOIN đảm bảo lấy được toàn bộ các row của bảng dữ liệu kể cả không ghim nhằm mục đích sắp xếp hiển thị. 
Ta xét ví dụ bảng `posts`, được quy định 1 vị trí ghim trang chủ tên place là `home`, cần lấy ra các bài được ghim theo thứ tự từ nhỏ đến lớn:


```
#!php

$home_posts = Post::select('posts.*')
	->leftJoin('pins', function ($join) {
        $join->on('pins.type_id','=','posts.id');
        $join->on('pins.type',DB::raw("'posts'"));
        $join->on('pins.place',DB::raw("'home'"));
    })
    ->addSelect(DB::raw('(case when pins.value is null then 2147483647 else pins.value end) as pin_home'))
    ->orderBy('pin_home', 'ASC')
    ->get();
```

Trong query trên các post không được ghim sẽ nhận value với vị trí home là `2147483647`, đây là số nguyên lớn nhất lưu trong kiểu mysql int(11) để đảm bảo các post đó luôn xuống dưới cùng.