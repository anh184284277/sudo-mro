# Quy trình thực hiện dự án

### 1. Dev hỗ trợ Sale phân tích tính năng đặc biệt, tính khối lượng khi có yêu cầu.

### 2. Dev hỗ trợ Designer review thiết kế trước khi gửi cho khách. 

### 3. Nhận thiết kế, đọc mô tả dự án, deadline yêu cầu.

* Review lại toàn bộ thông tin xem có vướng mắc, thiếu hợp lý nếu thực hiện dự án để yêu cầu thay đổi. 
* Chú ý các website có dữ liệu lớn sẽ phức tạp lên nhiều lần. 
* Tính toán ngày lên demo bàn giao cho khách, thống nhất với sale set due date trong issue gitlab.

### 4. Phân tích thiết kế hệ thống, đưa ra mô hình database.

### 5. Cắt giao diện đúng thiết kế 

* Chú ý những chi tiết nhỏ như khoảng cách, font, font-size, icon ... 
* Sử dụng framework css đề nghị.

### 6. Sử dụng core code backend theo mô hình database đã đưa ra, sử dụng các package có sẵn để thực hiện

Những module chưa có package sau khi thực hiện nên tổng hợp và đẩy về package cho team sau này nếu gặp lại.

### 7. Đổ giao diện theo tính năng và giao diện đã cắt.

### 8. Hoàn thành dự án đẩy test

* Đảm bảo sản phẩm vượt qua [checklist](https://bitbucket.org/taitt/sudo-startproject/wiki/WebDesignChecklist) 
* Đảm bảo nhập liệu demo đúng 100% thiết kế

### 9. Thông báo với khách hàng demo - hotfix. 

* Khi dự án đã lên demo, nhắn với khách hàng: "Chào anh/chị dự án đã hoàn thành và được demo tại domain ..., bên em sẽ nhận các yêu cầu hotfix chỉnh sửa trong thời gian ... (thời gian đã thống nhất trong hợp đồng tùy thuộc vào độ phức tạp của dự án) từ ngày ... đến ngày ...
* Tạo tài khoản: quantri gửi cho khách, không gửi tài khoản dev
* Tiến hành fix các lỗi khách hàng báo, chỉnh sửa giao diện nhỏ không ảnh hưởng đến tính năng

### 10. Thông báo và hỗ trợ bên server release dự án.
Phối hợp với khách hàng, server để triển khai dự án theo hướng dẫn tại: [yêu cầu triển khai](https://docs.sudo.company/doku.php?id=sudo:cong_vi%E1%BB%87c_c%E1%BA%A7n_lam_khi_nh%E1%BA%ADn_du%E1%BB%A3c_yeu_c%E1%BA%A7u_d%E1%BA%A9y_website_len_domain_chinh)