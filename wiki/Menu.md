# Hướng dẫn cấu hình Menu #

Menu hay Sidebar là một phần không thể thiếu của một trang quản trị, nó giúp điều hướng đến các trang quản trị nội dung khác một cách dễ dàng hơn là phải nhớ từng link và mỗi lần thêm MENU thay vì phải copy và paste những đoạn HTML rườm rà thì SudoCore sẽ hỗ trợ cấu hình một cách cực kỳ đơn giản và dễ dàng. 

Cụ thể, chúng ta sẽ cấu hình MENU tại file `config/SudoMenu.php` (Được tạo ra sau khi Publish Core) và SudoCore hỗ trợ tạo ra 3 dạng menu sau:

### 1/ Menu tiêu đề

Menu thuộc dạng text không có link, thường được dùng để nhóm các phân vùng menu riêng biệt lại với nhau

![menu_group.PNG](https://bitbucket.org/repo/xXzd9GA/images/4186056153-menu_group.PNG)

**Cách cấu hình**

	[
        'type' 		=> 'group',
        'name' 		=> 'Tài khoản',
        'role' 		=> [
			'admin_users_create',
			'admin_users_index'
		]
    ],

Trong đó:

* type: Là loại menu, mặc định cho dạng này sẽ là "group"
* name: Sẽ là tên hiển thị
* role: Đây là cấu hình dùng để kiểm tra tại khoản hiện tại có quyền để hiển thị menu này hay không. Như ở cấu hình trên, nếu như tài khoản hiện tại không có quyền THÊM hay TRUY CẬP "Tài khoản quản trị" thì sẽ không thấy menu này nhưng nếu như có ít nhất 1 trong 2 quyền thì sẽ thấy.

### 2/ Menu đơn

Menu đơn có dạng một link không chia cấp, thường được dùng để đi thẳng đến link quản trị luôn.

![menu_single.PNG](https://bitbucket.org/repo/xXzd9GA/images/642669237-menu_single.PNG)

**Cách cấu hình**

	[
		'type' 		=> 'single',
		'name' 		=> 'Lịch sử hệ thống',
		'icon' 		=> 'fas fa-redo',
		'route' 	=> 'admin.system_logs.index',
		'role'		=> 'system_logs_index',
		'active' 	=> [ 'admin.system_logs.show' ]
	]

Trong đó

* type: Là loại menu, mặc định cho dạng này sẽ là "single"
* name: Sẽ là tên hiển thị
* icon: Là icon hiển thị của menu, sử dụng bộ font-awesome
* route: là tên của route (Router Name) được định nghĩa trước
* role: sẽ là phân quyền của route hiện tại. Như VD trên là quyền truy cập đến Lịch sử hệ thống.
* active: sẽ là quyền active với route_name tương ứng. Như VD trên khi xem Chi tiết Lịch sử hệ thống thì vẫn active menu này. Sẽ rất hữu dụng khi truy cập vào các route xem/thêm/sửa mà vẫn active menu này lên.

### 3/ Menu 2 cấp

Menu đơn có dạng cha con và được chia thành 2 cấp, thường được dùng để nhóm các link thuộc cùng 1 Module lại với nhau.

![menu_multiple.PNG](https://bitbucket.org/repo/xXzd9GA/images/2365797855-menu_multiple.PNG)

**Cách cấu hình**

	[
    	'type' 				=> 'multiple',
        'name' 				=> 'Tài khoản quản trị',
        'icon' 				=> 'fas fa-users',
        'childs' => [
            [
                'name' 		=> 'Thêm mới',
                'route' 	=> 'admin.admin_users.create',
                'role' 		=> 'admin_users_create'
            ],
            [
                'name' 		=> 'Danh sách',
				'icon' 		=> 'far fa-circle',
                'route' 	=> 'admin.admin_users.index',
                'role' 		=> 'admin_users_index',
                'active' 	=> [ 'admin.admin_users.show', 'admin.admin_users.edit' ]
            ]
        ]
    ],

Trong đó

* type: Là loại menu, mặc định cho dạng này sẽ là "multiple"
* name: Sẽ là tên hiển thị
* icon: Là icon hiển thị của menu, sử dụng bộ font-awesome
* childs: Nhóm menu con phụ thuộc
	* name: Sẽ là tên hiển thị
	* icon: Là icon hiển thị của menu, sử dụng bộ font-awesome
	* route: sẽ là phân quyền của route hiện tại. Như VD trên là quyền thêm và truy cập đến Module Tài khoản khoản quản trị
	* role: sẽ là phân quyền của route hiện tại.
	* active: sẽ là quyền active với route_name tương ứng. Như VD trên khi "xem" và "sửa" Tài khoản quản trị thì vẫn active menu "Danh sách".

## Cách hoạt động của menu đơn với dạng menu  khác

Ta có cấu hình như sau:

	[
        'type' 		=> 'group',
        'name' 		=> 'Tài khoản',
        'role' 		=> [
        	'admin_users_create',
        	'admin_users_index'
        ],
    ],
    [
    	'type' 				=> 'multiple',
        'name' 				=> 'Tài khoản quản trị',
        'icon' 				=> 'fas fa-users',
        'childs' => [
            [
                'name' 		=> 'Thêm mới',
                'route' 	=> 'admin.admin_users.create',
                'role' 		=> 'admin_users_create'
            ],
            [
                'name' 		=> 'Danh sách',
                'route' 	=> 'admin.admin_users.index',
                'role' 		=> 'admin_users_index',
                'active' 	=> [ 'admin.admin_users.show', 'admin.admin_users.edit' ]
            ]
        ]
    ],

Như cấu hình trên thì chúng ta sẽ thấy được Module "Tài khoản quản trị" sẽ thuộc nhóm "Tài khoản". Phần role của menu group "Tài khoản" sẽ là một mảng chứa các role có tại menu multiple "Tài khoản quản trị".

Như ở cấu hình trên, nếu như tài khoản hiện tại không có quyền THÊM hay TRUY CẬP "Tài khoản quản trị" thì sẽ không thấy menu group "Tài khoản" nhưng nếu như có ít nhất 1 trong 2 quyền thì sẽ thấy.

Cách hoạt động với menu single cũng tương tự.

## Chú ý

* Để xem cách lấy giá trị role hãy truy cập [Permission](https://bitbucket.org/taitt/sudo-startproject/wiki/Permission)