@extends('Core::layouts.app')
@section('title') @lang('Chào mừng bạn đến với trang quản trị') @endsection
@section('content')
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="x_panel">
                        <div class="x_content">
                            <h4>Hướng dẫn quản trị</h4>
						    <ul>
						        <li>Quản lý các Modules theo các danh mục Modules bên trái</li>
						        <li>Nội dung các Modules được hiển thị ở khung bên phải</li>
						        <li>Có thể mở rộng màn hình module bằng cách mở rộng/thu nhỏ thanh điều hướng ở header</li>
						        <li>Icon user : Truy cập Module quản lý thông tin cá nhân - Thoát phiên đăng nhập</li>
						    </ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection